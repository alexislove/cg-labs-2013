//-------------------------------------------------------------------
// File: triangle.h
// 
// This class represents plain triangle primitive and some basic methods
// to operate with it
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef GEOMETRY_TRIANGLE_H
	#define GEOMETRY_TRIANGLE_H

	#include "vector3d.h"

	struct Triangle
	{
		// Get triangle center
		friend Vector3D TriGetCenter(const Triangle& tri);

		// Get triangle normal
		friend Vector3D TriGetNormal(const Triangle& tri);

		// Get perimeter of the triangle
		friend float TriGetPerimeter(const Triangle& tri);

		// Get area of the triangle
		friend float TriGetArea(const Triangle& tri);

		Vector3D A;
		Vector3D B;
		Vector3D C;
	};

	inline Vector3D TriGetCenter(const Triangle& tri)
	{
		return 0.333333333f * (tri.A + tri.B + tri.C);
	}

	inline Vector3D TriGetNormal(const Triangle& tri)
	{
		Vector3D BA = tri.B - tri.A;
		Vector3D CA = tri.C - tri.A;

		return Vec3Cross(BA, CA).toUnit();
	}

	inline float TriGetPerimeter(const Triangle& tri)
	{
		return Vec3Length(tri.B - tri.A) + Vec3Length(tri.C - tri.B) + Vec3Length(tri.C - tri.A);
	}

	inline float TriGetArea(const Triangle& tri)
	{
		return 0.5f * (Vec3Length(Vec3Cross(tri.B - tri.A, tri.C - tri.A)));
	}

#endif // GEOMETRY_TRIANGLE_H