//-------------------------------------------------------------------
// File: matrix4x4.h
// 
// 4x4 matrix definition
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef GEOMETRY_MATRIX4X4_H
	#define GEOMETRY_MATRIX4X4_H
	
	class Vector3D;

	class Matrix4x4
	{
		// Arithmetic operations
		friend Matrix4x4 operator+(const Matrix4x4& lh, const Matrix4x4& rh);

		friend Matrix4x4 operator-(const Matrix4x4& lh, const Matrix4x4& rh);

		friend Matrix4x4 operator*(const Matrix4x4& lh, const Matrix4x4& rh);

		friend Matrix4x4 operator*(const Matrix4x4& lh, float value);

		// Result will be projected using w = 1
		friend Vector3D operator*(const Matrix4x4& lh, const Vector3D& rh);

		// Transformation operations

		// For rotation angle is given in degrees
		friend void M4x4Rotation(const Vector3D& axis, float angle, Matrix4x4* out);

		friend void M4x4Translation(const Vector3D& dr, Matrix4x4* out);

		friend void M4x4Scale(const Vector3D& ds, Matrix4x4* out);

		friend void M4x4Transpose(const Matrix4x4& m, Matrix4x4* out);

		friend bool M4x4Inverse(const Matrix4x4& src, Matrix4x4* out);

    friend float M4x4Determinant(const Matrix4x4& src);

		// Matrix internal type definition
		enum { DIM = 4 };
		typedef float TMatrix[DIM][DIM];
	public:

		// Default ctor will set matrix to identity
		Matrix4x4();

		// VS2010 doesn't support initializer_list
		Matrix4x4(float x00, float x01, float x02, float x03,
							float x10, float x11, float x12, float x13,
							float x20, float x21, float x22, float x23,
							float x30, float x31, float x32, float x33);


		// Copy ctor
		Matrix4x4(const Matrix4x4& other);
	
		// Move ctor
		Matrix4x4(const Matrix4x4&& other);

		Matrix4x4& toIdentity();

		float operator()(int i, int j) const
		{
			return mM[i][j];
		}

		float& operator()(int i, int j)
		{
			return mM[i][j];
		}

		void toArray(float out[DIM * DIM]) const;
		

	private:
		TMatrix mM;
	};

#endif // GEOMETRY_MATRIX4X4_H