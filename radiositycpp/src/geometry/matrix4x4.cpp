//-------------------------------------------------------------------
// File: matrix4x4.cpp
// 
// 4x4 matrix implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#define _USE_MATH_DEFINES

#include "vector3d.h"

#include "matrix4x4.h"

Matrix4x4::Matrix4x4()
{
	toIdentity();
}

Matrix4x4::Matrix4x4(float x00, float x01, float x02, float x03, 
										 float x10, float x11, float x12, float x13, 
										 float x20, float x21, float x22, float x23, 
										 float x30, float x31, float x32, float x33)
{
	mM[0][0] = x00; mM[0][1] = x01; mM[0][2] = x02; mM[0][3] = x03;
	mM[1][0] = x10; mM[1][1] = x11; mM[1][2] = x12; mM[1][3] = x13;
	mM[2][0] = x20; mM[2][1] = x21; mM[2][2] = x22; mM[2][3] = x23;
	mM[3][0] = x30; mM[3][1] = x31; mM[3][2] = x32; mM[3][3] = x33;
}

Matrix4x4::Matrix4x4(const Matrix4x4& other)
{
	mM[0][0] = other.mM[0][0]; mM[0][1] = other.mM[0][1]; mM[0][2] = other.mM[0][2]; mM[0][3] = other.mM[0][3];
	mM[1][0] = other.mM[1][0]; mM[1][1] = other.mM[1][1]; mM[1][2] = other.mM[1][2]; mM[1][3] = other.mM[1][3];
	mM[2][0] = other.mM[2][0]; mM[2][1] = other.mM[2][1]; mM[2][2] = other.mM[2][2]; mM[2][3] = other.mM[2][3];
	mM[3][0] = other.mM[3][0]; mM[3][1] = other.mM[3][1]; mM[3][2] = other.mM[3][2]; mM[3][3] = other.mM[3][3];
}

Matrix4x4::Matrix4x4(const Matrix4x4&& other)
{
	mM[0][0] = other.mM[0][0]; mM[0][1] = other.mM[0][1]; mM[0][2] = other.mM[0][2]; mM[0][3] = other.mM[0][3];
	mM[1][0] = other.mM[1][0]; mM[1][1] = other.mM[1][1]; mM[1][2] = other.mM[1][2]; mM[1][3] = other.mM[1][3];
	mM[2][0] = other.mM[2][0]; mM[2][1] = other.mM[2][1]; mM[2][2] = other.mM[2][2]; mM[2][3] = other.mM[2][3];
	mM[3][0] = other.mM[3][0]; mM[3][1] = other.mM[3][1]; mM[3][2] = other.mM[3][2]; mM[3][3] = other.mM[3][3];
}

Matrix4x4& Matrix4x4::toIdentity()
{
	mM[0][0] = 1.f; mM[0][1] = 0.f; mM[0][2] = 0.f; mM[0][3] = 0.f;
	mM[1][0] = 0.f; mM[1][1] = 1.f; mM[1][2] = 0.f; mM[1][3] = 0.f;
	mM[2][0] = 0.f; mM[2][1] = 0.f; mM[2][2] = 1.f; mM[2][3] = 0.f;
	mM[3][0] = 0.f; mM[3][1] = 0.f; mM[3][2] = 0.f; mM[3][3] = 1.f;

	return *this;
}

void Matrix4x4::toArray(float out[DIM * DIM]) const
{
	out[0] = mM[0][0];
	out[1] = mM[0][1];
	out[2] = mM[0][2];
	out[3] = mM[0][3];

	out[4] = mM[1][0];
	out[5] = mM[1][1];
	out[6] = mM[1][2];
	out[7] = mM[1][3];

	out[8]  = mM[2][0];
	out[9]  = mM[2][1];
	out[10] = mM[2][2];
	out[11] = mM[2][3];

	out[12] = mM[3][0];
	out[13] = mM[3][1];
	out[14] = mM[3][2];
	out[15] = mM[3][3];
}

// Friend methods implementation

#define DEG_TO_RAD(x) static_cast<float>((x) * M_PI / 180.f)

Matrix4x4 operator+(const Matrix4x4& lh, const Matrix4x4& rh)
{
	return Matrix4x4(
		lh(0, 0) + rh(0, 0), lh(0, 1) + rh(0, 1), lh(0, 2) + rh(0, 2), lh(0, 3) + rh(0, 3), 
		lh(1, 0) + rh(1, 0), lh(1, 1) + rh(1, 1), lh(1, 2) + rh(1, 2), lh(1, 3) + rh(1, 3), 
		lh(2, 0) + rh(2, 0), lh(2, 1) + rh(2, 1), lh(2, 2) + rh(2, 2), lh(2, 3) + rh(2, 3), 
		lh(3, 0) + rh(3, 0), lh(3, 1) + rh(3, 1), lh(3, 2) + rh(3, 2), lh(3, 3) + rh(3, 3)
	);
}

Matrix4x4 operator-(const Matrix4x4& lh, const Matrix4x4& rh)
{
	return Matrix4x4(
		lh(0, 0) - rh(0, 0), lh(0, 1) - rh(0, 1), lh(0, 2) - rh(0, 2), lh(0, 3) - rh(0, 3), 
		lh(1, 0) - rh(1, 0), lh(1, 1) - rh(1, 1), lh(1, 2) - rh(1, 2), lh(1, 3) - rh(1, 3), 
		lh(2, 0) - rh(2, 0), lh(2, 1) - rh(2, 1), lh(2, 2) - rh(2, 2), lh(2, 3) - rh(2, 3), 
		lh(3, 0) - rh(3, 0), lh(3, 1) - rh(3, 1), lh(3, 2) - rh(3, 2), lh(3, 3) - rh(3, 3));
}

Matrix4x4 operator*(const Matrix4x4& lh, const Matrix4x4& rh)
{
	Matrix4x4 res;
	// O(n^3)
	for (int i = 0; i < Matrix4x4::DIM; ++i)
	{
		for (int j = 0; j < Matrix4x4::DIM; ++j)
		{
			float& ax = res(i, j);
			ax = 0.f;
			for (int k = 0; k < Matrix4x4::DIM; ++k)
			{
				ax += lh(i, k) * rh(k, j);
			}
		}
	}

	return res;
}

Matrix4x4 operator*(const Matrix4x4& lh, float value)
{
	Matrix4x4 res;

	res(0, 0) = lh(0, 0) * value; res(0, 1) = lh(0, 1) * value; res(0, 2) = lh(0, 2) * value; res(0, 3) = lh(0, 3) * value;
	res(1, 0) = lh(1, 0) * value; res(1, 1) = lh(1, 1) * value; res(1, 2) = lh(1, 2) * value; res(1, 3) = lh(1, 3) * value;
	res(2, 0) = lh(2, 0) * value; res(2, 1) = lh(2, 1) * value; res(2, 2) = lh(2, 2) * value; res(2, 3) = lh(2, 3) * value;
	res(3, 0) = lh(3, 0) * value; res(3, 1) = lh(3, 1) * value; res(3, 2) = lh(3, 2) * value; res(3, 3) = lh(3, 3) * value;

	return res;
}


Vector3D operator*(const Matrix4x4& lh, const Vector3D& rh)
{
	float x = lh(0, 0) * rh.x() + lh(1, 0) * rh.y() + lh(2, 0) * rh.z() + lh(3, 0);
	float y = lh(0, 1) * rh.x() + lh(1, 1) * rh.y() + lh(2, 1) * rh.z() + lh(3, 1);
	float z = lh(0, 2) * rh.x() + lh(1, 2) * rh.y() + lh(2, 2) * rh.z() + lh(3, 2);
	float w = lh(0, 3) * rh.x() + lh(1, 3) * rh.y() + lh(2, 3) * rh.z() + lh(3, 3);

	return Vector3D(x, y, z);
}

void M4x4Rotation(const Vector3D& axis, float angle, Matrix4x4* out)
{
  // Ensure axis is normalized
  Vector3D u = axis.normalize();

  // Construct tensor and cross product matrices of u

  Matrix4x4 Ux(0.f,  -u.z(),  u.y(), 0.f,
               u.z(),   0.f, -u.x(), 0.f,
              -u.y(), u.x(),    0.f, 0.f,
                 0.f,   0.f,    0.f, 0.f);

  Matrix4x4 tensor(u.x() * u.x(), u.x() * u.y(), u.x() * u.z(), 0.f,
                   u.x() * u.y(), u.y() * u.y(), u.y() * u.z(), 0.f,
                   u.x() * u.z(), u.y() * u.z(), u.z() * u.z(), 0.f,
                   0.f          , 0.f          , 0.f          , 0.f);


  Matrix4x4 I;

  const float cosTheta = cosf(angle);
  const float sinTheta = sinf(angle);

  *out = (I * cosTheta + Ux * sinTheta + tensor * (1 - cosTheta)) * (*out);
}

void M4x4Translation(const Vector3D& dr, Matrix4x4* out)
{
	Matrix4x4 victim;

	victim(3, 0) = dr.x();
	victim(3, 1) = dr.y();
	victim(3, 2) = dr.z();

	*out = victim * (*out);
}

void M4x4Scale(const Vector3D& ds, Matrix4x4* out)
{
	Matrix4x4 victim;

	victim(0, 0) = ds.x();
	victim(1, 1) = ds.y();
	victim(2, 2) = ds.z();

	*out = victim * (*out);
}

void M4x4Transpose(const Matrix4x4& m, Matrix4x4* out)
{
	for (int idx = 0; idx < Matrix4x4::DIM; ++idx)
	{
		for (int jdx = 0; jdx < Matrix4x4::DIM; ++jdx)
		{
			(*out)(idx, jdx) = m(jdx, idx);
		}
	}
}

bool M4x4Inverse(const Matrix4x4& src, Matrix4x4* out)
{
	float m[16];
	float inv[16];

	src.toArray(m);

  inv[0] = m[5]  * m[10] * m[15] - 
           m[5]  * m[11] * m[14] - 
           m[9]  * m[6]  * m[15] + 
           m[9]  * m[7]  * m[14] +
           m[13] * m[6]  * m[11] - 
           m[13] * m[7]  * m[10];

  inv[4] = -m[4]  * m[10] * m[15] + 
            m[4]  * m[11] * m[14] + 
            m[8]  * m[6]  * m[15] - 
            m[8]  * m[7]  * m[14] - 
            m[12] * m[6]  * m[11] + 
            m[12] * m[7]  * m[10];

  inv[8] = m[4]  * m[9] * m[15] - 
            m[4]  * m[11] * m[13] - 
            m[8]  * m[5] * m[15] + 
            m[8]  * m[7] * m[13] + 
            m[12] * m[5] * m[11] - 
            m[12] * m[7] * m[9];

  inv[12] = -m[4]  * m[9] * m[14] + 
              m[4]  * m[10] * m[13] +
              m[8]  * m[5] * m[14] - 
              m[8]  * m[6] * m[13] - 
              m[12] * m[5] * m[10] + 
              m[12] * m[6] * m[9];

  inv[1] = -m[1]  * m[10] * m[15] + 
            m[1]  * m[11] * m[14] + 
            m[9]  * m[2] * m[15] - 
            m[9]  * m[3] * m[14] - 
            m[13] * m[2] * m[11] + 
            m[13] * m[3] * m[10];

  inv[5] = m[0]  * m[10] * m[15] - 
            m[0]  * m[11] * m[14] - 
            m[8]  * m[2] * m[15] + 
            m[8]  * m[3] * m[14] + 
            m[12] * m[2] * m[11] - 
            m[12] * m[3] * m[10];

  inv[9] = -m[0]  * m[9] * m[15] + 
            m[0]  * m[11] * m[13] + 
            m[8]  * m[1] * m[15] - 
            m[8]  * m[3] * m[13] - 
            m[12] * m[1] * m[11] + 
            m[12] * m[3] * m[9];

  inv[13] = m[0]  * m[9] * m[14] - 
            m[0]  * m[10] * m[13] - 
            m[8]  * m[1] * m[14] + 
            m[8]  * m[2] * m[13] + 
            m[12] * m[1] * m[10] - 
            m[12] * m[2] * m[9];

  inv[2] = m[1]  * m[6] * m[15] - 
            m[1]  * m[7] * m[14] - 
            m[5]  * m[2] * m[15] + 
            m[5]  * m[3] * m[14] + 
            m[13] * m[2] * m[7] - 
            m[13] * m[3] * m[6];

  inv[6] = -m[0]  * m[6] * m[15] + 
            m[0]  * m[7] * m[14] + 
            m[4]  * m[2] * m[15] - 
            m[4]  * m[3] * m[14] - 
            m[12] * m[2] * m[7] + 
            m[12] * m[3] * m[6];

  inv[10] = m[0]  * m[5] * m[15] - 
            m[0]  * m[7] * m[13] - 
            m[4]  * m[1] * m[15] + 
            m[4]  * m[3] * m[13] + 
            m[12] * m[1] * m[7] - 
            m[12] * m[3] * m[5];

  inv[14] = -m[0]  * m[5] * m[14] + 
              m[0]  * m[6] * m[13] + 
              m[4]  * m[1] * m[14] - 
              m[4]  * m[2] * m[13] - 
              m[12] * m[1] * m[6] + 
              m[12] * m[2] * m[5];

  inv[3] = -m[1] * m[6] * m[11] + 
            m[1] * m[7] * m[10] + 
            m[5] * m[2] * m[11] - 
            m[5] * m[3] * m[10] - 
            m[9] * m[2] * m[7] + 
            m[9] * m[3] * m[6];

  inv[7] = m[0] * m[6] * m[11] - 
            m[0] * m[7] * m[10] - 
            m[4] * m[2] * m[11] + 
            m[4] * m[3] * m[10] + 
            m[8] * m[2] * m[7] - 
            m[8] * m[3] * m[6];

  inv[11] = -m[0] * m[5] * m[11] + 
              m[0] * m[7] * m[9] + 
              m[4] * m[1] * m[11] - 
              m[4] * m[3] * m[9] - 
              m[8] * m[1] * m[7] + 
              m[8] * m[3] * m[5];

  inv[15] = m[0] * m[5] * m[10] - 
            m[0] * m[6] * m[9] - 
            m[4] * m[1] * m[10] + 
            m[4] * m[2] * m[9] + 
            m[8] * m[1] * m[6] - 
            m[8] * m[2] * m[5];

  float det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];

  if (det < FLOAT_ZERO)
	{
    return false;
	}

  det = 1.0f / det;

  for (int idx = 0; idx < Matrix4x4::DIM; ++idx)
	{
		for (int jdx = 0; jdx < Matrix4x4::DIM; ++jdx)
		{
			(*out)(idx, jdx) = inv[idx * Matrix4x4::DIM + jdx] * det;
		}
	}
      

  return true;
}

float M4x4Determinant(const Matrix4x4& src)
{
  float det = src(0, 0) * src(1, 1) * src(2, 2) * src(3, 3) + src(0, 0) * src(1, 2) * src(2, 3) * src(3, 1) + src(0, 0) * src(1, 3) * src(2, 1) * src(3, 2) +
              src(0, 1) * src(1, 0) * src(2, 3) * src(3, 2) + src(0, 1) * src(1, 2) * src(2, 0) * src(3, 3) + src(0, 1) * src(1, 3) * src(2, 2) * src(3, 0) +
              src(0, 2) * src(1, 0) * src(2, 1) * src(3, 3) + src(0, 2) * src(1, 1) * src(2, 3) * src(3, 0) + src(0, 2) * src(1, 3) * src(2, 0) * src(3, 1) +
              src(0, 3) * src(1, 0) * src(2, 2) * src(3, 1) + src(0, 3) * src(1, 1) * src(2, 0) * src(3, 2) + src(0, 3) * src(1, 2) * src(2, 1) * src(3, 0) -
              src(0, 0) * src(1, 1) * src(2, 3) * src(3, 2) - src(0, 0) * src(1, 2) * src(2, 1) * src(3, 3) - src(0, 0) * src(1, 3) * src(2, 2) * src(3, 1) -
              src(0, 1) * src(1, 0) * src(2, 2) * src(3, 3) - src(0, 1) * src(1, 2) * src(2, 3) * src(3, 0) - src(0, 1) * src(1, 3) * src(2, 0) * src(3, 2) -
              src(0, 2) * src(1, 0) * src(2, 3) * src(3, 1) - src(0, 2) * src(1, 1) * src(2, 0) * src(3, 3) - src(0, 2) * src(1, 3) * src(2, 1) * src(3, 0) -
              src(0, 3) * src(1, 0) * src(2, 1) * src(3, 2) - src(0, 3) * src(1, 1) * src(2, 2) * src(3, 0) - src(0, 3) * src(1, 2) * src(2, 0) * src(3, 1);
  return det;
}