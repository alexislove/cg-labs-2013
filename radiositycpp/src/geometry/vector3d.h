//-------------------------------------------------------------------
// File: vector3d.h
// 
// 3D vector inlined arithmetics and operations
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef GEOMETRY_VECTOR3D_H
	#define GEOMETRY_VECTOR3D_H

	#include <math.h>

	#include "precision.h"

	#pragma warning(disable: 4201)

	class Vector3D
	{
		//! Arithmetic operators in order of appearance: sum, difference, scale, component-wise multiplication
		friend Vector3D operator+(const Vector3D& lh, const Vector3D& rh);

		friend Vector3D operator-(const Vector3D& lh, const Vector3D& rh);

		friend Vector3D operator*(const Vector3D& lh, float value);

		friend Vector3D operator*(const Vector3D& lh, const Vector3D& rh);

		friend Vector3D operator*(float value, const Vector3D& rh);

		friend Vector3D operator/(const Vector3D& lh, float value);

		friend Vector3D operator-(const Vector3D& v);

    friend bool operator==(const Vector3D& v1, const Vector3D& v2);

		//! Vector operations: dot product and cross product
		friend float Vec3Dot(const Vector3D& lh, const Vector3D& rh);

		friend Vector3D Vec3Cross(const Vector3D& lh, const Vector3D& rh);

		friend float Vec3Length(const Vector3D& v);

		friend float Vec3Length2(const Vector3D& v);

		// Get center between 2 given vertices
		friend Vector3D Vec3Center(const Vector3D& v1, const Vector3D& v2);

	public:

		//! Ctors
		Vector3D()
		{
			u.mX = u.mY = u.mZ = 0.f;
		}

		Vector3D(float x, float y, float z)
		{
			u.mX = x;
			u.mY = y;
			u.mZ = z;
		}

		//! Accessors and mutators
		float x() const
		{
			return u.mX;
		}

		float y() const
		{
			return u.mY;
		}

		float z() const
		{
			return u.mZ;
		}

		void setX(float x) 
		{
			u.mX = x;
		}

		void setY(float y)
		{
			u.mY = y;
		}

		void setZ(float z)
		{
			u.mZ = z;
		}

		void setXYZ(float x, float y, float z)
		{
			u.mX = x; u.mY = y; u.mZ = z;
		}

		// Inplace operators, inlining them
		Vector3D& operator+=(const Vector3D& rh)
		{
			u.mX += rh.u.mX;
			u.mY += rh.u.mY;
			u.mZ += rh.u.mZ;
			return *this;
		}

		Vector3D& operator-=(const Vector3D& rh)
		{
			u.mX -= rh.u.mX;
			u.mY -= rh.u.mY;
			u.mZ -= rh.u.mZ;
			return *this;
		}

		Vector3D& operator*=(float value)
		{
			u.mX *= value;
			u.mY *= value;
			u.mZ *= value;
			return *this;
		}

		Vector3D& operator/=(float value)
		{
			u.mX /= value;
			u.mY /= value;
			u.mZ /= value;
			return *this;
		}

		void normalize()
		{
			float len = Vec3Length(*this);
			if (len != 0.f)
				*this /= len;
		}

		Vector3D normalize() const
		{
			float len = Vec3Length(*this);

			if (len != 0.f)
			{
				return Vector3D(u.mX / len, u.mY / len, u.mZ / len);
			}

			return *this;
		}

		// Perform inplace normalization
		Vector3D& toUnit()
		{
			normalize();
			return *this;
		}

		Vector3D inverse() const
		{
			return Vector3D(1.f / u.mX, 1.f / u.mY, 1.f / u.mZ);
		}

		Vector3D absolute() const
		{
			return Vector3D(fabs(u.mX), fabs(u.mY), fabs(u.mZ));
		}

		operator float*()
		{
			return u.mData;
		}

	private:
		union
		{
			struct 
			{
				float mX, mY, mZ;
			};
			float mData[3];
		} u;
	};

	//! Inline all operation

	inline Vector3D operator+(const Vector3D& lh, const Vector3D& rh)
	{
		return Vector3D(lh.u.mX + rh.u.mX, lh.u.mY + rh.u.mY, lh.u.mZ + rh.u.mZ);
	}

	inline Vector3D operator-(const Vector3D& lh, const Vector3D& rh)
	{
		return Vector3D(lh.u.mX - rh.u.mX, lh.u.mY - rh.u.mY, lh.u.mZ - rh.u.mZ);
	}

	inline Vector3D operator*(const Vector3D& lh, float value)
	{
		return Vector3D(lh.u.mX * value, lh.u.mY * value, lh.u.mZ * value);
	}

	inline Vector3D operator*(float value, const Vector3D& rh)
	{
		return Vector3D(rh.u.mX * value, rh.u.mY * value, rh.u.mZ * value);
	}

	inline Vector3D operator*(const Vector3D& lh, const Vector3D& rh)
	{
		return Vector3D(lh.u.mX * rh.u.mX, lh.u.mY * rh.u.mY, lh.u.mZ * rh.u.mZ);
	}

	inline Vector3D operator/(const Vector3D& lh, float value)
	{
		return Vector3D(lh.u.mX / value, lh.u.mY / value, lh.u.mZ / value);
	}

	inline Vector3D operator-(const Vector3D& lh)
	{
		return Vector3D(-lh.u.mX, -lh.u.mY, -lh.u.mZ);
	}

  inline bool operator==(const Vector3D& v1, const Vector3D& v2)
  {
    return fabs(v1.x() - v2.x()) < FLOAT_ZERO && fabs(v1.y() - v2.y()) < FLOAT_ZERO && fabs(v1.z() - v2.z()) < FLOAT_ZERO;
  }


	inline float Vec3Dot(const Vector3D& lh, const Vector3D& rh)
	{
		return lh.u.mX * rh.u.mX + lh.u.mY * rh.u.mY + lh.u.mZ * rh.u.mZ;
	}

	inline Vector3D Vec3Cross(const Vector3D& lh, const Vector3D& rh)
	{
		return Vector3D(lh.u.mY * rh.u.mZ - lh.u.mZ * rh.u.mY,
										lh.u.mZ * rh.u.mX - lh.u.mX * rh.u.mZ,
										lh.u.mX * rh.u.mY - lh.u.mY * rh.u.mX);
	}

	inline float Vec3Length(const Vector3D& v)
	{
		float len = Vec3Length2(v);

		if (len < FLOAT_ZERO)
			return len;
		if (len == 1.f)
			return 1.f;

		return sqrtf(len);
	}

	inline float Vec3Length2(const Vector3D& v)
	{
		return Vec3Dot(v, v);
	}

	inline Vector3D Vec3Center(const Vector3D& v1, const Vector3D& v2)
	{
		return 0.5f * (v1 + v2);
	}

#endif // GEOMETRY_VECTOR3D_H