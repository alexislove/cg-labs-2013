//-------------------------------------------------------------------
// File: cubesurface.cpp
// 
// This class represents cube surface
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include "radiosity/patch.h"

#include "cubesurface.h"

CubeSurface::CubeSurface(const Material& material, const QString& name)
	: mMaterial(material)
{
	setName(name);
	initialize();
}

void CubeSurface::initialize()
{
	std::shared_ptr< TrianglesCollection > triangles(new TrianglesCollection);

	// Cube in center of coordinates

	PatchTriangle triangle;
	triangle.TheMaterial = mMaterial;
	// Left plane
	
	triangle.A = Vector3D(-0.5f, -0.5f, -0.5f);
	triangle.B = Vector3D(-0.5f, -0.5f, +0.5f);
	triangle.C = Vector3D(-0.5f, +0.5f, +0.5f);

	triangles->add(triangle);

	triangle.A = Vector3D(-0.5f, +0.5f, +0.5f);
	triangle.B = Vector3D(-0.5f, +0.5f, -0.5f);
	triangle.C = Vector3D(-0.5f, -0.5f, -0.5f);

	triangles->add(triangle);

	// Right plane

	triangle.A = Vector3D(+0.5f, -0.5f, -0.5f);
	triangle.B = Vector3D(+0.5f, +0.5f, -0.5f);
	triangle.C = Vector3D(+0.5f, +0.5f, +0.5f);

	triangles->add(triangle);

	triangle.A = Vector3D(+0.5f, +0.5f, +0.5f);
	triangle.B = Vector3D(+0.5f, -0.5f, +0.5f);
	triangle.C = Vector3D(+0.5f, -0.5f, -0.5f);

	triangles->add(triangle);

	// Bottom plane

	triangle.A = Vector3D(-0.5f, -0.5f, -0.5f);
	triangle.B = Vector3D(+0.5f, -0.5f, -0.5f);
	triangle.C = Vector3D(+0.5f, -0.5f, +0.5f);

	triangles->add(triangle);

	triangle.A = Vector3D(+0.5f, -0.5f, +0.5f);
	triangle.B = Vector3D(-0.5f, -0.5f, +0.5f);
	triangle.C = Vector3D(-0.5f, -0.5f, -0.5f);

	triangles->add(triangle);

	// Top plane

	triangle.A = Vector3D(-0.5f, +0.5f, -0.5f);
	triangle.B = Vector3D(-0.5f, +0.5f, +0.5f);
	triangle.C = Vector3D(+0.5f, +0.5f, +0.5f);

	triangles->add(triangle);

	triangle.A = Vector3D(+0.5f, +0.5f, +0.5f);
	triangle.B = Vector3D(+0.5f, +0.5f, -0.5f);
	triangle.C = Vector3D(-0.5f, +0.5f, -0.5f);

	triangles->add(triangle);

	// Rear plane

	triangle.A = Vector3D(-0.5f, -0.5f, -0.5f);
	triangle.B = Vector3D(-0.5f, +0.5f, -0.5f);
	triangle.C = Vector3D(+0.5f, +0.5f, -0.5f);

	triangles->add(triangle);

	triangle.A = Vector3D(+0.5f, +0.5f, -0.5f);
	triangle.B = Vector3D(+0.5f, -0.5f, -0.5f);
	triangle.C = Vector3D(-0.5f, -0.5f, -0.5f);

	triangles->add(triangle);

	// Front plane

	triangle.A = Vector3D(-0.5f, -0.5f, +0.5f);
	triangle.B = Vector3D(+0.5f, -0.5f, +0.5f);
	triangle.C = Vector3D(+0.5f, +0.5f, +0.5f);

	triangles->add(triangle);

	triangle.A = Vector3D(+0.5f, +0.5f, +0.5f);
	triangle.B = Vector3D(-0.5f, +0.5f, +0.5f);
	triangle.C = Vector3D(-0.5f, -0.5f, +0.5f);

	triangles->add(triangle);

	setTrianglesCollection(triangles);
}