//-------------------------------------------------------------------
// File: cubesurface.h
// 
// This class represents cube surface
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef FRONTEND_CUBESURFACE_H
	#define FRONTEND_CUBESURFACE_H

	#include "illumination/material.h"

	#include "namedsurface.h"

	class CubeSurface : public NamedSurface
	{
	public:
		explicit CubeSurface(const Material& material, const QString& name);

	private:
		// Initialize cube triangles
		void initialize();

	private:
		//! Material instance
		Material mMaterial;
	};

#endif // FRONTEND_CUBESURFACE_H