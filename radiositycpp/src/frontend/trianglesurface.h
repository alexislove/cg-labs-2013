//-------------------------------------------------------------------
// File: trianglesurface.h
// 
// This class represents surface, that contains single triangle
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef FRONTEND_TRIANGLESURFACE_H
	#define FRONTEND_TRIANGLESURFACE_H

	#include "radiosity/patchtriangle.h"

	#include "namedsurface.h"

	class TriangleSurface : public NamedSurface
	{
	public:
		explicit TriangleSurface(const PatchTriangle& tri, const QString& name)
		{
			setName(name);
			addTriangle(tri);
		}
	};

#endif // FRONTEND_TRIANGLESURFACE_H
