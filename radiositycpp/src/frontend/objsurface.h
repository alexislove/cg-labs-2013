//-------------------------------------------------------------------
// File: objsurface.h
// 
// This class represents obj model surface
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef FRONTEND_OBJSURFACE_H
	#define FRONTEND_OBJSURFACE_H

	#include <vector>

	#include "illumination/material.h"

	#include "namedsurface.h"

	class ObjSurface : public NamedSurface
	{
	public:
		explicit ObjSurface(const std::vector<Triangle>& triangles, const Material& material, const QString& name);

	private:
		//! Material instance
		Material mMaterial;
	};

#endif // FRONTEND_OBJSURFACE_H