//-------------------------------------------------------------------
// File: computedradiosityserializable.h
// 
// Qt based XML computed radiosity scene serializable implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef FRONTEND_COMPUTEDRADIOSITYSERIALIZABLE_H
	#define FRONTEND_COMPUTEDRADIOSITYSERIALIZABLE_H

	#include <memory>

	#include "radiosity/patch.h"

	#include "ixmlserializable.h"

	class ComputedRadiositySerializable : private IXmlSerializable
	{
	public:
		explicit ComputedRadiositySerializable();

		~ComputedRadiositySerializable();

		// Write XML representation of given patch collection into file with given name
		bool writePatchCollection(std::shared_ptr<PatchCollection> collection, const QString& fileName);

	protected:

		virtual bool read(const QDomNode* node);

		virtual bool write(QDomDocument* document, QDomNode* node) const;


	private:
		//! Local copy of patch collection, will be reset, after writing finishes
		std::shared_ptr<PatchCollection> mCollection;
	};

#endif // FRONTEND_COMPUTEDRADIOSITYSERIALIZABLE_H