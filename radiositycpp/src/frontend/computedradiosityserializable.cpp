//-------------------------------------------------------------------
// File: computedradiosityserializable.cpp
// 
// Qt based XML computed radiosity scene serializable implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include <QFile>

#include "computedradiosityserializable.h"

namespace
{
	// Local writers for different scene entities
	struct VectorWriter : public IXmlSerializable
	{
		void writePosition(QDomDocument* document, QDomNode *node, const Vector3D& vector)
		{
			Vector = vector;
			write(document, node);
		}

		virtual bool read(const QDomNode *node)
		{
			return false;
		}

		bool write(QDomDocument* document, QDomNode* node) const
		{
			addAttribute(node->toElement(), "x", Vector.x());
			addAttribute(node->toElement(), "y", Vector.y());
			addAttribute(node->toElement(), "z", Vector.z());

			return true;
		}


		Vector3D Vector;
	};

	struct ColorWriter : public IXmlSerializable
	{
		void writeColor(QDomDocument* document, QDomNode *node, const Color& color)
		{
			TheColor = color;
			write(document, node);
		}

		virtual bool read(const QDomNode *node)
		{
			return false;
		}

		bool write(QDomDocument* document, QDomNode* node) const
		{
			addAttribute(node->toElement(), "r", TheColor.r());
			addAttribute(node->toElement(), "g", TheColor.g());
			addAttribute(node->toElement(), "b", TheColor.b());

			return true;
		}


		Color TheColor;
	};

	struct PatchWriter : public IXmlSerializable
	{
	
		virtual bool read(const QDomNode *node)
		{
			return false;
		}

		bool write(QDomDocument* document, QDomNode* node) const
		{
			//addAttribute(node->toElement(), "r", TheColor.r());
			//addAttribute(node->toElement(), "g", TheColor.g());
			//addAttribute(node->toElement(), "b", TheColor.b());

			QDomElement v0		= addElement(*document, *node, "pos");
			QDomElement v1	  = addElement(*document, *node, "pos");
			QDomElement v2	  = addElement(*document, *node, "pos");
			QDomElement color = addElement(*document, *node, "color");

			VectorWriter vectorWriter;

			vectorWriter.writePosition(document, &v0, ObjTriangle.A);
			vectorWriter.writePosition(document, &v1, ObjTriangle.B);
			vectorWriter.writePosition(document, &v2, ObjTriangle.C);

			ColorWriter colorWriter;

			colorWriter.writeColor(document, &color, ObjTriangle.TheMaterial.Radiosity);

			return true;
		}

		PatchTriangle ObjTriangle;
	};

	struct PatchCollectionWriter : public IXmlSerializable
	{

		virtual bool read(const QDomNode *node)
		{
			return false;
		}

		bool write(QDomDocument* document, QDomNode* node) const
		{
			PatchWriter writer;
			for (int idx = 0, count = Collection->size(); idx < count; ++idx)
			{
				const PatchTriangle& tri = (*Collection)[idx];

				writer.ObjTriangle = tri;

				QDomNode triangle = addElement(*document, *node, "triangle");

				writer.write(document, &triangle);
			}

			return true;
		}

		std::shared_ptr<PatchCollection> Collection;
	};
}

ComputedRadiositySerializable::ComputedRadiositySerializable()
{

}

ComputedRadiositySerializable::~ComputedRadiositySerializable()
{

}

bool ComputedRadiositySerializable::writePatchCollection(std::shared_ptr<PatchCollection> collection, const QString& fileName)
{
	mCollection = collection;

	QDomDocument document;

	document.appendChild(document.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\""));

	QDomElement root = document.createElement("scene");
	document.appendChild(root);

	if (!write(&document, &root))
	{
		return false;
	}

	// Now present document as string and write it to device
	QString xmlString = document.toString();

	if (xmlString.isEmpty())
		return false;

	QFile sceneFile(fileName);

	if (!sceneFile.open(QIODevice::WriteOnly))
		return false;

	sceneFile.write(xmlString.toLocal8Bit());

	mCollection.reset();

	return true;
}

bool ComputedRadiositySerializable::read(const QDomNode* node)
{
	Q_ASSERT(!"Reading computed radiosity not implemented!");
	return false;
}

bool ComputedRadiositySerializable::write(QDomDocument* document, QDomNode* node) const
{
	// Write the whole collection

	QDomNode triangles = addElement(*document, *node, "triangles");

	PatchCollectionWriter writer;

	writer.Collection = mCollection;

	writer.write(document, &triangles);

	return true;
}