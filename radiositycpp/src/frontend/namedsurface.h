//-------------------------------------------------------------------
// File: namedsurface.h
// 
// This class represents named surface with a unique name, 
// that can be assigned to it
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef FRONTEND_SURFACE_H
	#define FRONTEND_SURFACE_H

	#include <QString>

	#include "radiosity/surface.h"

	class NamedSurface : public Surface
	{
	public:
		explicit NamedSurface()
		{

		}

		void setName(const QString& name)
		{
			mName = name;
		}

		const QString& getName() const
		{
			return mName;
		}

	private:
		//! Name of the surface
		QString mName;
	};

#endif // FRONTEND_SURFACE_H