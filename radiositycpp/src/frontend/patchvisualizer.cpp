//--------------------------------------------------------------------------------
// File: patchvisualizer.cpp
// 
// This class represents visualization OpenGL-driven engine for rendering patches
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//---------------------------------------------------------------------------------

#include <sstream>
#include <map>

#include <glut.h>

#include "radiosity/trianglescollection.h"

#include "patchvisualizer.h"

int PatchVisualizer::mWindowWidth  = 0;
int PatchVisualizer::mWindowHeight = 0;

PatchVisualizer::ProjectionParams PatchVisualizer::mProjection;
PatchVisualizer::RenderParams			PatchVisualizer::mRender;
PatchVisualizer::InputData				PatchVisualizer::mInput;


// GLUT callbacks
void PatchVisualizer::onDisplay()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
	glMatrixMode(GL_MODELVIEW);                   
	glLoadIdentity();                             

	// Move away from cam
	glTranslatef(mInput.Shifts.x(), mInput.Shifts.y(), 0.0f);
	glTranslatef(mInput.Shifts.x(), mInput.Shifts.y(), mInput.NewPosition.z());
	// Rotate object according to cursor
	glRotatef(mInput.NewPosition.y(), 1.0, 0.0, 0.0);
	glRotatef(mInput.NewPosition.x(), 0.0, 1.0, 0.0);

	glCallList(mRender.CallListId);

	glFlush();
	glutSwapBuffers();
}

void PatchVisualizer::onReshape(int width, int height)
{
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(mProjection.FOV, static_cast<double>(width) / static_cast<double>(height), mProjection.Near, mProjection.Far);
	glMatrixMode(GL_MODELVIEW);

	mWindowWidth  = width;
	mWindowHeight = height;
}

void PatchVisualizer::onKeyboard(unsigned char key, int x, int y)
{
	if (key>='A' && key<='Z') key+='a'-'A';       /* convert upper char on lower */

	switch (key) 
	{                                
	case '1' : // Vertices
		glPolygonMode(GL_FRONT, GL_POINT);
		glPolygonMode(GL_BACK, GL_POINT);
		glutPostRedisplay();
		break;
	case '2' : // Wireframe
		glPolygonMode(GL_FRONT, GL_LINE);
		glPolygonMode(GL_BACK, GL_LINE);
		glutPostRedisplay();
		break;
	case '3' : // Polygons
		glPolygonMode(GL_FRONT, GL_FILL);
		glPolygonMode(GL_BACK, GL_FILL);
		glutPostRedisplay();
		break;
	case '5' : // Decrease FOV
		if (mProjection.FOV > 0.f) 
		{
			--mProjection.FOV;
		}
		onReshape(mWindowWidth, mWindowHeight);
		glutPostRedisplay();
		break;
	case '6' : // Increase FOV
		if (mProjection.FOV < 180.f) 
		{
			++mProjection.FOV;     
		}
		onReshape(mWindowWidth, mWindowHeight);
		glutPostRedisplay();
		break;
	case '7' :             
		if (mRender.LineWidth > 0.f) 
		{
			mRender.LineWidth -= 0.5f;	
		}
		glLineWidth(mRender.LineWidth);                
		glPointSize(mRender.LineWidth);                
		onReshape(mWindowWidth, mWindowHeight);
		glutPostRedisplay();
		break;
	case '8' :             
		if (mRender.LineWidth < 10.f)
		{
			mRender.LineWidth += 0.5f;
		}
		glLineWidth(mRender.LineWidth);                
		glPointSize(mRender.LineWidth);                
		onReshape(mWindowWidth, mWindowHeight);
		glutPostRedisplay();
		break;
	case 27 :                  
	case 'q' :
		exit(0);                
		break;
	case 'f' :
		glutFullScreen();     
		break;
	case 'w' :
		glutReshapeWindow(mWindowWidth, mWindowHeight);
		break;
	default:
		break;
	}
}

void PatchVisualizer::onSpecial(int key, int, int)
{
	switch(key)
	{
	case GLUT_KEY_UP:
		if (mInput.Shifts.y() > -50.f) 
		{ 
			mInput.Shifts.setY(mInput.Shifts.y() - 1);
		}
		onReshape(mWindowWidth, mWindowHeight);
		glutPostRedisplay();
		break;
	case GLUT_KEY_DOWN:
		if (mInput.Shifts.y() < 50.f) 
		{ 
			mInput.Shifts.setY(mInput.Shifts.y() + 1);
		}
		onReshape(mWindowWidth, mWindowHeight);
		glutPostRedisplay();
		break;
	case GLUT_KEY_LEFT:
		if (mInput.Shifts.x() < 50.f) 
		{ 
			mInput.Shifts.setX(mInput.Shifts.x() + 1);
		}
		onReshape(mWindowWidth, mWindowHeight);
		glutPostRedisplay();
		break;
	case GLUT_KEY_RIGHT:
		if (mInput.Shifts.x() > -50.f) 
		{ 
			mInput.Shifts.setX(mInput.Shifts.x() - 1);
		}
		onReshape(mWindowWidth, mWindowHeight);
		glutPostRedisplay();
		break;
	}	
}

void PatchVisualizer::onMouseClick(int button, int, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON) 
	{           
		if (mInput.MouseState == GLUT_DOWN) 
		{         
			mInput.MouseState = 1;          
			mInput.MousePosition.setX(static_cast<float>(x));
			mInput.MousePosition.setY(static_cast<float>(y));
		}
		else 
		{                                
			mInput.MouseState = 0;         
			mInput.OldPosition.setX(mInput.NewPosition.x());
			mInput.OldPosition.setY(mInput.NewPosition.y());
		}
	}
	if (button == GLUT_RIGHT_BUTTON) 
	{            
		if (mInput.MouseState == GLUT_DOWN) 
		{          
			mInput.MouseState = 2;
			mInput.MousePosition.setZ(y);
		}
		else 
		{
			mInput.MouseState = 0;
			mInput.OldPosition.setZ(mInput.NewPosition.z());
		}
	}
	glutPostRedisplay();
}

void PatchVisualizer::onMouseMotion(int x, int y)
{
	if (mInput.MouseState == 1) 
	{ 
		mInput.NewPosition.setX(mInput.OldPosition.x() + x - mInput.MousePosition.x());
		mInput.NewPosition.setY(mInput.OldPosition.y() + y - mInput.MousePosition.y());

		glutPostRedisplay();
	}
	if (mInput.MouseState == 2) 
	{
		mInput.NewPosition.setZ(mInput.OldPosition.z() + y - mInput.MousePosition.z());
		glutPostRedisplay();
	}
}

void PatchVisualizer::createGLUTWindow()
{
	// Initialize common rendering params
	mInput.MouseState = 0;

	mProjection.FOV  = 45.f;
	mProjection.Near = 1.f;
	mProjection.Far  = 1000.f;

	mRender.LineWidth = 1.f;
	mRender.PointSize = 1.f;
	mRender.Solid			= true;

	mWindowWidth  = 1024;
	mWindowHeight = 768;

	mRender.CallListId = 1;

	char* dummyArgv[1];
	dummyArgv[0] = const_cast<char *>("run");
	int		dummyArgc = 1;

	glutInit(&dummyArgc, dummyArgv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);

	glutInitWindowSize(mWindowWidth, mWindowHeight);

	glutCreateWindow("radiosity");

	glutDisplayFunc(onDisplay);
	glutReshapeFunc(onReshape);
	glutKeyboardFunc(onKeyboard);
	glutSpecialFunc(onSpecial);
	glutMouseFunc(onMouseClick);
	glutMotionFunc(onMouseMotion);

	glClearColor(0.f, 0.f, 0.f, 1.f);
	glClearDepth(1.f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);

	glLineWidth(mRender.LineWidth);
	glPointSize(mRender.PointSize);
}

void PatchVisualizer::render(std::shared_ptr< TrianglesCollection > triangles)
{
	createGLUTWindow();
	std::vector< GLTriangle > glTriangles;
	createGLTriangles(triangles, &glTriangles);
	createRenderLists(glTriangles);

	glutMainLoop();
}

void PatchVisualizer::createRenderLists(const std::vector< GLTriangle >& triangles)
{
	glNewList(mRender.CallListId, GL_COMPILE);

	// Render all triangles
	glBegin(GL_TRIANGLES);
	for (auto tri = triangles.begin(); tri != triangles.end(); ++tri)
	{
		const GLTriangle& triangle = *tri;
		glColor3f(triangle.ColorA.r(), triangle.ColorA.g(), triangle.ColorA.b());
		glVertex3f(triangle.A.x(), triangle.A.y(), triangle.A.z());
		glColor3f(triangle.ColorB.r(), triangle.ColorB.g(), triangle.ColorB.b());
		glVertex3f(triangle.B.x(), triangle.B.y(), triangle.B.z());
		glColor3f(triangle.ColorC.r(), triangle.ColorC.g(), triangle.ColorC.b());
		glVertex3f(triangle.C.x(), triangle.C.y(), triangle.C.z());
	}
	glEnd();

	glEndList();
}

namespace
{
	inline std::string vertexToString(const Vector3D& v)
	{
		std::ostringstream stream;
		stream << v.x() << "," << v.y() << "," << v.z();
		return stream.str();
	}
}

void PatchVisualizer::createGLTriangles(std::shared_ptr< TrianglesCollection > triangles, std::vector< GLTriangle >* out)
{
	out->reserve(triangles->size());
	for (auto tri = triangles->begin(); tri != triangles->end(); ++tri)
	{
		GLTriangle glTri;

		glTri.A = tri->A;
		glTri.B = tri->B;
		glTri.C = tri->C;

		glTri.ColorA = glTri.ColorB = glTri.ColorC = tri->TheMaterial.Radiosity;
		out->push_back(glTri);
	}

	// Interpolate colors from patches to per-vertex colors of GL triangles
	typedef std::vector< Color* > ColorPtrVector;
	typedef std::map<std::string, ColorPtrVector> VertexColorMap;

	VertexColorMap vertices;

	for (auto tri = out->begin(); tri != out->end(); ++tri)
	{
		ColorPtrVector& colorsa = vertices[vertexToString(tri->A)];
		ColorPtrVector& colorsb = vertices[vertexToString(tri->B)];
		ColorPtrVector& colorsc = vertices[vertexToString(tri->C)];

		colorsa.push_back(&tri->ColorA);
		colorsb.push_back(&tri->ColorB);
		colorsc.push_back(&tri->ColorC);
	}

	for (auto v = vertices.begin(); v != vertices.end(); ++v)
	{
		ColorPtrVector& colors = v->second;

		Color sum;
		int   count = 0;
		auto  color = colors.begin();
		for (; color != colors.end(); ++color)
		{
			Color& c = **color;

			// Don't update count in case black color
			if (c == Color(0.f, 0.f, 0.f))
			{
				continue;
			}

			sum += c;
			++count;
		}

		if (count > 1)
		{
			sum *= static_cast<float>(1.f / count);
		}

		color = colors.begin();

		for (; color != colors.end(); ++color)
		{
			**color = sum;
		}
	}

	for (auto tri = out->begin(); tri != out->end(); ++tri)
	{
		GLTriangle& glTri = *tri;
		glTri.RawA = glTri.ColorA;
		glTri.RawB = glTri.ColorB;
		glTri.RawC = glTri.ColorC;
	}

	for (int idx = 0, count = out->size(); idx < count; ++idx)
	{
		GLTriangle& glTri = (*out)[idx];

		Color sum(0.f, 0.f, 0.f);
		sum += glTri.ColorA;
		sum += glTri.ColorB;
		sum += glTri.ColorC;

		sum *= 0.3333333333f;

		glTri.ColorA = sum;
		glTri.ColorB = sum;
		glTri.ColorC = sum;
	}

	for (auto v = vertices.begin(); v != vertices.end(); ++v)
	{
		ColorPtrVector& colors = v->second;

		Color sum;
		for (auto color = colors.begin(); color != colors.end(); ++color)
		{
			sum += **color;
		}
		sum *= static_cast<float>(1.f / colors.size());
		for (auto color = colors.begin(); color != colors.end(); ++color)
		{
			**color = sum;
		}
	}

	//const int trianglesCount = triangles->size();

	//// Vertex -> color mapping
	//std::vector< std::vector<Color> > colors(3 * trianglesCount);

	//int vertexIndex   = 0;	
	//for (auto tri = triangles->begin(); tri != triangles->end(); ++tri)
	//{
	//	const PatchTriangle& triangle = *tri;

	//	colors[vertexIndex + 0].push_back(triangle.TheMaterial.Radiosity);
	//	colors[vertexIndex + 1].push_back(triangle.TheMaterial.Radiosity);
	//	colors[vertexIndex + 2].push_back(triangle.TheMaterial.Radiosity);

	//	vertexIndex += 3;
	//}

	//for (auto vertex = colors.begin(); vertex != colors.end(); ++vertex)
	//{
	//	std::vector< Color >& vertexColors = *vertex;

	//	Color sum;
	//	int   count = 0;
	//	for each (auto color in vertexColors)
	//	{
	//		if (fabs(color.r()) < FLOAT_ZERO && fabs(color.g()) < FLOAT_ZERO && fabs(color.b()) < FLOAT_ZERO)
	//		{
	//			continue;
	//		}

	//		sum += color;
	//		++count;
	//	}

	//	sum *= (1.f / count);

	//	for (auto color = vertexColors.begin(); color != vertexColors.end(); ++color)
	//	{
	//		*color = sum;
	//	}
	//}

	//// Create gl triangles with initial colors
	//vertexIndex = 0;
	//for (auto tri = triangles->begin(); tri != triangles->end(); ++tri)
	//{
	//	GLTriangle glTri;

	//	glTri.A = tri->A;
	//	glTri.B = tri->B;
	//	glTri.C = tri->C;
	//		 
	//	glTri.ColorA = colors[vertexIndex + 0][0];
	//	glTri.ColorB = colors[vertexIndex + 1][0];
	//	glTri.ColorC = colors[vertexIndex + 2][0];

	//	out->push_back(glTri);

	//	vertexIndex += 3;
	//}

	//// Interpolate colors for vertices
	//for (auto tri = out->begin(); tri != out->end(); ++tri)
	//{
	//	Color sum;

	//	sum += tri->ColorA;
	//	sum += tri->ColorB;
	//	sum += tri->ColorC;

	//	sum *= 0.333333333f;

	//	tri->ColorA = sum;
	//	tri->ColorB = sum;
	//	tri->ColorC = sum;
	//}
}


