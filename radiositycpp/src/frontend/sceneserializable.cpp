//-------------------------------------------------------------------
// File: sceneserializable.cpp
// 
// Qt based XML radiosity scene serializable implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include <memory>
#include <iostream>

#include <QFile>
#include <QMap>

#include "geometry/matrix4x4.h"

#include "illumination/material.h"

#include "radiosity/scene.h"
#include "radiosity/surface.h"

#include "cubesurface.h"
#include "objsurface.h"
#include "spheresurface.h"
#include "trianglesurface.h"
#include "trianglescollectionsurface.h"

#include "sceneserializable.h"

namespace
{
	std::ostream& GDumpErrorMessage(const QDomNode& node, const QDomNode& parent, const std::string& message)
	{
		QString nodeName("undefined");
		QString parentName("undefined");
		if (node.isElement())
			nodeName = node.toElement().tagName();
		if (parent.isElement())
			parentName = node.toElement().tagName();
		std::cerr << "Error reading scene at node: <" << nodeName.toUtf8().constData() << "> with parent: <" << parentName.toUtf8().constData() << ">." << std::endl;
		std::cerr << message << std::endl;
		return std::cerr;
	}

	// Local readers for different scene entities
	struct VectorReader : public IXmlSerializable
	{
		Vector3D readPosition(const QDomNode *node, bool* ok)
		{
			*ok = read(node);
			if (*ok)
			{
				return Vector;
			}
			return Vector3D();
		}

		virtual bool read(const QDomNode *node)
		{
			float x, y, z;
			bool ok = true;
			ok = readAttribute(*node, "x", x) && ok;
			ok = readAttribute(*node, "y", y) && ok;
			ok = readAttribute(*node, "z", z) && ok;
			Vector.setXYZ(x, y, z);

			return ok;
		}

		bool write(QDomDocument* document, QDomNode* node) const
		{
			return false;
		}


		Vector3D Vector;
	};

	struct ColorReader : public IXmlSerializable
	{
		Color readColor(const QDomNode *node, bool* ok)
		{
			*ok = read(node);
			if (*ok)
			{
				return C;
			}
			return Color();
		}

		virtual bool read(const QDomNode *node)
		{
			float r, g, b;
			bool ok = true;
			ok = readAttribute(*node, "r", r) && ok;
			ok = readAttribute(*node, "g", g) && ok;
			ok = readAttribute(*node, "b", b) && ok;
			C.setRGB(r, g, b);

			return ok;
		}

		bool write(QDomDocument* document, QDomNode* node) const
		{
			return false;
		}

		Color C;
	};


	struct MaterialReader : public IXmlSerializable
	{
		MaterialReader()
		{
		}

		virtual bool read(const QDomNode* node)
		{
			QDomNode readNode = node->firstChild();

			bool ok = true;
			ColorReader reader;

			// <emission>
			ObjMaterial.Emission = reader.readColor(&readNode, &ok);
			if (!ok)
			{
				GDumpErrorMessage(readNode, *node, "Failed reading emission of the material!");
				return false;
			}
			readNode = readNode.nextSibling();
			// <reflection>
			ObjMaterial.Reflection = reader.readColor(&readNode, &ok);
			if (!ok)
			{
				GDumpErrorMessage(readNode, *node, "Failed reading reflection of the material!");
				return false;
			}
			readNode = readNode.nextSibling();
			// <radiosity>
			ObjMaterial.Radiosity = reader.readColor(&readNode, &ok);
			if (!ok)
			{
				GDumpErrorMessage(readNode, *node, "Failed reading radiosity of the material!");
				return false;
			}
			readNode = readNode.nextSibling();

			// TODO: Diffuse textures
			//readNode = readNode.nextSibling();
			//// <texture>
			//// Read texture, if name if given
			//if (!readNode.isNull())
			//{
			//	QString textureName;
			//	ok = readAttribute(readNode, "file_name", textureName);
			//	if (!ok)
			//	{
			//		GDumpErrorMessage(readNode, *node, "Failed reading material texture file name!");
			//		return false;
			//	}
			//	if (ok && !textureName.isEmpty())
			//	{
			//		TextureReader reader;
			//		if (reader.read(textureName))
			//		{
			//			ObjMaterial->DiffuseTexture = reader.ObjTexture;
			//		}
			//	}

			//	readNode = readNode.nextSibling();
			//	// Read texture scales
			//	// <texscaleu>
			//	ok = readAttribute(readNode, "scale", ObjMaterial->TexScaleU) && ok;
			//	if (!ok)
			//	{
			//		GDumpErrorMessage(readNode, *node, "Failed reading texture U coordinate scale!");
			//		return false;
			//	}
			//	readNode = readNode.nextSibling();
			//	// <texscalev>
			//	ok = readAttribute(readNode, "scale", ObjMaterial->TexScaleV) && ok;
			//	if (!ok)
			//	{
			//		GDumpErrorMessage(readNode, *node, "Failed reading texture V coordinate scale!");
			//		return false;
			//	}
			//}

			return ok;
		}

		bool write(QDomDocument* document, QDomNode* node) const
		{
			return false;
		}

		Material ObjMaterial; 
	};


	struct TriangleReader : public IXmlSerializable
	{
		TriangleReader(bool createObjTriangle) 
			: CreateObjTriangle(createObjTriangle)
		{
		}

		virtual bool read(const QDomNode* node)
		{
			Vector3D v0;
			Vector3D v1;
			Vector3D v2;

			// Read name of the surface
			QString name;
			readAttribute(*node, "name", name);

			QDomNode readNode = node->firstChild();

			bool ok = true;
			VectorReader reader;
			// Read vertices
			// <pos>
			v0 = reader.readPosition(&readNode, &ok);
			if (!ok)
			{
				GDumpErrorMessage(readNode, *node, "Failed reading first triangle vertex!");
				return false;
			}
			readNode = readNode.nextSibling();
			// <pos>
			v1 = reader.readPosition(&readNode, &ok);
			if (!ok)
			{
				GDumpErrorMessage(readNode, *node, "Failed reading second triangle vertex!");
				return false;
			}
			readNode = readNode.nextSibling();
			// <pos>
			v2 = reader.readPosition(&readNode, &ok); 
			if (!ok)
			{
				GDumpErrorMessage(readNode, *node, "Failed reading third triangle vertex!");
				return false;
			}
			readNode = readNode.nextSibling();
			// Read materials
			MaterialReader matReader;
			ok = matReader.read(&readNode);

			if (ok)
			{
				PatchTriangle triangle;
				triangle.A					 = v0;
				triangle.B					 = v1;
				triangle.C					 = v2;
				triangle.TheMaterial = matReader.ObjMaterial;
				if (CreateObjTriangle)
				{
					ObjTriangle = std::shared_ptr< TriangleSurface >(new TriangleSurface(triangle, name));
				}
				Patch = triangle;
			}
			else
			{
				GDumpErrorMessage(readNode, *node, "Failed reading triangle material!");
			}

			return true;
		}

		bool write(QDomDocument* document, QDomNode* node) const
		{
			return false;
		}

		std::shared_ptr< TriangleSurface > ObjTriangle;
		PatchTriangle											 Patch;							// Cache patch triangle instance
		bool															 CreateObjTriangle; // True, if surface should be created
	};

	struct TrianglesCollectionReader : public IXmlSerializable
	{
		TrianglesCollectionReader() 
		{
		}

		virtual bool read(const QDomNode* node)
		{
			// Read name of the surface
			QString name;

			if (!readAttribute(*node, "name", name))
			{
				GDumpErrorMessage(*node, node->parentNode(), "Failed reading triangle surface name!");
				return false;
			}


			QDomNode readNode = node->firstChild();

			std::shared_ptr< TrianglesCollection > triangles(new TrianglesCollection);

			bool ok = true;


			while (!readNode.isNull() && readNode.toElement().tagName() != "material")
			{
				TriangleReader reader(false);

				if (!reader.read(&readNode))
				{
					GDumpErrorMessage(readNode, *node, "Failed reading triangles collection triangle!");
					return false;
				}

				triangles->add(reader.Patch);

				readNode = readNode.nextSibling();
			}


			if (ok)
			{
				ObjTriangles = std::shared_ptr< TrianglesCollectionSurface >(new TrianglesCollectionSurface(triangles, name));
			}
			else
			{
				GDumpErrorMessage(readNode, *node, "Failed reading triangles collection material!");
			}

			return true;
		}

		bool write(QDomDocument* document, QDomNode* node) const
		{
			return false;
		}

		std::shared_ptr< TrianglesCollectionSurface > ObjTriangles;
	};

	struct CubeReader : public IXmlSerializable
	{
		CubeReader() 
		{
		}

		virtual bool read(const QDomNode* node)
		{
			// Read name of the surface
			QString name;

			if (!readAttribute(*node, "name", name))
			{
				GDumpErrorMessage(*node, node->parentNode(), "Failed reading cube name!");
				return false;
			}

			QDomNode readNode = node->firstChild();

			bool ok = false;
			// Read materials
			MaterialReader matReader;
			ok = matReader.read(&readNode);

			if (ok)
			{
				ObjCube.reset(new CubeSurface(matReader.ObjMaterial, name));
			}
			else
			{
				GDumpErrorMessage(readNode, *node, "Failed reading cube material!");
			}

			return true;
		}

		bool write(QDomDocument* document, QDomNode* node) const
		{
			return false;
		}

		std::shared_ptr< CubeSurface > ObjCube;
	};

	struct SphereReader : public IXmlSerializable
	{
		SphereReader() 
		{
		}

		virtual bool read(const QDomNode* node)
		{
			// Read name of the surface
			QString name;

			if (!readAttribute(*node, "name", name))
			{
				GDumpErrorMessage(*node, node->parentNode(), "Failed reading sphere name!");
				return false;
			}

			QDomNode readNode = node->firstChild();

			bool ok = false;
			// Read materials
			MaterialReader matReader;
			ok = matReader.read(&readNode);

			if (ok)
			{
				ObjSphere.reset(new SphereSurface(matReader.ObjMaterial, name));
			}
			else
			{
				GDumpErrorMessage(readNode, *node, "Failed reading sphere material!");
			}

			return true;
		}

		bool write(QDomDocument* document, QDomNode* node) const
		{
			return false;
		}

		std::shared_ptr< SphereSurface > ObjSphere;
	};

	class ObjLoader
	{
		struct Vertex 
		{
			Vector3D Position;
			Vector3D Normal;
			Vector3D TexCoords; // Actually 2 dimensional
		};

	public:
		ObjLoader()
		{
		}

		bool read(const QString& fileName)
		{
			// Read obj file and construct triangles
			QFile modelFile(fileName);

			modelFile.open(QIODevice::ReadOnly);
			if (!modelFile.isOpen())
			{
				std::cerr << "Failed loading model file: " << fileName.toUtf8().constData() << std::endl;
				return false;
			}

			std::vector<Vector3D> positions;
			std::vector<Vector3D> normals;
			std::vector<Vector3D> texCoords;

			std::vector<Vertex>	  faceVertices;
			std::vector<unsigned> faceVerticesIndices;
			std::vector<unsigned> faceIndices;

			// Result indices for triangles construction
			std::vector< unsigned > indices;
			// Result vertices for triangles construction
			std::vector< Vertex >		vertices;

			for(;;)
			{
				QString command = modelFile.readLine();
				if (modelFile.atEnd())
				{
					break;
				}

				if (command.contains("#"))
				{
					// Comment
				}
				else if (command.startsWith("v "))
				{
					// Vertex
					// Also apply translation
					Vector3D modelPosition = fromObjLine(command, "v");
					positions.push_back(modelPosition);
				}
				else if (command.startsWith("vt "))
				{
					// Texcoords
					texCoords.push_back(fromObjLine(command, "vt"));
				}
				else if (command.startsWith("vn "))
				{
					// Normals
					normals.push_back(fromObjLine(command, "vn"));
				}
				else if (command.startsWith("f "))
				{
					faceVertices.clear();
					faceVerticesIndices.clear();

					QStringList indicesDescs = toIndicesDescriptor(command, "f");

					foreach (const QString& index, indicesDescs)
					{
						unsigned position, texcoord, normal;
						Vertex v;

						toIndices(index, &position, &normal, &texcoord);

						// OBJ uses 1-based arrays
						if (!positions.empty())
							v.Position  = positions[position - 1];
						if (!normals.empty())
							v.Normal    = normals[normal - 1];
						if (!texCoords.empty())
							v.TexCoords = texCoords[texcoord - 1];

						faceVertices.push_back(v);
						faceVerticesIndices.push_back(position);
					}
					const unsigned count = faceVertices.size();
					faceIndices.resize(count);
					for (unsigned idx = 0; idx < count; ++idx)
					{
						// Triangle strip
						if (idx > 2)
						{
							indices.push_back( faceIndices[0] );
							indices.push_back( faceIndices[idx - 1] );
						}
						faceIndices[idx] = vertices.size();
						vertices.push_back(faceVertices[idx]);
						indices.push_back(faceIndices[idx]);
					}
				}

				// Also commands can be "mtlib" and "usemtl", but we do not support them
			}

			// Now create triangles, we need only positions, maybe for now
			for (int idx = 0, count = indices.size(); idx < count; idx += 3)
			{
				Vertex a = vertices[indices[idx]];
				Vertex b = vertices[indices[idx + 1]];
				Vertex c = vertices[indices[idx + 2]];

				Triangle tri;
				tri.A = a.Position;
				tri.B = b.Position;
				tri.C = c.Position;

				// Material isn't assigned to triangle, it's owned by the model
				mTriangles.push_back(tri);
			}

			return true;
		}

		const std::vector< Triangle > getTriangles() const
		{
			return mTriangles;
		}

	private:
		Vector3D fromObjLine(QString line /* Will be modified */, const QString& prefix)
		{
			QString			vectorString = line.remove(prefix + " ").remove("\n").remove("\r");
			QStringList coords			 = vectorString.split(" ", QString::SkipEmptyParts);
			// Remove spaces
			coords.removeAll(" ");
			coords.removeAll("");
			coords.removeAll("\n");
			if (coords.size() == 3)
				return Vector3D(coords[0].toFloat(), coords[1].toFloat(), coords[2].toFloat());

			// For texcoords
			return Vector3D(coords[0].toFloat(), coords[1].toFloat(), 0.f);
		}

		QStringList toIndicesDescriptor(QString line, const QString& prefix)
		{
			QString			 indicesString = line.remove(prefix + " ");
			QStringList	 descs = indicesString.split(" ");
			return descs;
		}

		void toIndices(const QString& line, unsigned *position, unsigned *normal, unsigned *texcoord)
		{
			QStringList indices = line.split("/");
			*position = indices[0].toUInt();
			*texcoord = indices[1].toUInt(); // Texcoord is second 
			*normal   = indices[2].toUInt();
		}

	private:
		
		std::vector< Triangle >				mTriangles;
	};

	struct ModelReader : public IXmlSerializable
	{
		ModelReader() 
		{
		}

		virtual bool read(const QDomNode* node)
		{
			// Read name of the surface
			QString name;

			if (!readAttribute(*node, "name", name))
			{
				GDumpErrorMessage(*node, node->parentNode(), "Failed reading sphere name!");
				return false;
			}

			QString  fileName;
			QDomNode readNode = node->firstChild();

			
			bool ok = true;
			
			// Read model file name
			// <model>
			ok = readAttribute(readNode, "name", fileName);

			if (!ok)
			{
				GDumpErrorMessage(readNode, *node, "Failed reading model file name!");
				return false;
			}

			ObjLoader loader;

			if (!loader.read(fileName))
			{
				GDumpErrorMessage(readNode, *node, "Failed reading model!");
				return false;
			}

			readNode = readNode.nextSibling();

			// Read materials
			MaterialReader matReader;
			ok = matReader.read(&readNode);

			if (ok)
			{
				Obj.reset(new ObjSurface(loader.getTriangles(), matReader.ObjMaterial, name));
			}
			else
			{
				GDumpErrorMessage(readNode, *node, "Failed reading model material!");
			}

			return ok;
		}

		virtual bool write(QDomDocument* document, QDomNode* node) const
		{
			return false;
		}

		std::shared_ptr< ObjSurface > Obj;
	};


	struct TransformReader : public IXmlSerializable
	{
		TransformReader()
		{
		}

		virtual bool read(const QDomNode* node)
		{
			QDomNode readNode = node->firstChild();

			bool ok = false;

			// Read translation, scale and rotation
			VectorReader reader;

			Vector3D translate;
			// <translate>
			translate = reader.readPosition(&readNode, &ok);
			if (!ok)
			{
				GDumpErrorMessage(readNode, *node, "Failed reading translation of the object or scene!");
				return false;
			}

			readNode = readNode.nextSibling();

			Vector3D scale;
			// <scale>
			scale = reader.readPosition(&readNode, &ok);
			if (!ok)
			{
				GDumpErrorMessage(readNode, *node, "Failed reading scale of the object or scene!");
				return false;
			}

			readNode = readNode.nextSibling();

			Vector3D axis;
			float    angle;
			// <rotation>
			axis = reader.readPosition(&readNode, &ok);
			if (!ok)
			{
				GDumpErrorMessage(readNode, *node, "Failed reading rotation axis of the object or scene!");
				return false;
			}

			ok = readAttribute(readNode, "angle", angle);
			if (!ok)
			{
				GDumpErrorMessage(readNode, *node, "Failed reading rotation angle of the object or scene!");
				return false;
			}

			readNode = readNode.nextSibling();

			
			// Now, if node exists, read name of the surfaces
			if (!readNode.isNull())
			{
				// <surface>
				ok = readAttribute(readNode, "name", Name);
			}

			Matrix4x4 transform;

			M4x4Translation(translate, &transform);
			M4x4Scale(scale, &transform);
			M4x4Rotation(axis, angle, &transform);

			// Store transform
			WorldTransform = transform; 

			return true;
		}

		virtual bool write(QDomDocument* document, QDomNode* node) const
		{
			return false;
		}

		QString										 Name;
		Matrix4x4									 WorldTransform; // Transform, not applied to the surface, but to the whole world
	};

	struct SceneTransformReader : public IXmlSerializable
	{
		SceneTransformReader()
		{
		}

		virtual bool read(const QDomNode* node)
		{
			QDomNode readNode = node->firstChild();

			while (!readNode.isNull())
			{
				TransformReader reader;

				if (!reader.read(&readNode))
				{
					GDumpErrorMessage(readNode, *node, "Failed reading transformation matrix!");
					return false;
				}

				if (reader.Name.isEmpty())
				{
					WorldTransform = reader.WorldTransform * WorldTransform;
				}
				else
				{
					TransformsMap[reader.Name] = reader.WorldTransform * TransformsMap[reader.Name];
				}

				readNode = readNode.nextSibling();
			}
			
			return true;
		}

		virtual bool write(QDomDocument* document, QDomNode* node) const
		{
			return false;
		}

		Matrix4x4									 WorldTransform; // Transform, not applied to the surface, but to the whole world
		QMap< QString, Matrix4x4 > TransformsMap;
	};
}

SceneSerializable::SceneSerializable()
{

}

SceneSerializable::~SceneSerializable()
{

}

QSharedPointer< Scene > SceneSerializable::readScene(const QString& fileName)
{
	QFile sceneFile(fileName);

	sceneFile.open(QIODevice::ReadOnly);
	if (!sceneFile.isOpen())
	{
		std::cerr << "Can't open scene file " << fileName.toUtf8().constData() << std::endl;
		return mScene;
	}

	QDomDocument document;

	if (!document.setContent(&sceneFile))
	{
		std::cerr << "Malformed scene file! Please, check XML syntax!" << std::endl;
		return mScene;
	}

	// Read xml document
	QDomElement docElem = document.documentElement();

	if (!read(&docElem))
	{
		std::cerr << "Scene reading failed!" << std::endl;
		mScene = QSharedPointer< Scene >();
		return mScene;
	}


	return mScene;
}

bool SceneSerializable::read(const QDomNode* node)
{
	mScene = QSharedPointer< Scene >(new Scene());

	// We must be in <scene> node, so start reading from first child
	QDomNode currentNode = node->firstChild();

	// Store map of surfaces, so that we'll update transformation matrix of them
	QMap< QString, std::shared_ptr< NamedSurface > > surfaces;

	while (!currentNode.isNull())
	{
		QDomElement element = currentNode.toElement();
		const QString& tag  = element.tagName();

		if (tag == "triangle")
		{
			TriangleReader reader(true);
			if (!reader.read(&currentNode))
			{
				return false;
			}
			surfaces[reader.ObjTriangle->getName()] = reader.ObjTriangle;
		}
		else if (tag == "triangles")
		{
			TrianglesCollectionReader reader;
			if (!reader.read(&currentNode))
			{
				return false;
			}
			surfaces[reader.ObjTriangles->getName()] = reader.ObjTriangles;
		}
		else if (tag == "cube")
		{
			CubeReader reader;
			if (!reader.read(&currentNode))
			{
				return false;
			}
			surfaces[reader.ObjCube->getName()] = reader.ObjCube;
		}
		else if (tag == "sphere")
		{
			SphereReader reader;
			if (!reader.read(&currentNode))
			{
				return false;
			}
			surfaces[reader.ObjSphere->getName()] = reader.ObjSphere;
		}
		else if (tag == "model")
		{
			ModelReader reader;
			if (!reader.read(&currentNode))
			{
				return false;
			}
			surfaces[reader.Obj->getName()] = reader.Obj;
		}
		else if (tag == "transforms")
		{
			SceneTransformReader reader;
			if (!reader.read(&currentNode))
			{
				return false;
			}
			for (auto m = reader.TransformsMap.begin(); m != reader.TransformsMap.end(); ++m)
			{
				if (!surfaces.contains(m.key()))
				{
					GDumpErrorMessage(currentNode, currentNode, "Transform was set to undefined object!");
					return false;
				}
				surfaces[m.key()]->setTransform(m.value());
				surfaces[m.key()]->applyTransformation();
			}
		}

		currentNode = currentNode.nextSibling();
	}

	// Copy all loaded surfaces to the scene
	for each (auto surface in surfaces)
	{
		mScene->addSurface(surface);
	}

	return true;
}

bool SceneSerializable::write(QDomDocument* document, QDomNode* node) const
{
	Q_ASSERT(!"Radiosity scene can't be written!");
	return false;
}
