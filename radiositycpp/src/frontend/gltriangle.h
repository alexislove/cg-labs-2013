//-------------------------------------------------------------------
// File: gltriangle.h
// 
// This class extends geometry triangle with per-vertex colors
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef FRONTEND_GLTRIANGLE_H
	#define FRONTEND_GLTRIANGLE_H

	#include "geometry/triangle.h"
	#include "illumination/color.h"
	
	struct GLTriangle : public Triangle
	{
		GLTriangle(const Color& color = Color())
			: ColorA(color),
				ColorB(color),
				ColorC(color)
		{
		}

		GLTriangle(const Color& a, const Color& b, const Color& c)
			: ColorA(a),
				ColorB(b),
				ColorC(c)
		{
		}

		// Interpolated colors
		Color ColorA;
		Color ColorB;
		Color ColorC;
		// Raw colors
		Color RawA;
		Color RawB;
		Color RawC;
	};

#endif // FRONTEND_GLTRIANGLE_H

