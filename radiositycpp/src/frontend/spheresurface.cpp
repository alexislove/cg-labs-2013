//-------------------------------------------------------------------
// File: spheresurface.cpp
// 
// This class represents sphere surface
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#define _USE_MATH_DEFINES
#include <math.h>

#include "radiosity/trianglescollection.h"

#include "spheresurface.h"

SphereSurface::SphereSurface(const Material& material, const QString& name)
	: mMaterial(material)
{
	setName(name);
	initialize();
}

void SphereSurface::initialize()
{
	std::shared_ptr< TrianglesCollection > triangles(new TrianglesCollection);

	static const int cSegmentsCount = 20;
	static const float cRadius		  = 0.5f;

	// Store vertices, calculated on the last iteration
	Vector3D ringVertices[cSegmentsCount + 3];
	Vector3D current;
	
	// Accumulate quads
	Vector3D vertices[4];
	for (int idx = 0; idx < cSegmentsCount / 2 + 1; ++idx)
	{
		const float angle = 2 * M_PI / cSegmentsCount * idx;
		float y  = cRadius * cosf(angle);
		float z0 = cRadius * sinf(angle);
		
		for (int jdx = 0; jdx < cSegmentsCount + 3; ++jdx)
		{
			float angle = 2 * M_PI / cSegmentsCount * jdx;
			float x = z0 * cosf(angle);
			float z = z0 * sinf(angle);

			Vector3D v(x, y, z);

			if (idx == 0)
			{
				ringVertices[jdx] = Vector3D(0.f, cRadius, 0.f);
			}
			else
			{
				if (jdx == 0)
				{
					current = v;
				}
				else
				{
					vertices[0] = v;
					vertices[1] = current;
					vertices[2] = ringVertices[jdx - 1];
					vertices[3] = ringVertices[jdx];

					if (jdx == cSegmentsCount / 2)
					{
						vertices[0].setX(0.f);
						vertices[0].setZ(0.f);
						vertices[1].setX(0.f);
						vertices[1].setZ(0.f);
					}

					/*if (jdx != cSegmentsCount + 2 && jdx != 1)
					{*/
					addQuad(triangles, vertices);
					//}

					ringVertices[jdx - 1] = current;
					current							  = v;
				}
			}
		}
	}

	setTrianglesCollection(triangles);
}

void SphereSurface::addQuad(std::shared_ptr< TrianglesCollection > triangles, Vector3D v[4])
{
	PatchTriangle triangle;
	triangle.TheMaterial = mMaterial;

	// Triangle 1
	triangle.A = v[0];
	triangle.B = v[1];
	triangle.C = v[2];

	if (v[0] != v[1])
	{
		triangles->add(triangle);
	}

	triangle.B = v[2];
	triangle.C = v[3];

	if (v[2] != v[3])
	{
		triangles->add(triangle);
	}
}