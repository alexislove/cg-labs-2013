//-------------------------------------------------------------------
// File: sceneserializable.h
// 
// Qt based XML radiosity scene serializable implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef FRONTEND_SCENESERIALIZABLE_H
	#define FRONTEND_SCENESERIALIZABLE_H

	#include <QSharedPointer>

	#include "ixmlserializable.h"

	class Scene;

	class SceneSerializable : private IXmlSerializable // Disable polymorph behavior, here it's not what's desired
	{
	public:
		//! Constructor
		explicit SceneSerializable();

		virtual ~SceneSerializable();

		//! Read scene from given file, returns readed ray tracer scene pointer or invalid pointer if not found
		QSharedPointer< Scene > readScene(const QString& fileName);

	protected:
		// Close methods from the outer world

		// Override serialization methods

		virtual bool read(const QDomNode* node);

		virtual bool write(QDomDocument* document, QDomNode* node) const;

	private:
		//! Scene pointer, shared between methods during reading
		QSharedPointer< Scene > mScene;
	};

#endif // FRONTEND_SCENESERIALIZABLE_H
