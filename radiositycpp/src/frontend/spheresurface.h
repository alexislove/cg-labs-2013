//-------------------------------------------------------------------
// File: spheresurface.h
// 
// This class represents sphere surface
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef FRONTEND_SPHERESURFACE_H
	#define FRONTEND_SPHERESURFACE_H

	#include <memory>

	#include "illumination/material.h"

	#include "namedsurface.h"

	class TrianglesCollection;

	class SphereSurface : public NamedSurface
	{
	public:
		explicit SphereSurface(const Material& material, const QString& name);

	private:
		// Initialize sphere triangles
		void initialize();

		// Add 2 triangles, formed by 4 vertices of the sphere segment
		void addQuad(std::shared_ptr< TrianglesCollection > triangles, Vector3D v[4]);


	private:
		//! Material instance
		Material mMaterial;
	};

#endif // FRONTEND_SPHERESURFACE_H