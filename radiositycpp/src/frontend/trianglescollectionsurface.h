//-------------------------------------------------------------------
// File: trianglesurface.h
// 
// This class represents surface, that contains collection of triangles
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef FRONTEND_TRIANGLESCOLLECTIONSURFACE_H
	#define FRONTEND_TRIANGLESCOLLECTIONSURFACE_H

	#include "radiosity/trianglescollection.h"

	#include "namedsurface.h"

	class TrianglesCollectionSurface : public NamedSurface
	{
	public:
		explicit TrianglesCollectionSurface(std::shared_ptr< TrianglesCollection > collection, const QString& name)
		{
			setName(name);
			setTrianglesCollection(collection);
		}
	};

#endif // FRONTEND_TRIANGLESCOLLECTIONSURFACE_H
