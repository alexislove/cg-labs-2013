//-------------------------------------------------------------------
// File: objsurface.cpp
// 
// This class represents obj model surface
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include "radiosity/trianglescollection.h"

#include "objsurface.h"

ObjSurface::ObjSurface(const std::vector<Triangle>& triangles, const Material& material, const QString& name)
	: mMaterial(material)
{
	setName(name);

	std::shared_ptr< TrianglesCollection > collection(new TrianglesCollection);

	for (auto tri = triangles.begin(); tri != triangles.end(); ++tri)
	{
		const Triangle& aTri = *tri;

		PatchTriangle patchTri;

		patchTri.A = aTri.A;
		patchTri.B = aTri.B;
		patchTri.C = aTri.C;
		patchTri.TheMaterial = mMaterial;

		collection->add(patchTri);
	}

	setTrianglesCollection(collection);
}