//--------------------------------------------------------------------------------
// File: patchvisualizer.h
// 
// This class represents visualization OpenGL-driven engine for rendering patches
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//---------------------------------------------------------------------------------

#ifndef FRONTEND_PATCHVISUALIZER_H
	#define FRONTEND_PATCHVISUALIZER_H

	#include <memory>
	#include <vector>

	#include "geometry/vector3d.h"

	#include "gltriangle.h"

	class PatchVisualizer
	{
		struct ProjectionParams
		{
			float FOV;
			float Near;
			float Far;
		};

		struct InputData
		{
			Vector3D NewPosition;
			Vector3D OldPosition;
			Vector3D MousePosition;
			int			 MouseState;
			Vector3D Shifts;
		};

		struct RenderParams
		{
			int   ObjectType;
			bool  Solid;
			float PointSize;
			float LineWidth;
			int   CallListId;
		};

	public:

		// Initialization methods
		static void createGLUTWindow();

		//! Render given patch collection
		static void render(std::shared_ptr< TrianglesCollection > triangles);

	private:
		//! GLUT and OGl related stuff

		// GLUT callbacks
		static void onDisplay();

		static void onReshape(int width, int height);

		static void onKeyboard(unsigned char key, int x, int y);

		static void onSpecial(int key, int, int);

		static void onMouseClick(int button, int state, int x, int y);

		static void onMouseMotion(int x, int y);

		static void createRenderLists(const std::vector< GLTriangle >& triangles);

		static void createGLTriangles(std::shared_ptr< TrianglesCollection > triangles, std::vector< GLTriangle >* out);

	private:
		// Window dimensions
		static int mWindowWidth;
		static int mWindowHeight;

		static ProjectionParams mProjection;
		static InputData				mInput;
		static RenderParams			mRender;
	};

#endif // FRONTEND_PATCHVISUALIZER_H
