//-------------------------------------------------------------------
// File: mainwindow.cpp
// 
// Application main window
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------


#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	
}

MainWindow::~MainWindow()
{

}
