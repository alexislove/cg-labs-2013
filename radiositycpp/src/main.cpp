//-------------------------------------------------------------------
// File: main.cpp
// 
// Application entry point
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include <iostream>

#include "frontend/computedradiosityserializable.h"
#include "frontend/patchvisualizer.h"
#include "frontend/sceneserializable.h"

#include "radiosity/formfactorrenderer.h"
#include "radiosity/radiosityrenderer.h"
#include "radiosity/radiositysettings.h"
#include "radiosity/scene.h"

void usage()
{
	std::cout << "Radiosity engine"																																																							 << std::endl;
	std::cout << "Usage:"																																																												 << std::endl;
	std::cout	<< "radiosity.exe --scene=myscene.xml --num_iterations=30 --patch_size=1 --hemicube_edge=256 --output=computed.xml --memory=1024"  << std::endl;
	std::cout << "/? or --help to print this message!"																																													 << std::endl;
	std::cout << "Good luck!"																																																										 << std::endl;
}


#include <QCoreApplication>

int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);

	QString sceneFile;
	int			iterations;
	float   patchSize;
	float   hemicubeEdge;
	QString outputFile;
	long		memoryLimit = 0;

	if (argc < 7)
	{
		usage();
		return 0;
	}

	for (int idx = 0; idx < argc; ++idx)
	{
		QString arg(argv[idx]);


		if (arg.contains("--scene"))
		{
			sceneFile = arg.remove("--scene=");
			sceneFile.remove("\"");
		}
		else if (arg.contains("--num_iterations"))
		{
			iterations = arg.remove("--num_iterations=").toInt();
		}
		else if (arg.contains("--patch_size"))
		{
			patchSize = arg.remove("--patch_size=").toFloat();
		}
		else if (arg.contains("--hemicube_edge"))
		{
			hemicubeEdge = arg.remove("--hemicube_edge=").toFloat();
		}
		else if (arg.contains("--output"))
		{
			outputFile = arg.remove("--output=");
			outputFile.remove("\"");
		}
		else if (arg.contains("--memory"))
		{
			memoryLimit = arg.remove("--memory=").toLong() * 1024 * 1024; // Convert from MBs to bytes
		}
		else if (arg.contains("/?") || arg.contains("--help"))
		{
			usage();
			return 0;
		}
	}

	RadiositySettings& settings = RadiositySettings::getInstance();

	settings.setFloat(RadiositySettingsKeys::g_HemicubeEdgeSize,					 hemicubeEdge);
	settings.setFloat(RadiositySettingsKeys::g_HemicubeImportantArea,			 2 * hemicubeEdge);
	settings.setFloat(RadiositySettingsKeys::g_HemicubeRenderViewportSize, 3 * hemicubeEdge);

	FormFactorRenderer& ffrender = FormFactorRenderer::getInstance();

	ffrender.initialize();

	SceneSerializable sceneReader;

	QSharedPointer< Scene > scene = sceneReader.readScene(sceneFile);

	if (!scene)
	{
		std::cerr << "Failed reading radiosity scene! Application is closing..." << std::endl;
		return 0;
	}

	std::cout << "Scene reading completed." << std::endl;

	scene->split(patchSize);
	// Calculate emission for all patches
	scene->setupLightSources();

	std::shared_ptr< TrianglesCollection > triangles = scene->mergeSurfaces();

	std::cout << "Scene surfaces splitted. Number of patches: " << triangles->size() << std::endl;
	std::cout << "Memory usage limit is: " << memoryLimit << "(" << memoryLimit / (1024 * 1024) << " MiBs)" << std::endl;

	std::shared_ptr< RadiosityRenderer > renderer(new RadiosityRenderer(triangles, iterations, 0.f, memoryLimit));

	renderer->computeRadiosity();
	renderer->postprocess();

	ComputedRadiositySerializable radiosityWriter;

	if (!radiosityWriter.writePatchCollection(triangles, outputFile))
	{
		std::cerr << "Failed writing computed scene! Application is closing..." << std::endl;
		return 0;
	}

	PatchVisualizer::render(triangles);

	return 0;
}
