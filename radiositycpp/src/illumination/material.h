//-------------------------------------------------------------------
// File: material.h
// 
// This class represents material, that can be assigned to the patch
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef ILLUMINATION_MATERIAL_H
	#define ILLUMINATION_MATERIAL_H

	#include "color.h"

	struct Material
	{
		Color Emission;			 // Emission value of the patch 
		Color Reflection;		 // Reflectivity of the patch
		Color Radiosity;		 // Already calculated radiosity values
	};

#endif // ILLUMINATION_MATERIAL_H