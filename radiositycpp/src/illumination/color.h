//-------------------------------------------------------------------
// File: color.h
// 
// RGB color inlined arithmetics and operations
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef ILLUMINATION_COLOR_H
	#define ILLUMINATION_COLOR_H

	#define COLOR_FLOAT_ZERO 0.00001f

	class Color 
	{
		//! Arithmetic operations
		friend Color operator+(const Color& lh, const Color& rh);

		friend Color operator-(const Color& lh, const Color& rh);

		friend Color operator*(const Color& lh, const Color& rh);

		friend Color operator*(const Color& lh, float rh);

		friend Color operator*(float lh, const Color& rh);

		friend bool  operator==(const Color& lh, const Color& rh);

	public:
		explicit Color(float r = 0.f, float g = 0.f, float b = 0.f)
			: mR(r),
				mG(g),
				mB(b)
		{

		}

		void setRGB(float r, float g, float b)
		{
			mR = r; mG = g; mB = b;
		}

		void setR(float r)
		{
			mR = r;
		}

		void setG(float g)
		{
			mG = g;
		}

		void setB(float b)
		{
			mB = b;
		}

		float r() const
		{
			return mR;
		}

		float g() const
		{
			return mG;
		}

		float b() const
		{
			return mB;
		}

		// Inplace arithmetics
		Color& operator+=(const Color& rh)
		{
			mR += rh.mR;
			mG += rh.mG;
			mB += rh.mB;

			return *this;
		}

		Color& operator-=(const Color& rh)
		{
			mR -= rh.mR;
			mG -= rh.mG;
			mB -= rh.mB;

			return *this;
		}

		Color& operator*=(const Color& rh)
		{
			mR *= rh.mR;
			mG *= rh.mG;
			mB *= rh.mB;

			return *this;
		}

		Color& operator*=(float rh)
		{
			mR *= rh;
			mG *= rh;
			mB *= rh;

			return *this;
		}

	private:
		float mR;
		float mG;
		float mB;
	};

	inline Color operator+(const Color& lh, const Color& rh)
	{
		return Color(lh.mR + rh.mR, lh.mG + rh.mG, lh.mB + rh.mB);
	}

	inline Color operator-(const Color& lh, const Color& rh)
	{
		return Color(lh.mR - rh.mR, lh.mG - rh.mG, lh.mB - rh.mB);
	}

	inline Color operator*(const Color& lh, const Color& rh)
	{
		return Color(lh.mR * rh.mR, lh.mG * rh.mG, lh.mB * rh.mB);
	}

	inline Color operator*(const Color& lh, float rh)
	{
		return Color(lh.mR * rh, lh.mG * rh, lh.mB * rh);
	}

	inline Color operator*(float lh, const Color& rh)
	{
		return Color(lh * rh.mR, lh * rh.mG, lh * rh.mB);
	}

	inline bool operator==(const Color& lh, const Color& rh)
	{
		return fabs(lh.r() - rh.r()) < COLOR_FLOAT_ZERO && 
					 fabs(lh.g() - rh.g()) < COLOR_FLOAT_ZERO &&
					 fabs(lh.b() - rh.b()) < COLOR_FLOAT_ZERO;
	}

#endif // ILLUMINATION_COLOR_H
