//-------------------------------------------------------------------
// File: ipatchiterator.h
// 
// This class represents interface for patch iterators
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef RADIOSITY_IPATCHITERATOR_H
	#define RADIOSITY_IPATCHITERATOR_H

	#include <memory>

	#include "patch.h"

	struct IPatchIterator;
	struct IPatchConstIterator;

	typedef std::shared_ptr<IPatchIterator>			 IPatchIteratorPtr;
	typedef std::shared_ptr<IPatchConstIterator> IPatchConstIteratorPtr;

	struct IPatchIterator
	{
		virtual ~IPatchIterator()
		{

		}

		// Clone iterator instance
		virtual IPatchIteratorPtr clone() const = 0;

		// Get current value
		virtual Patch& operator*() = 0;

		//! Move iterator to the next element, don't mess up with forward movement
		//! this method can be used in reverse or random access iterators
		virtual void next() = 0;

		//! Check, whether iterator is pointing on the valid value or not
		virtual bool isNull() const = 0;

		//! Check, whether iterator has next element
		virtual bool hasNext() const = 0;
	};

	struct IPatchConstIterator
	{
		virtual ~IPatchConstIterator()
		{

		}

		// Clone iterator instance
		virtual IPatchConstIteratorPtr clone() const = 0;

		// Get current value
		virtual const Patch& operator*() const = 0;

		//! Move iterator to the next element, don't mess up with forward movement
		//! this method can be used in reverse or random access iterators
		virtual void next() = 0;

		//! Check, whether iterator is pointing on the valid value or not
		virtual bool isNull() const = 0;

		//! Check, whether iterator has next element
		virtual bool hasNext() const = 0;
	};

	
#endif // RADIOSITY_IPATCHITERATOR_H
