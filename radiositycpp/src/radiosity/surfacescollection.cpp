//-------------------------------------------------------------------
// File: surfacescollection.cpp
// 
// This class represents collection of abstract surfaces
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include <assert.h>

#include "forwardpatchcollectioniterator.h"
#include "surface.h"

#include "surfacescollection.h"

SurfacesCollection::SurfacesCollection()
{
}

SurfacesCollection::~SurfacesCollection()
{
}

void SurfacesCollection::add(std::shared_ptr<Surface> surface)
{
	mSurfaces.push_back(surface);
}

void SurfacesCollection::split(float size)
{
	for each (auto surface in mSurfaces)
	{
		surface->split(size);
	}
}

IPatchIteratorPtr SurfacesCollection::getPatchIterator()
{
	return IPatchIteratorPtr(new ForwardPatchCollectionIterator(*this));
}

IPatchConstIteratorPtr SurfacesCollection::getPatchIterator() const
{
	return IPatchConstIteratorPtr(new ForwardPatchCollectionConstIterator(*this));
}

Surface& SurfacesCollection::operator[](unsigned index)
{
	assert(index < mSurfaces.size());
	return *mSurfaces[index];
}

const Surface& SurfacesCollection::operator[](unsigned index) const
{
	assert(index < mSurfaces.size());
	return *mSurfaces[index];
}

IPatchIteratorPtr SurfacesCollection::begin()
{
	return getPatchIterator();
}

IPatchConstIteratorPtr SurfacesCollection::begin() const
{
	return getPatchIterator();
}

IPatchIteratorPtr SurfacesCollection::end()
{
	// Return iterator on the element after last existing patch in the whole collection
	return IPatchIteratorPtr(new ForwardPatchCollectionIterator(*this, size(), IPatchIteratorPtr()));
}

IPatchConstIteratorPtr SurfacesCollection::end() const
{
	// Return iterator on the element after last existing patch in the whole collection
	return IPatchConstIteratorPtr(new ForwardPatchCollectionConstIterator(*this, size(), IPatchConstIteratorPtr()));
}