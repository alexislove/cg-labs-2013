//-------------------------------------------------------------------
// File: radiosityprogressobserver.cpp
// 
// This class provides capabilities to show progress of radiosity 
// computation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include <iostream>

#include "illumination/color.h"

#include "radiosityprogressobserver.h"

void RadiosityProgressObserver::writeProgress(int iteration, int patch)
{
	std::cout << "Processing iteration " << iteration << " at patch " << patch << "... ";
}

void RadiosityProgressObserver::writeColor(int iteration, int patch, const Color& color)
{
	std::cout << "Computed color at " << iteration << " for patch " << patch << "(" << color.r() << ", " << color.g() << ", " << color.b() << ")";
}

void RadiosityProgressObserver::returnCarriage()
{
	std::cout << "\r";
}