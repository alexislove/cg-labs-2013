//-------------------------------------------------------------------
// File: surface.h
// 
// This class represents abstract surface, that can be divided into triangle patches
// and holds basic radiosity-related attributes
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef RADIOSITY_SURFACE_H
	#define RADIOSITY_SURFACE_H
	
	#include <memory>

	#include "geometry/matrix4x4.h"

	#include "ipatchiterator.h"
	#include "patch.h"

	class PatchTriangle;
	class TrianglesCollection;

	class Surface
	{
	public:

		virtual ~Surface();

		// Set transformation matrix
		void setTransform(const Matrix4x4& transform)
		{
			mTransform = transform;
		}

		// Get transformation matrix
		const Matrix4x4& getTransform() const
		{
			return mTransform;
		}

		// Get iterator over patches collection
		IPatchIteratorPtr getPatchIterator();

		// Get iterator over patches collection
		IPatchConstIteratorPtr getPatchIterator() const;

		// Split surface triangles and from new patch collection, size is maximal size of the patch
		void split(float size);

		// Get number of patches, surface is splitted on
		unsigned getPatchCount() const
		{
			return mPatches->size();
		}

		// Get patch at given index
		const Patch& getPatchAt(int index) const
		{
			return (*mPatches)[index];
		}

		// Get patch collection
		std::shared_ptr<PatchCollection> getPatchCollection() const;

		// Apply transformation for all the triangles, should be called, when surface was loaded
		void applyTransformation();


	protected:
		// Force subclassing
		explicit Surface();

		// Add triangle to the surface triangles collection
		void addTriangle(const PatchTriangle& tri);

		// Set triangles collection
		void setTrianglesCollection(std::shared_ptr< TrianglesCollection > collection);

	private:
		// Surface isn't copyable
		Surface(const Surface&);
		Surface& operator=(const Surface&);

	private:
		//! List of triangles, forming the surface
		std::shared_ptr<TrianglesCollection> mTriangles;
		//! List of patches
		std::shared_ptr<PatchCollection>		 mPatches;
		//! Surface transformation matrix
		Matrix4x4														 mTransform;
	};

#endif // GEOMETRY_SURFACE_H