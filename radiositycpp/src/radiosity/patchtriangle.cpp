//-------------------------------------------------------------------
// File: patchtriangle.cpp
// 
// This class represents triangle used as a patch in radiosity solver
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include "trianglescollection.h"

#include "patchtriangle.h"

std::shared_ptr<TrianglesCollection> PatchTriangle::split(const PatchTriangle& tri)
{
	// Calculate centers of triangles
	
	Vector3D c0 = Vec3Center(tri.B, tri.C);
	Vector3D c1 = Vec3Center(tri.C, tri.A);
	Vector3D c2 = Vec3Center(tri.A, tri.B);

	std::shared_ptr<TrianglesCollection> res(new TrianglesCollection);
	// Form new triangles
	PatchTriangle triangle = tri; // Copy non-geometry data

	triangle.A = tri.A;
	triangle.B = c2;
	triangle.C = c1;

	res->add(triangle);

	triangle.A = tri.B;
	triangle.B = c0;
	triangle.C = c2;

	res->add(triangle);

	triangle.A = tri.C;
	triangle.B = c1;
	triangle.C = c0;

	res->add(triangle);

	// And the last one with vertices in the centers

	triangle.A = c0;
	triangle.B = c1;
	triangle.C = c2;

	res->add(triangle);

	return res;
}