//-------------------------------------------------------------------
// File: radiositysettings.h
// 
// This is singleton class, holding radiosity settings
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef RADIOSITY_RADIOSITYSETTINGS_H
	#define RADIOSITY_RADIOSITYSETTINGS_H

	#include <map>
	#include <memory>
	#include <string>

	namespace RadiositySettingsKeys
	{
		extern const char* const g_HemicubeEdgeSize;
		extern const char* const g_HemicubeImportantArea;
		extern const char* const g_HemicubeRenderViewportSize;
	} // namespace RadiositySettingsKeys

	class RadiositySettings
	{
	public:
		static RadiositySettings& getInstance()
		{
			if (mInstance)
				return *mInstance;
			mInstance.reset(new RadiositySettings);
			return *mInstance;
		}

	public:
		// Get or set float params
		float getFloat(const std::string& key, bool* ok) const;

		void setFloat(const std::string& key, float value);

	private:
		RadiositySettings();

	private:
		//! Canonical instance
		static std::unique_ptr<RadiositySettings> mInstance;

	private:
		std::map<std::string, float> mFloatParams;
	};

#endif // RADIOSITY_RADIOSITYSETTINGS_H