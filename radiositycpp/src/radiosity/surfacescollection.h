//-------------------------------------------------------------------
// File: surfacescollection.h
// 
// This class represents collection of abstract surfaces
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef RADIOSITY_SURFACESCOLLECTION_H
	#define RADIOSITY_SURFACESCOLLECTION_H

	#include <vector>
	#include <memory>

	#include "ipatchiterator.h"

	class Surface;

	class SurfacesCollection
	{
	public:
		explicit SurfacesCollection();

		~SurfacesCollection();

		//! Add new surface to collection
		void add(std::shared_ptr<Surface> surface);

		//! Split surfaces, size is maximal size of the patch
		void split(float maxSize);

		//! Get number of surfaces
		unsigned size() const
		{
			return mSurfaces.size();
		}

		//! Get iterator over patches of all surfaces
		IPatchIteratorPtr getPatchIterator();

		//! Get iterator over patches of all surfaces
		IPatchConstIteratorPtr getPatchIterator() const;

		//! Indices access
		Surface& operator[](unsigned index);

		//! Indices access
		const Surface& operator[](unsigned index) const;

		// Support for each in iterations

		IPatchIteratorPtr begin();

		IPatchConstIteratorPtr begin() const;

		IPatchIteratorPtr end();

		IPatchConstIteratorPtr end() const;

	private:
		std::vector< std::shared_ptr<Surface> > mSurfaces;
	};

#endif // RADIOSITY_SURFACESCOLLECTION_H
