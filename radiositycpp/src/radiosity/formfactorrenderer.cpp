//-------------------------------------------------------------------
// File: formfactorrenderer.cpp
// 
// This class represents form factor renderer, that renders hemicube
// from the destination patch, applying color corrections using sin/cos
// precalculated map. Also this is a singleton.
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#define _USE_MATH_DEFINES

#include <assert.h>
#include <math.h>

#include <Windows.h>
#include <WinGDI.h>

#include <GL/GL.h>
#include <GL/GLU.h>
#include <GL/glext.h>

#include <glut.h>

#include "radiositysettings.h"
#include "surface.h"
#include "surfacepatchradiosityrenderer.h"

#include "formfactorrenderer.h"

static int WindowWidth	= 0;
static int WindowHeight = 0;

// For form factor rendering using OpenGL
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) 
	{
	case WM_PAINT:
		{
			HDC					hDC;
			PAINTSTRUCT ps;

			hDC = BeginPaint(hWnd, &ps);
			SwapBuffers(hDC);
			EndPaint(hWnd, &ps);

			return 0;
		}
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}


std::unique_ptr<FormFactorRenderer> FormFactorRenderer::mInstance;

FormFactorRenderer::FormFactorRenderer()
{

}

void FormFactorRenderer::reset()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearDepth(1.0f);
	glViewport(0, 0, mViewportSize, mViewportSize);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(90, mViewportSize / mViewportSize, 1e-3, 50);
	glMatrixMode(GL_MODELVIEW);
}

SurfacePatchRadiosityRenderer* FormFactorRenderer::render(const PatchCollection& patches, int patch, float patchThreshold)
{
	SurfacePatchRadiosityRenderer* renderer = new SurfacePatchRadiosityRenderer(patches, patchThreshold);

	doRender(patches, patch);
	
	std::hash_map<unsigned, float> corrections;

	screenToFormFactorHash(&corrections);

	const float Area = (mHemicubeEdgeSize * mHemicubeEdgeSize + 4.f * mHemicubeEdgeSize * (mHemicubeEdgeSize * 0.5f)); // Actually 4 * size * size / 2

	const unsigned patchCount = patches.size();
	for each (auto item in corrections)
	{
		// Ignore background
		if(item.first != 0xffffff)
		{
			if(patchCount > item.first) 
			{
				renderer->storeColorAndFormFactor(item.first, 2.0 * item.second / Area);
			}
		}
	}

	return renderer;
}

void FormFactorRenderer::calculateCorrectionFactors()
{
	// http://freespace.virgin.net/hugo.elias/radiosity/radiosity.htm
	mCorrections.resize(mHemicubeImportantArea);

	const float Radius = mHemicubeImportantArea;
	for (int idx = 0; idx < mHemicubeImportantArea; ++idx)
	{
		mCorrections[idx].resize(mHemicubeImportantArea);
		for (int jdx = 0; jdx < mHemicubeImportantArea; ++jdx)
		{
			float u = -mHemicubeEdgeSize + idx;
			float v = -mHemicubeEdgeSize + jdx;

			double cosU = cosf(M_PI * u/ Radius);
			double cosV = cosf(M_PI * v/ Radius);

			mCorrections[idx][jdx] = cosU * cosV;
		}
	}
}

void FormFactorRenderer::initialize()
{
	using namespace RadiositySettingsKeys;

	RadiositySettings& settings = RadiositySettings::getInstance();

	bool ok = false;
	mHemicubeEdgeSize			 = settings.getFloat(g_HemicubeEdgeSize, &ok); assert(ok);
	mHemicubeImportantArea = settings.getFloat(g_HemicubeImportantArea, &ok); assert(ok);
	mViewportSize					 = settings.getFloat(g_HemicubeRenderViewportSize, &ok); assert(ok);

	HINSTANCE hInstance = NULL;

	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX); 
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = (WNDPROC)WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = 0;
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName = 0;
	wcex.lpszClassName = L"FormFactorRenderWindowClass";
	wcex.hIconSm = 0;

	RegisterClassEx(&wcex);

	HWND hWnd = CreateWindow(L"FormFactorRenderWindowClass", 
													 L"Form factor render window", 
													 WS_POPUP, 
													 0, 
													 0, 
													 mViewportSize, mViewportSize, 
													 NULL, 
													 NULL, 
													 hInstance, 
													 NULL);

	HDC hDC = GetDC( hWnd );
	HGLRC  hglrc;

	PIXELFORMATDESCRIPTOR pfd;
	int pixelFormat = 0;
	ZeroMemory(&pfd, sizeof(pfd));

	pfd.nSize			 = sizeof( pfd );
	pfd.nVersion	 = 1;
	pfd.dwFlags		 = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 24;
	pfd.cDepthBits = 16;
	pfd.iLayerType = PFD_MAIN_PLANE;
	pixelFormat		 = ChoosePixelFormat( hDC, &pfd );

	DescribePixelFormat(hDC, pixelFormat, sizeof(PIXELFORMATDESCRIPTOR), &pfd);

	SetPixelFormat(hDC, pixelFormat, &pfd);

	hglrc = wglCreateContext(hDC);
	wglMakeCurrent(hDC, hglrc);

	/*ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);*/

	mhWnd = hWnd;

	// Setup basic OpenGL stuff
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, mViewportSize, mViewportSize);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(90, mViewportSize / mViewportSize, 1e-3, 50.f);
	glMatrixMode(GL_MODELVIEW);

	calculateCorrectionFactors();
}

void FormFactorRenderer::renderHemicube(const PatchCollection& patches, int x, int y, const Vector3D& center, const Vector3D& at, const Vector3D& up)
{
	glViewport(x,y, static_cast<GLsizei>(mHemicubeEdgeSize), static_cast<GLsizei>(mHemicubeEdgeSize));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(90, mViewportSize / mViewportSize, 1e-3, 50.f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(center.x(), center.y(), center.z(),
						at.x(),			at.y(),			at.z(),
						up.x(),			up.y(),			up.z());
	// Fallback to exact rendering
	doRender(patches);
}

void FormFactorRenderer::doRender(const PatchCollection& patches, int patch)
{
	// clear window
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	const Patch &tri = patches[patch];


	Vector3D center = TriGetCenter(tri);

	// Normal vector and inverse normal vector of this triangle
	Vector3D triNormal    = TriGetNormal(tri);
	Vector3D triNegNormal = -triNormal;

	Vector3D side = Vec3Cross(triNormal, Vector3D(1.f, 2.f, 3.f));
	if(Vec3Length(side) < FLOAT_ZERO)
	{
		side = Vec3Cross(triNormal, Vector3D(1.f, 1.f, 1.f)); 
	}
	Vector3D negSide = -side;

	Vector3D vecFront = Vec3Cross(side, triNormal);
	Vector3D vecBack  = Vec3Cross(negSide, triNormal);
	Vector3D vecLeft  = negSide;
	Vector3D vecRight = side;

	// points for directions of camera (top and 4 side)
	Vector3D top   = center + triNormal;
	Vector3D front = center + vecFront;
	Vector3D back  = center + vecBack;
	Vector3D left  = center + vecLeft;
	Vector3D right = center + vecRight;

	vecFront.toUnit();
	vecBack.toUnit();
	vecLeft.toUnit();
	vecRight.toUnit();

	// Top
	renderHemicube(patches, mHemicubeEdgeSize, mHemicubeEdgeSize, center, top, vecFront);

	// Front
	renderHemicube(patches, mHemicubeEdgeSize, 2 * mHemicubeEdgeSize, center, front, triNegNormal);

	// Back
	renderHemicube(patches, mHemicubeEdgeSize, 0, center, back, triNormal);

	// Left side
	renderHemicube(patches, 0, mHemicubeEdgeSize, center, left, vecFront);

	// Right side
	renderHemicube(patches, 2 * mHemicubeEdgeSize, mHemicubeEdgeSize, center, right, vecFront);

	glFlush();

	// Debug code
	//InvalidateRect(mhWnd, NULL, FALSE);
	//SendMessage(mhWnd, WM_PAINT, 0L, 0L);
}

void FormFactorRenderer::doRender(const PatchCollection& patches)
{
	// Native fixed-pipeline rendering
	glBegin(GL_TRIANGLES);
	for(unsigned idx = 0, count = patches.size(); idx < count; ++idx)
	{
		const Patch& patch = patches[idx];
		glColor3ub((idx) & 0xff, (idx >> 8) & 0xff,(idx >> 16) & 0xff);
		
		glVertex3f(patch.A.x(), patch.A.y(), patch.A.z());
		glVertex3f(patch.B.x(), patch.B.y(), patch.B.z());
		glVertex3f(patch.C.x(), patch.C.y(), patch.C.z());
	}
	glEnd();
}

void FormFactorRenderer::screenToFormFactorHash(std::hash_map<unsigned, float>* hash)
{
	const int width  = static_cast<int>(mViewportSize);
	const int height = static_cast<int>(mViewportSize);

	// Allocate memory to read frame buffer
	const int bufferSize = width * height * 3; // 3 components

	unsigned char* frameBuffer = new unsigned char[bufferSize];

	glReadPixels(0, 0, width, height, GL_BGR, GL_UNSIGNED_BYTE, frameBuffer);

	const int startX = static_cast<int>(mHemicubeEdgeSize / 2);
	const int startY = static_cast<int>(mHemicubeEdgeSize / 2);

	const int endX = static_cast<int>(mHemicubeEdgeSize / 2) + static_cast<int>(mHemicubeEdgeSize * 2);
	const int endY = static_cast<int>(mHemicubeEdgeSize / 2) + static_cast<int>(mHemicubeEdgeSize * 2);

	for (int x = startX; x < endX; ++x)
	{
		for (int y = startY; y < endY; ++y)
		{
			unsigned char b = frameBuffer[3 * (x * height + y) + 0];
			unsigned char g = frameBuffer[3 * (x * height + y) + 1];
			unsigned char r = frameBuffer[3 * (x * height + y) + 2];

			unsigned color = r + (g << 8) + (b << 16);

			(*hash)[color] += mCorrections[x - startX][y - startY];
		}
	}

	//glDrawPixels(width, height, GL_BGR, GL_UNSIGNED_BYTE, frameBuffer);
	//SendMessage(mhWnd, WM_PAINT, 0, 0);

	delete[] frameBuffer;
}

