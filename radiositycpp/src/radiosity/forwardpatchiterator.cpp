//-------------------------------------------------------------------
// File: forwardpatchiterator.cpp
// 
// This class represents forward patch iterator implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include <assert.h>

#include "trianglescollection.h"

#include "forwardpatchiterator.h"

ForwardPatchIterator::ForwardPatchIterator(PatchCollection& tris, int index /* = 0*/)
	: mOwner(tris),
		mIndex(index)
{
}

ForwardPatchIterator::ForwardPatchIterator(const ForwardPatchIterator& other)
	: mOwner(other.mOwner),
		mIndex(other.mIndex)
{

}

ForwardPatchIterator::~ForwardPatchIterator()
{
}

IPatchIteratorPtr ForwardPatchIterator::clone() const
{
	return IPatchIteratorPtr(new ForwardPatchIterator(mOwner, mIndex));
}

Patch& ForwardPatchIterator::operator*()
{
	assert(mIndex < mOwner.size());
	return mOwner[mIndex];
}

void ForwardPatchIterator::next()
{
	++mIndex;
}

bool ForwardPatchIterator::isNull() const
{
	return mIndex >= mOwner.size();
}

bool ForwardPatchIterator::hasNext() const
{
	return mIndex != (mOwner.size() - 1);
}

ForwardPatchConstIterator::ForwardPatchConstIterator(const PatchCollection& tris, int index /* = 0*/)
	: mOwner(tris),
		mIndex(index)
{
}

ForwardPatchConstIterator::ForwardPatchConstIterator(const ForwardPatchConstIterator& other)
	: mOwner(other.mOwner),
		mIndex(other.mIndex)
{
	
}

ForwardPatchConstIterator::~ForwardPatchConstIterator()
{
}

IPatchConstIteratorPtr ForwardPatchConstIterator::clone() const
{
	return IPatchConstIteratorPtr(new ForwardPatchConstIterator(mOwner, mIndex));
}

const Patch& ForwardPatchConstIterator::operator*() const
{
	assert(mIndex < mOwner.size());
	return mOwner[mIndex];
}

void ForwardPatchConstIterator::next()
{
	++mIndex;
}

bool ForwardPatchConstIterator::isNull() const
{
	return mIndex >= mOwner.size();
}

bool ForwardPatchConstIterator::hasNext() const
{
	return mIndex != (mOwner.size() - 1);
}
