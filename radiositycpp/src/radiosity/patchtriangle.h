//-------------------------------------------------------------------
// File: patchtriangle.h
// 
// This class represents triangle used as a patch in radiosity solver
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef RADIOSITY_PATCHTRIANGLE_H
	#define RADIOSITY_PATCHTRIANGLE_H

	#include <memory>

	#include "geometry/triangle.h"
	#include "illumination/material.h"

	class TrianglesCollection;

	struct PatchTriangle : Triangle
	{
		// Split patch into 4 triangles, each 1/4 area of original
		static std::shared_ptr<TrianglesCollection> split(const PatchTriangle& tri);

		Material TheMaterial;
		Color		 PrevRadiosity; // Cache previous iteration radiosity value
	};

#endif // RADIOSITY_PATCHTRIANGLE_H