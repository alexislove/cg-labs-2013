//-------------------------------------------------------------------
// File: radiosityrenderer.h
// 
// This class represents radiosity renderer for the whole scene
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef RADIOSITY_RADIOSITYRENDERER_H
	#define RADIOSITY_RADIOSITYRENDERER_H

	#include "patch.h"

	class SurfaceRadiosityRenderer;

	class RadiosityRenderer
	{
		struct Statistics
		{
			int Iteration;
			int Patch;
		};
	public:
		explicit RadiosityRenderer(std::shared_ptr<PatchCollection> patches, int iterations, float threshold, long maxMemoryUsage);

		~RadiosityRenderer();

		//! Compute radiosity for given patch collection
		void computeRadiosity();

		//! Postprocess patches colors
		void postprocess();

	private:
		//! Perform iteration activity
		void iteration();

		//! Postprocess radiosity value
		void updateColorPeak(const Color& radiosity);

	private:
		// Radiosity renderer isn't copyable
		RadiosityRenderer(const RadiosityRenderer&);
		RadiosityRenderer& operator=(const RadiosityRenderer&);

	private:
		//! Patch collection to render
		std::shared_ptr<PatchCollection>					mPatches;
		//! Renderer of surface
		std::shared_ptr<SurfaceRadiosityRenderer> mRenderer;
		//! Iterations count
		int																				mIterations;
		//! Form factor threshold
		float																			mThreshold;
		//! Color peak for postprocessing
		float																			mColorPeak;
		//! Statistics for console output
		Statistics																mStatistics;
		//! Maximum available memory usage
		long																			mMaxMemoryUsage;
	};

#endif // RADIOSITY_RADIOSITYRENDERER_H