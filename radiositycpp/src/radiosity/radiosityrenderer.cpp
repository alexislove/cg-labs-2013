//-------------------------------------------------------------------
// File: radiosityrenderer.cpp
// 
// This class represents radiosity renderer for the whole scene
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include "surfaceradiosityrenderer.h"
#include "radiosityprogressobserver.h"

#include "radiosityrenderer.h"

namespace
{
	// Postprocess radiosity values
	Color postprocessRadiosity(const Color& color, float colorPeak)
	{
		float r = 1.f / colorPeak;

		return Color(color.r() * r, color.g() * r, color.b() * r);
	}
}

RadiosityRenderer::RadiosityRenderer(std::shared_ptr<PatchCollection> patches, int iterations, float threshold, long maxMemoryUsage)
	: mPatches(patches),
		mIterations(iterations),
		mThreshold(threshold),
		mRenderer(new SurfaceRadiosityRenderer(*patches, threshold, maxMemoryUsage)),
		mMaxMemoryUsage(maxMemoryUsage)
{
	mStatistics.Iteration = 0;
	mStatistics.Patch     = 0;
}

RadiosityRenderer::~RadiosityRenderer()
{

}

void RadiosityRenderer::computeRadiosity()
{
	for (int idx = 0; idx < mIterations; ++idx)
	{
		mStatistics.Iteration = idx;
		iteration();
	}
}

void RadiosityRenderer::iteration()
{
	const unsigned patchCount = mPatches->size();
	// Cache previous radiosity value
	for (int idx = 0; idx < patchCount; ++idx)
	{
		(*mPatches)[idx].PrevRadiosity = (*mPatches)[idx].TheMaterial.Radiosity;
	}

	for (int idx = 0; idx < patchCount; ++idx)
	{
		mStatistics.Patch = idx;

		RadiosityProgressObserver::writeProgress(mStatistics.Iteration, mStatistics.Patch);

		Patch& patch = (*mPatches)[idx];

		Color radiosity = mRenderer->computeRadiosity(idx); // Incident light of patch

		radiosity *= patch.TheMaterial.Reflection;
		radiosity += patch.TheMaterial.Emission;

		//RadiosityProgressObserver::writeColor(mStatistics.Iteration, mStatistics.Patch, radiosity);

		patch.TheMaterial.Radiosity = radiosity;

		RadiosityProgressObserver::returnCarriage();

		updateColorPeak(radiosity);
	}
}

void RadiosityRenderer::postprocess()
{
	for (int idx = 0, count = mPatches->size(); idx < count; ++idx)
	{
		(*mPatches)[idx].TheMaterial.Radiosity = postprocessRadiosity((*mPatches)[idx].TheMaterial.Radiosity, mColorPeak);
	}
}

void RadiosityRenderer::updateColorPeak(const Color& radiosity)
{
	if (mColorPeak < radiosity.r())
	{
		mColorPeak = radiosity.r();
	}

	if (mColorPeak < radiosity.g())
	{
		mColorPeak = radiosity.g();
	}

	if (mColorPeak < radiosity.b())
	{
		mColorPeak = radiosity.b();
	}
}
