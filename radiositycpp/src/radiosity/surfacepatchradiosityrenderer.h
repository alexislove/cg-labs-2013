//-------------------------------------------------------------------
// File: surfacepatchradiosityrenderer.h
// 
// This class represents radiosity renderer for surface patches
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef RADIOSITY_SURFACEPATCHRADIOSITYRENDERER_H
	#define RADIOSITY_SURFACEPATCHRADIOSITYRENDERER_H

	#include <memory>

	#include "illumination/color.h"

	#include "patch.h"

	class SurfacePatchRadiosityRenderer
	{
		// Form factor and color storage
		struct AreaRadiosity
		{
			int   Patch;
			float FormFactor;
			Color RadiosityColor;
		};

	public:
		// Get size of form factor descriptor in bytes
		static int getFormFactorDescByteSize()
		{
			return sizeof(AreaRadiosity);
		}

	public:
		explicit SurfacePatchRadiosityRenderer(const PatchCollection& patches, float threshold);

		// Store color and form factor of the patch with given index
		void storeColorAndFormFactor(int patch, float formFactor);

		// Update previous radiosity value for the patch
		void updatePreviousRadiosityValues();

		// Compute radiosity for the surface
		Color computeRadiosity() const;

		// Get number of form factors
		int getFormFactorsCount() const
		{
			return mAreaRadiosities.size();
		}

		// Get size in bytes of the renderer
		int getByteSize() const
		{
			return getFormFactorsCount() * getFormFactorDescByteSize() * sizeof(mPatches) * sizeof(float);
		}

	private:
		//! Surface instance
		const PatchCollection&			 mPatches;
		//! Threshold for form factor, every patch, smaller, that it will be ignored
		float												 mThreshold;
		//! Vector of radiosity per patch area
		std::vector<AreaRadiosity>	 mAreaRadiosities;
	};
	

#endif // RADIOSITY_SURFACEPATCHRADIOSITYRENDERER_H
