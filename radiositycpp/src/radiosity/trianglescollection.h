//-------------------------------------------------------------------
// File: trianglescollection.h
// 
// This class represents collection of patch triangle primitives
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef RADIOSITY_TRIANGLESCOLLECTION_H
	#define RADIOSITY_TRIANGLESCOLLECTION_H

	#include <vector>

	#include "patchtriangle.h"

	class TrianglesCollection
	{
	public:
		// Iterator over triangles
		typedef std::vector<PatchTriangle>::iterator			 Iterator;
		typedef std::vector<PatchTriangle>::const_iterator ConstIterator;

	public:
		explicit TrianglesCollection();

		~TrianglesCollection();

		void add(const PatchTriangle& tri);

		void append(const TrianglesCollection& tris);

		unsigned size() const
		{
			return mTriangles.size();
		}

		const PatchTriangle& operator[](int idx) const
		{
			return mTriangles[idx];
		}

		PatchTriangle& operator[](int idx)
		{
			return mTriangles[idx];
		}

		// Support for each in

		Iterator begin() 
		{
			return mTriangles.begin();
		}

		Iterator end()
		{
			return mTriangles.end();
		}

		ConstIterator begin() const
		{
			return mTriangles.begin();
		}

		ConstIterator end() const
		{
			return mTriangles.end();
		}

	private:
		std::vector<PatchTriangle> mTriangles;
	};

#endif // GEOMETRY_TRIANGLESCOLLECTION_H
