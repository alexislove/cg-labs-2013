//-------------------------------------------------------------------
// File: scene.h
// 
// This class scene, that can be rendered using radiosity renderer
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef RADIOSITY_SCENE_H
	#define RADIOSITY_SCENE_H

	#include <memory>

	#include "patch.h"

	class Surface;
	class SurfacesCollection;

	class Scene
	{
	public:
		explicit Scene();

		// Add surface to the scene
		void addSurface(std::shared_ptr<Surface> surface);

		// Split scene surfaces into patches
		void split(float size);

		// Get surfaces collection
		std::shared_ptr< SurfacesCollection > getSurfaces() const;

		// Merge all scene patches and return collection of them
		std::shared_ptr< PatchCollection > mergeSurfaces() const;

		// Setup emission for all light sources
		void setupLightSources();

	private:
		//! Collection of scene surfaces
		std::shared_ptr< SurfacesCollection > mSurfaces;
	};

#endif // RADIOSITY_SCENE_H