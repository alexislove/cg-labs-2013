//-------------------------------------------------------------------
// File: forwardpatchcollectioniterator.h
// 
// This class represents iterator over patches in surfaces collection
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef RADIOSITY_FORWARDPATCHCOLLECTIONITERATOR_H
	#define RADIOSITY_FORWARDPATCHCOLLECTIONITERATOR_H

	#include "ipatchiterator.h"

	class SurfacesCollection;

	class ForwardPatchCollectionIterator : public IPatchIterator
	{
	public:
		explicit ForwardPatchCollectionIterator(SurfacesCollection& collection, int surface = 0, IPatchIteratorPtr patch = IPatchIteratorPtr());

		ForwardPatchCollectionIterator(const ForwardPatchCollectionIterator& other);

		virtual ~ForwardPatchCollectionIterator();

		virtual IPatchIteratorPtr clone() const;

		virtual Patch& operator*();

		virtual void next();

		virtual bool isNull() const;

		virtual bool hasNext() const;

	private:
		//! Surfaces collection, that iterator belongs to
		SurfacesCollection& mOwner;
		//! Current surface index
		int									mSurfaceIndex;
		//! Current surface patches iterator
		IPatchIteratorPtr		mPatch;
	};

	class ForwardPatchCollectionConstIterator : public IPatchConstIterator
	{
	public:
		explicit ForwardPatchCollectionConstIterator(const SurfacesCollection& collection, int surface = 0, IPatchConstIteratorPtr patch = IPatchConstIteratorPtr());

		ForwardPatchCollectionConstIterator(const ForwardPatchCollectionConstIterator& other);

		virtual ~ForwardPatchCollectionConstIterator();

		virtual IPatchConstIteratorPtr clone() const;

		virtual const Patch& operator*() const;

		virtual void next();

		virtual bool isNull() const;

		virtual bool hasNext() const;

	private:
		//! Surfaces collection, that iterator belongs to
		const SurfacesCollection& mOwner;
		//! Current surface index
		int												mSurfaceIndex;
		//! Current surface patches iterator
		IPatchConstIteratorPtr					mPatch;
	};

#endif // RADIOSITY_FORWARDPATCHCOLLECTIONITERATOR_H
