//-------------------------------------------------------------------
// File: radiosityprogressobserver.h
// 
// This class provides capabilities to show progress of radiosity 
// computation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef RADIOSITY_RADIOSITYPROGRESSOBSERVER_H
	#define RADIOSITY_RADIOSITYPROGRESSOBSERVER_H

	class Color;

	struct RadiosityProgressObserver
	{
		 // Output current progress of computation
		 static void writeProgress(int iteration, int patch);

		 // Output computed value
		 static void writeColor(int iteration, int patch, const Color& color);

		 // Write return carriage symbol
		 static void returnCarriage();
	};

#endif // RADIOSITY_RADIOSITYPROGRESSOBSERVER_H