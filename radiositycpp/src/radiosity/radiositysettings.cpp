//-------------------------------------------------------------------
// File: radiositysettings.cpp
// 
// This is singleton class, holding radiosity settings
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include "radiositysettings.h"

namespace RadiositySettingsKeys
{
	const char* const g_HemicubeEdgeSize = "edge-size";
	const char* const g_HemicubeImportantArea = "important-area";
	const char* const g_HemicubeRenderViewportSize = "render-viewport-size";
} // namespace RadiositySettingsKeys

std::unique_ptr<RadiositySettings> RadiositySettings::mInstance;

float RadiositySettings::getFloat(const std::string& key, bool *ok) const
{
	auto found = mFloatParams.find(key);
	if (found == mFloatParams.end())
	{
		*ok = false;
		return found->second;
	}
	*ok = true;
	return found->second;
}

void RadiositySettings::setFloat(const std::string& key, float value)
{
	mFloatParams[key] = value;
}

RadiositySettings::RadiositySettings()
{
}