//-------------------------------------------------------------------
// File: surface.cpp
// 
// This class represents abstract surface, that can be divided into triangle patches
// and holds basic radiosity-related attributes
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include <memory>

#include "forwardpatchiterator.h"

#include "trianglescollection.h"
#include "trianglesqueue.h"

#include "surface.h"

Surface::Surface()
	: mTriangles(new TrianglesCollection)
{

}

Surface::~Surface()
{

}

IPatchIteratorPtr Surface::getPatchIterator()
{
	return IPatchIteratorPtr(new ForwardPatchIterator(*mPatches));
}

IPatchConstIteratorPtr Surface::getPatchIterator() const
{
	return IPatchConstIteratorPtr(new ForwardPatchConstIterator(*mPatches));
}

void Surface::split(float size)
{
	std::shared_ptr<PatchCollection> newPatchCollection(new TrianglesCollection);

	std::unique_ptr<TrianglesQueue> queue(new TrianglesQueue);

	queue->enqueue(*mTriangles);

	// Split all triangles
	while (!queue->empty())
	{
		PatchTriangle tri = queue->dequeue();
		// If triangle suits the size
		if (TriGetArea(tri) <= size)
		{
			newPatchCollection->add(tri);
			continue;
		}

		// Split triangle and enqueue results in case it stays too big yet
		std::shared_ptr<TrianglesCollection> splitted = PatchTriangle::split(tri);

		queue->enqueue(*splitted);

	}
	
	mPatches = newPatchCollection;
}

std::shared_ptr<PatchCollection> Surface::getPatchCollection() const
{
	return mPatches;
}

void Surface::applyTransformation()
{
	// Apply transform matrix for every triangle of the surface
	for (auto tri = mTriangles->begin(); tri != mTriangles->end(); ++tri)
	{
		tri->A = mTransform * tri->A;
		tri->B = mTransform * tri->B;
		tri->C = mTransform * tri->C;
	}
}

void Surface::addTriangle(const PatchTriangle& tri)
{
	mTriangles->add(tri);
}

void Surface::setTrianglesCollection(std::shared_ptr< TrianglesCollection > collection)
{
	mTriangles = collection;
}