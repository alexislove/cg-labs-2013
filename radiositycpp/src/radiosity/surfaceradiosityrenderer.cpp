//-------------------------------------------------------------------
// File: surfaceradiosityrenderer.cpp
// 
// This class represents radiosity renderer for a surface
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include <iostream>

#include "formfactorrenderer.h"
#include "surfacepatchradiosityrenderer.h"

#include "surfaceradiosityrenderer.h"

#define MIBS(v) (v) / (1024 * 1024)

MemoryQueueItem::MemoryQueueItem(SurfacePatchRadiosityRenderer** renderer)
	: mRenderer(renderer),
		mByteSize((*renderer)->getByteSize())
{

}

long MemoryQueueItem::getMemoryUsage() const
{
	return mByteSize;
}

SurfaceRadiosityRenderer::SurfaceRadiosityRenderer(const PatchCollection& patches, float threshold, long maxMemoryUsage)
	: mPatches(patches),
		mThreshold(threshold),
		mMaxMemoryUsage(maxMemoryUsage),
		mExistingRenderersCount(0),
		mFormFactorsCount(0)
{
	mPatchRenderers.resize(mPatches.size(), NULL);
}

SurfaceRadiosityRenderer::~SurfaceRadiosityRenderer()
{
	for (auto r = mPatchRenderers.begin(); r != mPatchRenderers.end(); ++r)
	{
		if (*r)
		{
			delete *r;
			*r = NULL;
		}
	}
}

long SurfaceRadiosityRenderer::getUsedMemoryCapacity() const
{
	return mPatches.size() * sizeof(Patch) + 
				 (sizeof(MemoryQueueItem) + sizeof(SurfacePatchRadiosityRenderer)) * mExistingRenderersCount +
				 SurfacePatchRadiosityRenderer::getFormFactorDescByteSize() * mFormFactorsCount;
}

Color SurfaceRadiosityRenderer::computeRadiosity(int patch)
{
	SurfacePatchRadiosityRenderer* renderer = mPatchRenderers[patch];
	if (!renderer)
	{
		FormFactorRenderer::getInstance().reset();
		renderer = FormFactorRenderer::getInstance().render(mPatches, patch, mThreshold);
		mPatchRenderers[patch] = renderer;

		++mExistingRenderersCount;
		mFormFactorsCount += renderer->getFormFactorsCount();
		mMemoryQueue.push(MemoryQueueItem(&renderer));
	}
	else
	{
		std::cout << "Updating previous values..." << std::endl;
		renderer->updatePreviousRadiosityValues();
	}

	Color radiosity = renderer->computeRadiosity();

	// Free memory, if we've jumped out of bounds
	if (getUsedMemoryCapacity() >= mMaxMemoryUsage)
	{
		std::cout << "Memory limit exceeded, removing renderer. Current capacity is: " << MIBS(getUsedMemoryCapacity()) << "MiBs" << std::endl;

		// Remove largest renderer
		MemoryQueueItem& item = mMemoryQueue.top();

		SurfacePatchRadiosityRenderer** largest = item.getRenderer();
		mFormFactorsCount -= (*largest)->getFormFactorsCount();

		mMemoryQueue.pop();

		delete *largest;
		*largest = NULL;

		std::cout << "Capacity after renderer removed: " << MIBS(getUsedMemoryCapacity()) << "MiBs" << std::endl;
	}

	return radiosity;
}
