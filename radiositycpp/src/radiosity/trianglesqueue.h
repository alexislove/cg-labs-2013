//-------------------------------------------------------------------
// File: trianglesstack.h
// 
// This class represents stack of patch triangle primitives
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef RADIOSITY_TRIANGLEQUEUE_H
	#define RADIOSITY_TRIANGLEQUEUE_H

	#include <queue>

	#include "patchtriangle.h"
	#include "trianglescollection.h"

	class TrianglesQueue
	{
	public:
		explicit TrianglesQueue();

		// Add single triangle
		void enqueue(const PatchTriangle& tri);

		// Add collection of triangles
		void enqueue(const TrianglesCollection& tris);

		// Pop triangle from the stack
		PatchTriangle dequeue();

		bool empty() const
		{
			return mTriangles.empty();
		}

	private:
		std::queue<PatchTriangle> mTriangles;
	};

#endif // RADIOSITY_TRIANGLESTACK_H

