//-------------------------------------------------------------------
// File: scene.cpp
// 
// This class represents scene, that can be rendered using radiosity renderer
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include "surface.h"
#include "surfacescollection.h"

#include "scene.h"

Scene::Scene()
	: mSurfaces(new SurfacesCollection())
{

}

void Scene::addSurface(std::shared_ptr<Surface> surface)
{
	mSurfaces->add(surface);
}

void Scene::split(float size)
{
	mSurfaces->split(size);
}

std::shared_ptr< SurfacesCollection > Scene::getSurfaces() const
{
	return mSurfaces;
}

std::shared_ptr< PatchCollection > Scene::mergeSurfaces() const
{
	std::shared_ptr< PatchCollection > collection(new PatchCollection());

	IPatchIteratorPtr patch = mSurfaces->getPatchIterator();

	for (; !patch->isNull(); patch->next())
	{
		collection->add(**patch);
	}

	return collection;
}

void Scene::setupLightSources()
{
	IPatchIteratorPtr patch = mSurfaces->getPatchIterator();

	for (; !patch->isNull(); patch->next())
	{
		Patch& p = **patch;

		if (p.TheMaterial.Radiosity.r() < p.TheMaterial.Emission.r())
			p.TheMaterial.Radiosity.setR(p.TheMaterial.Emission.r());
		if (p.TheMaterial.Radiosity.g() < p.TheMaterial.Emission.g())
			p.TheMaterial.Radiosity.setG(p.TheMaterial.Emission.g());
		if (p.TheMaterial.Radiosity.b() < p.TheMaterial.Emission.b())
			p.TheMaterial.Radiosity.setB(p.TheMaterial.Emission.b());
	}
}

