//-------------------------------------------------------------------
// File: trianglescollection.cpp
// 
// This class represents collection of patch triangle primitives
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include <algorithm>

#include "trianglescollection.h"

TrianglesCollection::TrianglesCollection()
{

}

TrianglesCollection::~TrianglesCollection()
{

}

void TrianglesCollection::add(const PatchTriangle& tri)
{
	mTriangles.push_back(tri);
}

void TrianglesCollection::append(const TrianglesCollection& tris)
{
	mTriangles.resize(size() + tris.size());

	std::copy(tris.begin(), tris.end(), begin() + size());
}
