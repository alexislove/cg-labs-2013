//-------------------------------------------------------------------
// File: surfacepatchradiosityrenderer.cpp
// 
// This class represents radiosity renderer for surface patches
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include "patch.h"

#include "surfacepatchradiosityrenderer.h"

SurfacePatchRadiosityRenderer::SurfacePatchRadiosityRenderer(const PatchCollection& patches, float threshold)
	: mPatches(patches),
		mThreshold(threshold)
{

}

void SurfacePatchRadiosityRenderer::storeColorAndFormFactor(int patch, float formFactor)
{
	if (formFactor <= mThreshold)
	{
		return;
	}

	const Patch& surfacePatch = mPatches[patch];
	// Cache radiosity value
	AreaRadiosity color = {
		patch,
		formFactor,
		surfacePatch.PrevRadiosity
	};

	mAreaRadiosities.push_back(color);
}

Color SurfacePatchRadiosityRenderer::computeRadiosity() const
{
	Color res;

	for each (auto rad in mAreaRadiosities)
	{
		res += (rad.RadiosityColor * rad.FormFactor);
	}

	return res;
}

void SurfacePatchRadiosityRenderer::updatePreviousRadiosityValues()
{
	for (auto item = mAreaRadiosities.begin(); item != mAreaRadiosities.end(); ++item)
	{
		AreaRadiosity& ar = *item;
		ar.RadiosityColor = mPatches[ar.Patch].PrevRadiosity;
	}
}