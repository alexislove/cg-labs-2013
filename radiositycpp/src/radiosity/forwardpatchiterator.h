//-------------------------------------------------------------------
// File: forwardpatchiterator.h
// 
// This class represents forward patch iterator implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef RADIOSITY_FORWARDPATCHITERATOR_H
	#define RADIOSITY_FORWARDPATCHITERATOR_H

	#include "ipatchiterator.h"

	class TrianglesCollection;

	class ForwardPatchIterator : public IPatchIterator
	{
	public:
		explicit ForwardPatchIterator(PatchCollection& tris, int index = 0);

		ForwardPatchIterator(const ForwardPatchIterator& other);

		~ForwardPatchIterator();

		virtual IPatchIteratorPtr clone() const;

		virtual Patch& operator*();

		virtual void next();

		virtual bool isNull() const;

		virtual bool hasNext() const;

	private:
		//! Reference to the owner's container
		PatchCollection& mOwner;
		//! Index of current patch
		unsigned				 mIndex;
	};

	class ForwardPatchConstIterator : public IPatchConstIterator
	{
	public:
		explicit ForwardPatchConstIterator(const PatchCollection& tris, int index = 0);

		ForwardPatchConstIterator(const ForwardPatchConstIterator& other);

		~ForwardPatchConstIterator();

		virtual IPatchConstIteratorPtr clone() const;

		virtual const Patch& operator*() const;

		virtual void next();

		virtual bool isNull() const;

		virtual bool hasNext() const;

	private:
		//! Reference to the owner's container
		const PatchCollection& mOwner;
		//! Index of current patch
		unsigned							 mIndex;
	};

#endif // RADIOSITY_FORWARDPATCHITERATOR_H