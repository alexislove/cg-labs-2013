//-------------------------------------------------------------------
// File: trianglesqueue.cpp
// 
// This class represents queue of patch triangle primitives
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include "trianglesqueue.h"

TrianglesQueue::TrianglesQueue()
{

}

void TrianglesQueue::enqueue(const PatchTriangle& tri)
{
  mTriangles.push(tri);
}

void TrianglesQueue::enqueue(const TrianglesCollection& tris)
{
  for each (auto tri in tris) 
  {
		mTriangles.push(tri);
  }
}

PatchTriangle TrianglesQueue::dequeue()
{
  PatchTriangle front = mTriangles.front();
  mTriangles.pop();
  return front;
}
