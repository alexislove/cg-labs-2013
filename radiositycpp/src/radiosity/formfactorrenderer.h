//-------------------------------------------------------------------
// File: formfactorrenderer.h
// 
// This class represents form factor renderer, that renders hemicube
// from the destination patch, applying color corrections using sin/cos
// precalculated map. Also this is a singleton.
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef RADIOSITY_FORMFACTORRENDERER_H
	#define RADIOSITY_FORMFACTORRENDERER_H

	#include <windows.h>

	#include <hash_map>
	#include <memory>
	#include <vector>

	#include "patch.h"

	class Surface;
	class SurfacePatchRadiosityRenderer;
	class Vector3D;

	class FormFactorRenderer
	{
	public:

		static FormFactorRenderer& getInstance()
		{
			if (mInstance)
				return *mInstance;
			mInstance.reset(new FormFactorRenderer);
			return *mInstance;
		}

	public:
		void initialize();

		//! Reset renderer, before drawing new hemicube. Settings won't be reset
		void reset();

		//! Render given surface, visible within given patch and return surface patch renderer instance
		SurfacePatchRadiosityRenderer* render(const PatchCollection& patches, int patch, float patchThreshold);

	private:
		FormFactorRenderer();

		void calculateCorrectionFactors();

		void renderHemicube(const PatchCollection& patches, int x, int y, const Vector3D& center, const Vector3D& at, const Vector3D& up);

		void doRender(const PatchCollection& patches, int patch);

		void doRender(const PatchCollection& patches);

		void screenToFormFactorHash(std::hash_map<unsigned, float>* hash);

	private:
		//! Canonical instance of renderer
		static std::unique_ptr<FormFactorRenderer> mInstance;

	private:
		// Hemicube settings
		float mHemicubeEdgeSize;
		float mHemicubeImportantArea;
		float mViewportSize;
		// Color correction factors (mHemicubeImportantArea * mHemicubeImportantArea array)
		std::vector< std::vector<float> > mCorrections;

		HWND mhWnd;
	};

#endif // RADIOSITY_FORMFACTORRENDERER_H