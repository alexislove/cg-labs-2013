//-------------------------------------------------------------------
// File: surfaceradiosityrenderer.h
// 
// This class represents radiosity renderer for a surface
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef RADIOSITY_SURFACERADIOSITYRENDERER_H
	#define RADIOSITY_SURFACERADIOSITYRENDERER_H

	#include <vector>
	#include <queue>

	#include "illumination/color.h"

	#include "patch.h"

	class SurfacePatchRadiosityRenderer;

	class MemoryQueueItem
	{
	public:
		explicit MemoryQueueItem(SurfacePatchRadiosityRenderer** renderer);

		// Implicitly used by std::priority_queue
		operator long() const
		{
			return getMemoryUsage();
		}

		long getMemoryUsage() const;

		SurfacePatchRadiosityRenderer** getRenderer() const
		{
			return mRenderer;
		}

	private:
		SurfacePatchRadiosityRenderer** mRenderer;
		long														mByteSize;
	};

	class SurfaceRadiosityRenderer
	{
	public:
		explicit SurfaceRadiosityRenderer(const PatchCollection& patches, float threshold, long maxMemoryUsage);

		~SurfaceRadiosityRenderer();

		// Compute radiosity for the given patch of the collection
		Color computeRadiosity(int patch);

		// Get amount of memory used by renderers
		long getUsedMemoryCapacity() const;

	private:
		//! Patch collection instance
		const PatchCollection&					 mPatches;
		//! Threshold for patches
		float														 mThreshold;
		//! Maximum memory usage in bytes allowed for the renderer
		long														 mMaxMemoryUsage;
		//! Number of existing renderers != mPatchRendereres.size()
		long														 mExistingRenderersCount;
		//! Number of stored form factors
		long														 mFormFactorsCount;

		//! Storage for patch renderers
		std::vector< SurfacePatchRadiosityRenderer* > mPatchRenderers;
		//! Priority queue for memory usage optimization
		std::priority_queue< MemoryQueueItem >				mMemoryQueue;
	};

#endif // RADIOSITY_SURFACECOLLECTIONRADIOSITYRENDERER_H

