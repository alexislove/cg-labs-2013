//-------------------------------------------------------------------
// File: patch.h
// 
// This file contains definition of the patch, used by radiosity renderer
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef RADIOSITY_PATCH_H
	#define RADIOSITY_PATCH_H

	#include "patchtriangle.h"
	#include "trianglescollection.h"

	// Below - there's a "black box" of the patch, that allows us to easily change it's type

	// Use patch triangle as main patch unit
	typedef PatchTriangle				Patch;
	typedef TrianglesCollection PatchCollection;

#endif // RADIOSITY_PATCH_H