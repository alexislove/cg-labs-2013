//-------------------------------------------------------------------
// File: forwardpatchcollectioniterator.h
// 
// This class represents iterator over patches in surfaces collection
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include <assert.h>

#include "surface.h"
#include "surfacescollection.h"

#include "forwardpatchcollectioniterator.h"

/**
 ForwardPatchCollectionIterator
 */
ForwardPatchCollectionIterator::ForwardPatchCollectionIterator(SurfacesCollection& collection, int surface/* = 0 */, IPatchIteratorPtr patch/* = IPatchIteratorPtr()*/)
	: mOwner(collection),
		mSurfaceIndex(surface)
{
	if (patch)
	{
		mPatch = patch->clone();
	}
	if (!mPatch && collection.size() > 0)
	{
		mPatch = collection[0].getPatchIterator();
	}
}

ForwardPatchCollectionIterator::ForwardPatchCollectionIterator(const ForwardPatchCollectionIterator& other)
	: mOwner(other.mOwner),
		mSurfaceIndex(other.mSurfaceIndex)
{
	if (other.mPatch)
	{
		mPatch = other.mPatch->clone();
	}
}

ForwardPatchCollectionIterator::~ForwardPatchCollectionIterator()
{

}

IPatchIteratorPtr ForwardPatchCollectionIterator::clone() const
{
	return IPatchIteratorPtr(new ForwardPatchCollectionIterator(*this));
}

Patch& ForwardPatchCollectionIterator::operator*() 
{
	assert(mPatch && !mPatch->isNull() && "Patch iterator out of range!");
	return *(*mPatch);
}

void ForwardPatchCollectionIterator::next()
{
	if (!mPatch->hasNext())
	{
		++mSurfaceIndex;
		if (mSurfaceIndex < mOwner.size())
		{
			mPatch = mOwner[mSurfaceIndex].getPatchIterator();
		}
		else
		{
			mPatch.reset();
		}

		return;
	}

	mPatch->next();
}

bool ForwardPatchCollectionIterator::isNull() const
{
	if (!mPatch)
	{
		return true;
	}
	// At the end of all collection
	return mPatch->isNull() && mSurfaceIndex >= mOwner.size();
}

bool ForwardPatchCollectionIterator::hasNext() const
{
	if (!mPatch)
	{
		return false;
	}
	return mSurfaceIndex != (mOwner.size() - 1);
}

/**
 ForwardPatchCollectionConstIterator
 */

ForwardPatchCollectionConstIterator::ForwardPatchCollectionConstIterator(const SurfacesCollection& collection, int surface /* = 0 */, IPatchConstIteratorPtr patch /* = IPatchConstIteratorPtr */)
	: mOwner(collection),
		mSurfaceIndex(surface)
{
	if (patch)
	{
		mPatch = patch->clone();
	}
	if (!mPatch && collection.size() > 0)
	{
		mPatch = collection[0].getPatchIterator();
	}
}

ForwardPatchCollectionConstIterator::ForwardPatchCollectionConstIterator(const ForwardPatchCollectionConstIterator& other)
	: mOwner(other.mOwner),
		mSurfaceIndex(other.mSurfaceIndex)
{
	if (other.mPatch)
	{
		mPatch = other.mPatch->clone();
	}
}

ForwardPatchCollectionConstIterator::~ForwardPatchCollectionConstIterator()
{
}

IPatchConstIteratorPtr ForwardPatchCollectionConstIterator::clone() const
{
	return IPatchConstIteratorPtr(new ForwardPatchCollectionConstIterator(*this));
}

const Patch& ForwardPatchCollectionConstIterator::operator*() const
{
	assert(!mPatch->isNull() && "Patch iterator out of range!");
	return *(*mPatch);
}

void ForwardPatchCollectionConstIterator::next()
{
	if (!mPatch->hasNext())
	{
		++mSurfaceIndex;
		if (mSurfaceIndex < mOwner.size())
		{
			mPatch = mOwner[mSurfaceIndex].getPatchIterator();
		}

		return;
	}

	mPatch->next();
}

bool ForwardPatchCollectionConstIterator::isNull() const
{
	if (!mPatch)
	{
		return false;
	}
	// At the end of all collection
	return mPatch->isNull() && mSurfaceIndex >= mOwner.size();
}

bool ForwardPatchCollectionConstIterator::hasNext() const
{
	if (!mPatch)
	{
		return false;
	}
	return mSurfaceIndex != (mOwner.size() - 1);
}
