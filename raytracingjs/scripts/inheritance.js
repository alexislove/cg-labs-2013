﻿/*
* @author Alexey Vinogradov
* @copyright Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
*/

/*
 * Extend given target prototype with given source prototype properties.
 * @param {Object} target Target class object (not a prototype!)
 * @param {Object} source Source class object (not a prototype!)
 * @return {Object} Target object
 */
function extend (target, source) {
    var hasOwnProperty = Object.prototype.hasOwnProperty;

    // Copy all own properties from source prototype to target one
    for (var property in source.prototype) {
        // Call 'hasOwnProperty' with this == source and 
        // also check, that target hasn't already defined this property
        if (hasOwnProperty.call(source.prototype, property) &&
            !hasOwnProperty.call(target.prototype, property)) {
            target.prototype[property] = source.prototype[property];
        }
    }

    return target;
}
