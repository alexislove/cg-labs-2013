﻿/*
 *  @author Alexey Vinogradov
 *  @copyright Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
 */

/**
 * @name Application
 * @class This class represents whole application, containing objects scene and performing animation frames

/**
* Application constructor
* @constructor
*/
function Application() {
    
}

// Ctor
Application.prototype.constructor = Application;

/**
public:
*/

/*
* Load application data
*/
Application.prototype.load = function () {
    // Create <div> element for application
    var applicationDiv = document.createElement('div');
    applicationDiv.id = g_ApplicationUniformTraits['div_name'];
    applicationDiv.align = 'center';
    document.body.appendChild(applicationDiv);

};

/*
* Start animation frame
*/
Application.prototype.animate = function () {

};

/**
 * Callback for <code>setInterval</code>
 */
function animate() {
    window.application.animate();
}

// Create application when window is loading
window.onload = function () {
    window.application = new Application();
    window.application.load();

    //setInterval(animate, g_ApplicationUniformTraits['frame_timeout']);
}