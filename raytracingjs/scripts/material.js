﻿/**
* @author Alexey Vinogradov
* @copyright Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
*/

/**
 * @name MaterialProperties
 * @class This class represents properties, that can be assigned for material
 * @property {Color}   [color] Color properties of material
 * @property {Texture} [texture] Texture, associated with the object
 */

/**
 * @name Material
 * @class This class represents material, that can be assigned for scene objects
 */

/**
 * Material constructor
 * @param {MaterialProperties} [props] Material properties
 * @constructor
 */
function Material(props) {
    this.props_ = props;
}

Material.prototype.constructor = Material;

/**
 * Get properties
 * @return {MaterialProperties} Currently used properties
 */
Material.prototype.getProperties() = function () {
    return this.props_;
}