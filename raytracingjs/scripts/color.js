﻿/**
* @author Alexey Vinogradov
* @copyright Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
*/

/**
 * @name ColorProperties
 * @class This class represents properties, that can be assigned for a color
 * @property {Vector3D} [ambient] Ambient color component
 * @property {Number}   [Ka]      Ambient component factor
 * @property {Vector3D} [diffuse] Diffuse color component
 * @property {Number}   [Kd]      Diffuse component factor
 * @property {Vector3D} [specular] Specular color component
 * @property {Number}   [Ks]       Specular component factor
 */

 /**
  * @name Color
  * @class This class represents color, that can be assigned for an object's material
  */

/**
 * Color constructor
 * @param {ColorProperties} [props] Properties of the color
 * @constructor
 */
function Color(props) {
    this.props_ = props;
}

/**
 * Get color properties
 * @return {ColorProperties} Currently set properties
 */
function getProperties() {
    return this.props_;
}
