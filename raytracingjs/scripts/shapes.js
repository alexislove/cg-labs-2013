﻿/**
* @author Alexey Vinogradov
* @copyright Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
*/

/**
 * @name Shape
 * This is abstraction for all shapes, supported by ray tracer
 */

 /**
  * Constructor for shape
  * @param {Material} [material] Shape material
  * @constructor
  */
function Shape(material) {
    this.material_ = material;
}

Shape.prototype.constructor = Shape;

// No intersection constant
Shape.prototype.NoIntersection = -1.0;
// Precision constant
Shape.prototype.Epsilon        = 0.000001;

/**
 * Intersect shape with given ray. This is some kind of abstract method and throws exception, in case it's not overriden in subclass
 * @param {Ray} [ray] Ray instance
 * @return {Number} Distance on the ray, where intersection occured
 */
Shape.prototype.intersect = function (ray) {
    throw new Error("Shape method 'intersect' must be implemented in subclass!");
}

/**
 * Get shape's normal at given ray and distance
 * @param {Ray}    [ray]  Ray instance
 * @param {Number} [dist] Distance
 * @return {Vector3D} Calculated normal
 */
Shape.prototype.getNormal = function (ray, dist) {
    throw new Error("Shape method 'getNormal' must be implemented in subclass!");
}

/**
 * Get shape's material 
 * @return {Material} Assigned material instance
 */
Shape.prototype.getMaterial = function () {
    return this.material_;
}

/*************************************************************************************/

/**
 * @name Sphere
 * This class represents sphere shape, implementing Shape abstraction
 */

/**
 * Sphere constructor
 * @param {Vector3D} [center]   Sphere center
 * @param {Number}   [radius]   Sphere radius
 * @param {Material} [material] Sphere material
 */
function Sphere(center, radius, material) {
    Shape.call(this, material);

    this.center_  = center;
    this.radius_  = radius;
    this.radius2_ = radius * radius; // Additionally store squared radius
}

extend(Sphere, Shape);

Sphere.prototype.constructor = Sphere;

Sphere.prototype.intersect = function (ray) {
    // Solve square equation

    var centerOrigin = ray.getOrigin().substract(this.center_);
    // Calculate square equation factors
    var p = dotProduct(ray.getDirection(), centerOrigin);
    var q = dotProduct(centerOrigin, centerOrigin) - this.radius2_;

    // Calculate discriminant
    var D = p * p - q;

    if (D < 0) {
        return this.NoIntersection; // No solution
    }

    D = Math.Sqrt(D);
    var root1 = -p - D;
    var root2 = -p + D;

    if (root1 > 0) {
        return root1;
    }
    if (root2 > 0) {
        return root2;
    }

    // Both roots invalid
    return this.NoIntersection;
}

Sphere.prototype.getNormal = function (ray, dist) {
    var normal = ray.apply(dist).substract(this.center_).divide(this.radius_);
    return normalize(normal);
}

/*************************************************************************************/

/**
* @name Plane
* This class represents plane shape, implementing Shape abstraction
*/

/**
* Plane constructor
* @param {Vector3D} [normal]   Plane normal
* @param {Number}   [distance] Plane distance
* @param {Material} [material] Plane material
*/
function Plane(normal, distance, material) {
    Shape.call(this, material);

    this.normal_   = normalize(normal);
    this.distance_ = distance;
}

extend(Plane, Shape);

Plane.prototype.constructor = Plane;

Plane.prototype.intersect = function (ray) {
    var distance = dotProduct(ray.getDirection(), this.normal_);
    if (distance == 0.0) {
        return this.NoIntersection;
    }
    return (-(dotProduct(ray.getOrigin(), this.normal_) + this.distance_)) / distance;
}

Plane.prototype.getNormal = function (ray, dist) {
    // WTF, maybe just normal?
    return this.normal_;
    //var normal = ray.apply(dist);
    //return normalize(normal);
}

/*************************************************************************************/

/**
* @name Box
* This class represents box shape, implementing Shape abstraction
*/

/**
* Box constructor
* @param {Vector3D} [start]    One of the box's points
* @param {Vector3D} [end]      Second box's point
* @param {Material} [material] Box material
*/
function Box(start, end, material) {
    Shape.call(this, material);

    this.start_ = start;
    this.end_   = end;

    // Sort point coordinates
    var compStart = this.start_.getX();
    var compEnd   = this.end_.getX();
    if (compStart > compEnd) {
        Auxiliary.swap(compStart, compEnd);
        this.start_.setX(compStart);
        this.end_.setX(compEnd);
    }
    compStart = this.start_.getY();
    compEnd = this.end_.getY();
    if (compStart > compEnd) {
        Auxiliary.swap(compStart, compEnd);
        this.start_.setY(compStart);
        this.end_.setY(compEnd);
    }
    var compStart = this.start_.getZ();
    var compEnd = this.end_.getZ();
    if (compStart > compEnd) {
        Auxiliary.swap(compStart, compEnd);
        this.start_.setZ(compStart);
        this.end_.setZ(compEnd);
    }

    this.invDims_  = this.start_.add(this.end_.negate()).inverse();
    this.negStart_ = this.start_.negate();
}

extend(Box, Shape);

Box.prototype.constructor = Box;

Box.prototype.intersect = function (ray) {
    var intersections = new Array();
    this.getSideIntersections(intersections, "x_", "y_", "z_", ray);
    this.getSideIntersections(intersections, "y_", "x_", "z_", ray);
    this.getSideIntersections(intersections, "z_", "x_", "y_", ray);

    if (intersections.length == 0) {
        return this.NoIntersection;
    }

    // Sort found intersections
    var distance = 100000000; // Some huge distance
    var origin = ray.getOrigin();
    for (var item = 0; item < intersections.length; ++item) {
        var curLength = vectorLength(intersections[item].substract(origin));
        if (curLength < distance) {
            distance = curLength;
        }
    }

    return distance;
}

Box.prototype.getNormal = function (ray, dist) {
    // Calculate normal
    var position = ray.getOrigin().add(ray.getDirection().multiply(dist));

    var boxPosition = position.add(this.negStart_).scale3D(this.invDims_).add(new Vector3D(-.5, -.5, -.5)); // Add some shift
    var absBoxPosition = boxPosition.absolute();

    if (absBoxPosition.getX() > absBoxPosition.getY() && absBoxPosition.getX() > absBoxPosition.getZ()) {
        return normalize(new Vector3D(boxPosition.getX(), 0.0, 0.0));
    }
    if (absBoxPosition.getY() > absBoxPosition.getX() && absBoxPosition.getY() > absBoxPosition.getZ()) {
        return normalize(new Vector3D(0.0, boxPosition.getY(), 0.0));
    }

    return normalize(new Vector3D(0.0, 0.0, boxPosition.getZ()));
}

/**
 * Get intersections with sides, specified by axises
 * @param {Array} [outIntsArray] Output intersections array
 * @param {String} [axis]        Desired axis
 * @param {String} [outAxis1]    First output axis
 * @param {String} [outAxis2]    Second output axis
 * @param {Ray}    [ray]         Ray to intersect with
 */
Box.prototype.getSideIntersections = function (outIntsArray, axis, outAxis1, outAxis2, ray) {
    var origin = ray.getOrigin();
    var direction = ray.getDirection();
    if (ray.getDirection()[axis] == 0.0) {
        return;
    }
    var intFactor1 = (this.start_[axis] - origin[axis]) / direction[axis];
    var intFactor2 = (this.end_[axis] - origin[axis]) / direction[axis];

    var intPoint1 = origin.add(direction.multiply(intFactor1));
    var intPoint2 = origin.add(direction.multiply(intFactor2));

    // Check, whether intersection point are in proper bounds
    if (intPoint1[outAxis1] >= this.start_[outAxis1] && intPoint1[outAxis1] < this.end_[outAxis1] &&
        intPoint1[outAxis2] >= this.start_[outAxis2] && intPoint1[outAxis2] < this.end_[outAxis2]) {
        outIntsArray[outIntsArray.length] = intPoint1;
    }
    if (intPoint2[outAxis1] >= this.start_[outAxis1] && intPoint2[outAxis1] < this.end_[outAxis1] &&
        intPoint2[outAxis2] >= this.start_[outAxis2] && intPoint2[outAxis2] < this.end_[outAxis2]) {
        outIntsArray[outIntsArray.length] = intPoint2;
    }
}

/**
* @name Triangle
* This class represents triangle shape, implementing Shape abstraction
*/

/**
* Triangle constructor
* @param {Vector3D} [v0] First vertex
* @param {Vector3D} [v1] Second vertex
* @param {Vector3D} [v2] Third vertex
* @param {Material} [material] Triangle material
*/
function Triangle(v0, v1, v2, material) {
    Shape.call(this, material);

    this.v0_     = v0;
    this.v1_     = v1;
    this.v2_     = v2;
    this.normal_ = crossProduct(v1.substract(v0), v2.substract(v0));
    
}

extend(Triangle, Shape);

Triangle.prototype.constructor = Triangle;

Triangle.prototype.intersect = function (ray) {
    // Algorithm from 
    // http://www.cs.virginia.edu/~gfx/Courses/2003/ImageSynthesis/papers/Acceleration/Fast%20MinimumStorage%20RayTriangle%20Intersection.pdf

    // Barycentric coordinates intersection test

    // Get edges between vertices
    var edge20 = this.v2_.subtract(this.v0_);
    var edge10 = this.v1_.subtract(this.v0_);

    // Calculate determinant 
    var pvec = crossProduct(ray.getDirection(), edge20); // Will be used for calculating u
    var det = dotProduct(edge10, pvec);

    if (det < this.Epsilon) {
        return this.NoIntersection;
    }

    // Calculate distance from vertex 0 to ray origin
    var tvec = ray.getOrigin().substract(this.v0_);

    // At barycentric coordinates:
    // T(u, v) = (1 - u - v) * V0 + u * V1 + v * V2
    // And also the distance t

    // Calculate U param
    var u = dotProduct(tvec, pvec);

    if (u < 0.0 || u > det) {
        return this.NoIntersection;
    }

    // Prepare to calculate V param
    var qvec = crossProduct(tvec, edge10);

    var v = dotProduct(ray.getDirection(), qvec);

    if (v < 0.0 || u + v > det) {
        return this.NoIntersection;
    }

    // Calculate t - distance
    var t = dotProduct(edge20, qvec);
    var inv_det = 1.0 / det;

    t *= inv_det;
    return t;
}

Triangle.prototype.getNormal = function (ray, dist) {
    // WTF, maybe just normal?
    return this.normal_;
}

/**
* @name Cylinder
* This class represents cylinder shape, implementing Shape abstraction
*/

/**
* Cylinder constructor
* @param {Vector3D} [start]    Cylinder top point
* @param {Vector3D} [end]      Cylinder bottom point
* @param {Vector3D} [radius]   Cylinder radius
* @param {Boolean}  [isOpen]   Cylinder has or hasn't got top and bottom circles
* @param {Material} [material] Cylinder material
*/
function Cylinder(start, end, radius, isOpen, material) {
    Shape.call(this, material);

    this.start_  = start;
    this.end_    = end;
    this.radius_ = radius;
    this.isOpen_ = isOpen;

    // Initialize extra data for intersection detection optimization
    this.negStart_      = this.start_.negate();
    this.negEnd_        = this.end_.negate();
    this.normAxis_      = normalize(this.end_.add(this.negStart_));
    this.negNormAxis_   = this.normAxis_.negate();
    this.radius2_       = this.radius_ * this.radius_;

    // Calculate bounding box
    // TODO
}

extend(Cylinder, Shape);

Cylinder.prototype.constructor = Cylinder;

Cylinder.prototype.intersect = function (ray) {
    var intersections = new Array();
    var origin = ray.getOrigin();
    var direction = ray.getDirection();

    var toEnd = origin.add(this.negEnd_);

    var closestIntersection = 100000000;
    var upDownIntersected = false;
    // Up/down sides
    if (!this.isOpen_) {
        var normAxisAngle = dotProduct(direction, this.normAxis_);
        if (normAxisAngle != 0) { // Not parallel
            var rayAngle = dotProduct(this.normAxis_, toEnd);
            var distance = -rayAngle / normAxisAngle;

            if (vectorLength2(origin.add(direction.multiply(distance)).add(this.negStart_)) < this.radius2_) {
                closestIntersection = distance;
                upDownIntersected = true;
            }

            rayAngle = dotProduct(this.negNormAxis_, origin.add(this.negEnd_));
            distance = rayAngle / normAxisAngle;
            if (vectorLength2(origin.add(direction.multiply(distance)).add(this.negEnd_)) < this.radius2_) {
                if (distance < closestIntersection) {
                    closestIntersection = distance;
                    upDownIntersected = true;
                }
            }
        }
    }
    upDownIntersected = (closestIntersection >= 0);
    // Cylinder side
    if (upDownIntersected) {
        return closestIntersection;
    }
    var u = direction.add(this.normAxis_.multiply(-dotProduct(direction, this.normAxis_)));
    var v = toEnd.add(this.normAxis_.multiply(-dotProduct(toEnd, this.normAxis_)));

    // Suppose a, b and c are factors of the square equation
    var a = dotProduct(u, u);
    if (a != 0) {
        // a = b = c = 0 - ray is travelling along cylinder side
        var b = 2 * dotProduct(u, v);
        var c = dotProduct(v, v) - this.radius2_;

        var D = b * b - 4 * a * c;
        if (D < 0.0) {
            return this.NoIntersection;
        }
        D = Math.Sqrt(D);
        var denom = 1 / (2 * a);
        // Solve square equation

        var root1 = (-b - D) * denom;
        if (root1 > 0.0) {
            return root1;
        }
        var root2 = (-b + D) * denom;
        if (root2 > 0.0) {
            return root2;
        }
        return this.NoIntersection;
    }
    

    return this.NoIntersection;
}

Cylinder.prototype.getNormal = function (ray, dist) {
    // Check, whether ray intersects up/down sides
    var origin = ray.getOrigin();
    var direction = ray.getDirection();

    var normAxisAngle = dotProduct(direction, this.normAxis_);
    if (normAxisAngle != 0) {
        if (vectorLength2(origin.add(direction.multiply(dist)).add(this.negStart_)) < this.radius2_) {
            return this.negNormAxis_;
        }
        if (vectorLength2(origin.add(direction.multiply(dist)).add(this.negEnd_)) < this.radius2_) {
            return this.normAxis_;
        }
    }

    var position = ray.apply(dist);

    // If we're here get normal to the side
    var nearestToAxis = this.normAxis_.multiply(dotProduct(position.add(this.negStart_), this.normAxis_)).add(this.start_);
    return normalize(position.add(nearestToAxis.negate()));
}

/**
* @name Cone
* This class represents cone shape, implementing Shape abstraction
*/

/**
* Cone constructor
* @param {Vector3D} [top]          Top point
* @param {Number}   [topRadius]    Top radius
* @param {Vector3D} [bottom]       Bottom point
* @param {Number}   [bottomRadius] Bottom radius
* @param {Boolean}  [isOpen]       Cone has or hasn't got bottom circle
* @param {Material} [material]     Cone material
*/
function Cone(top, topRadius, bottom, bottomRadius, isOpen, material) {
    Shape.call(this, material);

    this.top_          = top;
    this.topRadius_    = topRadius;
    this.bottom_       = bottom;
    this.bottomRadius_ = bottomRadius;
    this.isOpen_       = isOpen;

    // Calculate some values as optimization
    this.negTop_        = this.top_.negate();
    this.negBottom_     = this.bottom_.negate();
    this.normAxis_      = normalize(this.bottom_.add(this.negTop_));
    this.negNormAxis_   = this.normAxis_.negate();
    this.topRadius2_    = this.topRadius_ * this.topRadius_;
    this.bottomRadius2_ = this.bottomRadius_ * this.bottomRadius_;
    this.dRadius_       = (this.bottomRadius_ - this.topRadius_) / vectorLength(this.bottom_.add(this.negTop_)); // Radius difference per length unit
}

extend(Cone, Shape);

Cone.prototype.constructor = Cone;

Cone.prototype.intersect = function (ray) {
    var origin = ray.getOrigin();
    var direction = ray.getDirection();

    var toTop = origin.add(this.negTop_);

    var circleIntersected = false;
    var closestIntersection = 1000000;
    // Check intersection with circles
    if (!this.isOpen_) {
        var normAxisAngle = dotProduct(direction, this.normAxis_);
        if (a != 0) {
            var rayAngle = dotProduct(this.normAxis_, toTop);
            var distance = -rayAngle / normAxisAngle;

            if (vectorLength2(origin.add(direction.multiply(distance)).add(this.negTop_)) < this.topRadius2_) {
                closestIntersection = distance;
                circleIntersected = true;
            }

            rayAngle = dotProduct(this.negNormAxis_, origin.add(this.negBottom_));
            distance = rayAngle / normAxisAngle;
            if (vectorLength2(origin.add(direction.multiply(distance)).add(this.negBottom_)) < this.bottomRadius2_) {
                if (distance < closestIntersection) {
                    closestIntersection = distance;
                    circleIntersected = true;
                }
            }
        }
    }
    circleIntersected = (closestIntersection > 0.0);
    if (circleIntersected) {
        return closestIntersection;
    }

    var dirDotNorm = dotProduct(direction, this.normAxis_);
    var toTopDotNorm = dotProduct(toTop, this.normAxis_);

    var u = direction.add(this.normAxis_.multiply(-dirDotNorm));
    var v = toTop.add(this.normAxis_.multiply(-toTopDotNorm));
    var w = toTopDotNorm * this.dRadius_ + this.topRadius_;

    var dRadiusDirDotNorm = dirDotNorm * this.dRadius_;

    // Let a, b, c be factors of square equation
    var a = dotProduct(u, u) - dRadiusDirDotNorm * dRadiusDirDotNorm;
    if (a != 0) {
        // a = b = c = 0 - ray is travelling along cone side
        var b = 2 * (dotProduct(u, v) - w * dRadiusDirDotNorm);
        var c = dotProduct(v, v) - w * w;

        // Calculate discriminant and solve equation
        var D = b * b - 4 * a * c;

        if (D < 0) {
            return this.NoIntersection;
        }

        D = Math.Sqrt(D);
        var denom = 1 / (2 * a);

        var root1 = (-b - D) * denom;
        if (root1 > 0.0) {
            return root1;
        }
        var root2 = (-b + D) * denom;
        if (root2 > 0.0) {
            return root2;
        }
    }

    return this.NoIntersection;
}

Cone.prototype.getNormal = function (ray, dist) {
    // Check, whether ray intersects up/down sides
    var origin = ray.getOrigin();
    var direction = ray.getDirection();

    var normAxisAngle = dotProduct(direction, this.normAxis_);
    if (normAxisAngle != 0) {
        if (vectorLength2(origin.add(direction.multiply(dist)).add(this.negTop_)) < this.topRadius2_) {
            return this.negNormAxis_;
        }
        if (vectorLength2(origin.add(direction.multiply(dist)).add(this.negBottom_)) < this.bottomRadius2_) {
            return this.normAxis_;
        }
    }

    var position = ray.apply(dist);

    var pointOnAxis = this.normAxis_.multiply(dotProduct(position.add(this.negTop_), this.normAxis_), this.top_);
    var simpleNorm = position.add(pointOnAxis.negate());
    return normalize(simpleNorm.add(this.normAxis_.multiply(-this.dRadius_ * vectorLength(simpleNorm))));
}
