﻿/*
* @author Alexey Vinogradov
* @copyright Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
*/

/**
* @name Vector3D
* @class 3D vector implementation.
*/

/**
 * Constructor for 3D vector
 * @param {Number} [x] X coordinate
 * @param {Number} [y] Y coordinate
 * @constructor
 */
function Vector3D(x, y, z) {
    this.x_ = x;
    this.y_ = y;
    this.z_ = z;
}

// Ctor
Vector3D.prototype.constructor = Vector3D;

/**
* Clone this instance of Vector3D
* @return {Vector3D} Cloned instance
*/
Vector3D.prototype.clone = function () {
    return new Vector3D(this.x_, this.y_, this.z_);
};

/**
* Get x coordinate
* @return {Number} X coordinate
*/
Vector3D.prototype.getX = function () {
    return this.x_;
};

/**
 * Get y coordinate
 * @return {Number} Y coordinate
 */
Vector3D.prototype.getY = function () {
    return this.y_;
};

/**
 * Get z coordinate
 * @return {Number} Z coordinate
 */
Vector3D.prototype.getZ = function () {
    return this.z_;
};

/*
 * Set x coordinate
 * @param {Number} [x] X coordinate
 */
Vector3D.prototype.setX = function (x) {
    this.x_ = x;
};

/**
* Set y coordinate
* @param {Number} [y] Y coordinate
*/
Vector3D.prototype.setY = function (y) {
    this.y_ = y;
};

/**
* Set z coordinate
* @param {Number} [z] Z coordinate
*/
Vector3D.prototype.setZ = function (z) {
    this.z_ = z;
};

/**
* Add another point
* @param {Vector3D} [rh] Another point instance
* @return {Vector3D} Sum
*/
Vector3D.prototype.add = function (rh) {
    return new Vector3D(this.x_ + rh.x_, this.y_ + rh.y_, this.z_ + rh.z_);
};

/**
* Add another point to this instance
* @param {Vector3D} [rh] Another point instance
* @return {Vector3D} this instance
*/
Vector3D.prototype.inplaceAdd = function (rh) {
    this.x_ += rh.x_;
    this.y_ += rh.y_;
    this.z_ += rh.z_;
    return this;
};

/**
* Subtract another point
* @param {Vector3D} [rh] Another point instance
* @return {Vector3D} Difference
*/
Vector3D.prototype.substract = function (rh) {
    return new Vector3D(this.x_ - rh.x_, this.y_ - rh.y_, this.z_ - rh.z_);
};

/**
* Subtract another point from this instance
* @param {Vector3D} [rh] Another point instance
* @return {Vector3D} this instance
*/
Vector3D.prototype.inplaceSubstract = function (rh) {
    this.x_ -= rh.x_;
    this.y_ -= rh.y_;
    this.z_ -= rh.z_;
    return this;
};

/**
* Multiply point coordinates on given value
* @param {Number} [value] Value to multiply on
* @return {Vector3D} Result point
*/
Vector3D.prototype.multiply = function (value) {
    return new Vector3D(this.x_ * value, this.y_ * value, this.z_ * value);
};

/**
* Multiply this point coordinates on given value
* @param {Number} [value] Value to multiply on
* @return {Vector3D} this instance
*/
Vector3D.prototype.inplaceMultiply = function (value) {
    this.x_ *= value;
    this.y_ *= value;
    this.z_ *= value;
    return this;
};

/**
* Divide point coordinates on given value
* @param {Number} [value] Value to divide on
* @return {Vector3D} Result point
*/
Vector3D.prototype.divide = function (value) {
    return new Vector3D(this.x_ / value, this.y_ / value, this.z_ / value);
};

/**
* Divide this point coordinates on given value
* @param {Number} [value] Value to divide on
* @return {Vector3D} this instance
*/
Vector3D.prototype.inplaceDivide = function (value) {
    this.x_ /= value;
    this.y_ /= value;
    this.z_ /= value;
    return this;
};

/**
 * Multiply all this coordinates on coordinates of the given vector
 * @param {Vector3D} [rh] Scale vector instance
 * @return {Vector3D} Result vector
 */
Vector3D.prototype.scale3D = function (rh) {
    return new Vector3D(this.x_ * rh.x_, this.y_ * rh.y_, this.z_ * rh.z_);
}

/**
 * Get absolute coordinates for all components of this vector
 * @return {Vector3D} (abs(x), abs(y), abs(z)) vector
 */
Vector3D.prototype.absolute = function () {
    return new Vector3D(Math.abs(this.x_), Math.abs(this.y_), Math.abs(this.z_));
}

/**
* Negate coordinates of the point
* @return {Vector3D} Vector3D with coordinates (-x, -y, -z)
*/
Vector3D.prototype.negate = function () {
    return new Vector3D(-this.x_, -this.y_, -this.z_);
};

/**
 * Inverse coordinates of the point
 * @return {Vector3D} Vector3D with coordinates (1 / x, 1 / y, 1 / z)
 */
Vector3D.prototype.inverse = function () {
    return new Vector3D(1 / this.x_, 1 / this.y_, 1 / this.z_);
}

/**
* Check points equality
* @param {Vector3D} [point] Test point instance
* @return {Boolean} <code>true</code> - points coordinates match, <code>false</code> otherwise
*/
Vector3D.prototype.isEqualTo = function (point) {
    return (this.getX() == point.getX() && this.getY() == point.getY() && this.getZ() == point.getZ());
};

/**
 * Get string representation of a point
 * @return {String} Vector3D string
 */
Vector3D.prototype.toString = function () {
    return 'x: ' + this.getX() + ' ' + 'y: ' + this.getY() + ' ' + 'z: ' + this.getZ();
};


/**
 * Calculate dot product of 2 points
 * @param {Vector3D} [lh] First point
 * @param {Vector3D} [rh] Second point
 * @return {Number} Dot product value
 */
function dotProduct(lh, rh) {
  return lh.getX() * rh.getX() + lh.getY() * rh.getY() + lh.getX() * rh.getX();
}

/**
 * Calculate cross product of 2 vectors
 * @param {Vector3D} [lh] First vector
 * @param {Vector3D} [rh] Second vector
 * @return {Vector3D} Cross vector
 */
function crossProduct(lh, rh) {
    return new Vector3D(lh.getY() * rh.getZ() - lh.getZ() * rh.getY(),
                        lh.getZ() * rh.getX() - lh.getX() * rh.getZ(),
                        lh.getX() * rh.getY() - lh.getY() * rh.getX());
}


/**
* Calculate length of the vector
* @param {Vector3D} [point] Vector instance
* @return {Number} Calculated length
*/
function vectorLength(point) {
  return Math.sqrt(dotProduct(point, point));
}

/**
* Calculate squared length of the vector
* @param {Vector3D} [point] Vector instance
* @return {Number} Calculated length
*/
function vectorLength2(point) {
    return dotProduct(point, point);
}


/** 
* Normalize point coordinates - clamp coordinates to [0, 1] range, dividing them on the point length
* @param {Vector3D} [point] Vector3D instance
* @return {Vector3D} Normalized point instance
*/
function normalize(point) {
  var length = pointLength(point);
  return new Vector3D(point.getX() / length, point.getY() / length, point.getZ() / length);
}

/************************************************************************/

/**
* @name Segment
* @class This class represents segment described by two points
*/

/**
 * Constructor for segment
 * @param {Vector3D} [start] Segment start
 * @param {Vector3D} [end]   Segment end
 * @constructor
 */

function Segment(start, end) {
    this.start_ = start;
    this.end_   = end;
}


// Ctor
Segment.prototype.constructor = Segment;

/**
* Get segment start
* @return {Vector3D} Segment start
*/
Segment.prototype.getStart = function() {
    return this.start_;
};

/**
* Get segment end
* @return {Vector3D} Segment end
*/
Segment.prototype.getEnd = function() {
    return this.end_;
};

/**
 * Calculate segment length
 * @param {Segment} [segment] Segment instance
 * @return {Number} Length of the segment
 */
function segmentLength(segment) {
  return pointLength(segment.getStart().substract(segment.getEnd()));
}

/************************************************************************/

/**
 * @name Ray
 * @class This class represents ray with given origin and direction (simple geometry ray)
 */

/**
 * Constructor for ray
 * @param {Vector3D} org Ray origin
 * @param {Vector3D} dir Ray direction
 */
function Ray(org, dir) {
    this.origin_ = org;
    this.dir_    = normalize(dir); // Ensure, direction is normalized
}

Ray.prototype.constructor = Ray;

/**
 * Get ray origin
 * @return {Vector3D} Current origin
 */
Ray.prototype.getOrigin = function () {
    return this.origin_;
}

/**
* Get ray direction
* @return {Vector3D} Current direction
*/
Ray.prototype.getDirection = function () {
    return this.dir_;
}

/**
* Get point at given distance from origin
* @param {Number} [dist] Distance
* @return {Vector3D} Point coordinates
*/
Ray.prototype.apply = function (dist) {
    return this.origin_.add(this.dir_.multiply(dist));
}