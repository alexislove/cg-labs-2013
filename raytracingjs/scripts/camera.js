﻿/**
* @author Alexey Vinogradov
* @copyright Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
*/

/**
 * @name Camera
 * This class represents camera, observing scene
 */

/**
 * @name CameraProperties
 * @property {Vector3D} [eye] Point, from which we're looking
 * @property {Vector3D} [dir] Direction, where we're looking
 * @property {Vector3D} [right] Right vector, that describes camera orientation
 * @property {Vector3D} [up]    Up vector, that describes camera orientation
 * @property {Number}   [distance]   Projection distance
 * @property {Number}   [viewWidth]  Viewport width
 * @property {Number}   [viewHeight] Viewport height
 * @property {Number}   [resX]       Scene horizontal resolution
 * @property {Number}   [resY]       Scene vertical resolution
 */

/**
* Camera constructor
* @param {CameraProperties} [props] Camera properties
* @constructor
*/

function Camera(props) {
    this.props_   = props;
}

Camera.prototype.constructor = Camera;

/**
 * Set camera position
 * @param {Vector3D} [pos] Desired position
 * @param {Vector3D} [at]  Point, we're looking at
 * @param {Vector3D} [up]  Up vector, that describes camera orientation
 */

Camera.prototype.setPosition = function (pos, at, up) {
    this.props_.eye = pos;
    // Calculate direction
    this.props_.dir = (at.substract(pos));
    normalize(this.props_.dir);
    // Calculate right vector
    this.props_.right = crossProduct(this.props_.dir, up);
    normalize(this.props_.right);
    // Update up vector
    this.props_.up = crossProduct(this.props_.right, this.props_.dir); // Check for right-hand coordinates system, seems to me that it should be vice-versa
}

/**
 * Set scene resolution
 * @param {Number} [xRes] Horizontal resolution
 * @param {Number} [yRes] Vertical resolution
 */
Camera.prototype.setResolution = function (xRes, yRes) {
    this.props_.resX = xRes;
    this.props_.resY = yRes;
}

/**
 * Get camera properties
 * @return {CameraProperties} Currently set properties
 */
Camera.prototype.getProperties = function () {
    return this.props_;
}

/**
* Set camera's projection
* @param {Number} [distance]   Projection distance
* @param {Number} [viewWidth]  Viewport width
* @param {Number} [viewHeight] Viewport height
*/

Camera.prototype.setProjection = function (distance, viewWidth, viewHeight) {
    this.props_.distance   = distance;
    this.props_.viewWidth  = viewWidth;
    this.props_.viewHeight = viewHeight;
}

/**
 * Get ray from given camera at given screen space coordinates
 * @param {Camera} [camera] Camera instance
 * @param {Number} [screenX] x screen coordinate
 * @param {Number} [screenY] y screen coordinate
 * @return {Ray} Constructed ray
 */
function getCameraRay(camera, screenX, screenY) {
    var cameraProps = camera.getProperties();

    var atHorizontal = cameraProps.right.multiply((screenX - cameraProps.resX / 2) / cameraProps.resX * cameraProps.viewWidth);
    var atVertical   = cameraProps.up.multiply((cameraProps.resY / 2 - screenY) / cameraProps.resY * cameraProps.viewHeight);
    var atForward    = cameraProps.dir * cameraProps.distance;

    var result = atHorizontal.add(atVertical).add(atForward);

    return new Ray(cameraProps.eye.add(result), result);
}
