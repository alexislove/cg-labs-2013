﻿/**
* @author Alexey Vinogradov
* @copyright Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
*/

/**
 * @name Auxiliary
 * @class This class provides auxiliary application methods
 */

function Auxiliary() {

}

/**
 * Swap values
 * @param {Object} [lh] First value
 * @param {Object} [rh] Second value
 */
Auxiliary.prototype.swap = function (lh, rh) {
    var tmp = lh;
    lh = rh;
    rh = tmp;
}
