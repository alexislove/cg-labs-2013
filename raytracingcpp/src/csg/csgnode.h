//-------------------------------------------------------------------
// File: csgnode.h
// 
// Constructive solid geometry evaluation tree node abstraction
// CSG tree is a binary tree, so each node has left and right child
// Node extends IShape interface with tree-related methods
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef CSG_CSGNODE_H
	#define CSG_CSGNODE_H

	#include "geometry/intersection.h"
	#include "interfaces/ishape.h"
	
	class Ray;

	struct CSGNode : IShape
	{
		//! Remove all children
		virtual void removeChildren() = 0;

		//! Get left child node of the current one
		virtual CSGNode* left() const = 0;

		//! Get right child node of the current one
		virtual CSGNode* right() const = 0;

		//! Check, whether this node has left child 
		//! That's important, use thie method instead of comparision left() == NULL
		virtual bool hasLeft() const = 0;

		//! Check, whether this node has right child 
		//! That's important, use thie method instead of comparision right() == NULL
		virtual bool hasRight() const = 0;

	};
	

#endif // CSG_CSGNODE_H