//-------------------------------------------------------------------
// File: csgvalue.h
// 
// Constructive solid geometry evaluation tree value, it's actually the tree leaf
// Value contains scene object to search intersection. 
// This is the final class, implementing IShape interface methods
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef CSG_CSGVALUE_H
	#define CSG_CSGVALUE_H

	#include "csgoperand.h"

	class CSGValue : public CSGOperand
	{
	public:
		//! Construct operand, containing 2 nodes
		explicit CSGValue(IShape* shape);

		virtual ~CSGValue();

		virtual Intersection intersect(const Ray& ray);

		// Left, right states will automatically return NULL/false, because constructor will set 
		// children to NULL

		// Implement IShape methods
		//! Get material of the shape
		virtual const Material* getMaterial() const;

		//! Get normal of the shape at given distance and ray with additional data about intersection
		virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const;

		//! Set that object is light source
		virtual void setIsLight(bool light);

		//! Get object's light state
		virtual bool isLight() const;

		//! Get color at given point, returns either the texel of an ambient texture or ambient color of material in given point
		virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		//! Get color at given point, returns either the texel of an diffuse texture or diffuse color of material in given point
		virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		//! Get color at given point, returns either the texel of an specular texture or specular color of material in given point
		virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		//! Get texture coordinates at given point, 2 first components of vector will be used, and one will be ommited
		virtual Vector3D getTexCoords(const Vector3D& point, const Intersection& isect = Intersection()) const;

	private:
		//! Node value = geometry shape
		IShape* mShape;
	};

#endif // CSG_VALUE_H