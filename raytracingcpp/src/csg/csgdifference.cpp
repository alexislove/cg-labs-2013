//-------------------------------------------------------------------
// File: csgdifference.cpp
// 
// Constructive solid geometry evaluation tree difference operation
// Left and right nodes can be either values or operations.
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include <algorithm>
#include <xutility>

#include "csgdifference.h"

#define TOO_FAR_AWAY		 1000000.f 

CSGDifference::CSGDifference(CSGNode *lh, CSGNode *rh)
	: CSGOperation(lh, rh)
{
}

CSGDifference::~CSGDifference()
{
}

Intersection CSGDifference::intersect(const Ray& ray) 
{
	// Difference operation is LeftHand \ RightHand, obviously non-commutative

	// Post-order depth-first tree traversal
	Intersection leftIsect  = left()->intersect(ray);
	Intersection rightIsect = right()->intersect(ray);

	// Due to intersection, if one doesn't exist, no intersection at all
	if (!leftIsect.Exists)
	{
		return Intersection(false);
	}

	// Right not intersected, so just left intersection can be returned
	if (!rightIsect.Exists)
	{
		return leftIsect;
	}

	// Get min and max intersections
	float leftMin  = TOO_FAR_AWAY, leftMax  = -1.f;
	float rightMin = TOO_FAR_AWAY, rightMax = -1.f;

	// Sort intersections
	std::sort(leftIsect.Distances.begin(), leftIsect.Distances.end());
	std::sort(rightIsect.Distances.begin(), rightIsect.Distances.end());

	leftMin  = leftIsect.Distances.front();  leftMax  = leftIsect.Distances.back();
	rightMin = rightIsect.Distances.front(); rightMax = rightIsect.Distances.back();

	Intersection intersect(true);

	// Right object is closer, that left, so they even don't overlap
	if (rightMax < leftMin)
	{
		return leftIsect;
	}

	// Left object is closer, than right, so right object
	if (leftMax < rightMin)
	{
		return leftIsect;
	}

	
	if (leftMin < rightMin)
	{
		intersect.Distance  = leftMin;
		intersect.Object	  = leftIsect.Object;
		intersect.Normal    = leftIsect.Normal; 
		intersect.U					= leftIsect.U;
		intersect.V					= leftIsect.V;
		intersect.TexCoords = leftIsect.TexCoords;
	}
	else if (rightMax < leftMax)
	{
		intersect.Distance  = rightMax;
		intersect.Object	  = rightIsect.Object;
		intersect.Normal    = -rightIsect.Normal; // Use normal of negative object
		intersect.U					= leftIsect.U;
		intersect.V					= leftIsect.V;
		intersect.TexCoords = leftIsect.TexCoords;
	}
	else 
	{
		return Intersection(false);
	}

	// Copy all the distances
	intersect.Distances.resize(leftIsect.Distances.size() + rightIsect.Distances.size());
	std::copy(leftIsect.Distances.begin(), leftIsect.Distances.end(), intersect.Distances.begin());
	std::copy(rightIsect.Distances.begin(), rightIsect.Distances.end(), intersect.Distances.begin() + leftIsect.Distances.size());

	return intersect;
}

const Material* CSGDifference::getMaterial() const
{
	return NULL;
}

Vector3D CSGDifference::getNormal(const Ray& ray, float distance, const Intersection& isect/* = Intersection() */) const
{
	return isect.Normal;
}

void CSGDifference::setIsLight(bool)
{
	/** Can't be light **/
}

bool CSGDifference::isLight() const
{
	/** Can't be light **/
	return false;
}

Color CSGDifference::getAmbientColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// Fallback to data, cached in intersection
	return isect.Object->getAmbientColor(point, isect);
}


Color CSGDifference::getDiffuseColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// Fallback to data, cached in intersection
	return isect.Object->getDiffuseColor(point, isect);
}


Color CSGDifference::getSpecularColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// Fallback to data, cached in intersection
	return isect.Object->getSpecularColor(point, isect);
}

Vector3D CSGDifference::getTexCoords(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// Fallback to data, cached in intersection
	return isect.Object->getTexCoords(point);
}
