//-------------------------------------------------------------------
// File: csgoperand.cpp
// 
// Constructive solid geometry evaluation tree operand
// Operand can either be an operation or the object container, if it's a leaf node
// So this is the super class for value and operation. 
// Also it stays abstract, because all methods, inherited from IShape are still not implemented.
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include "csgoperand.h"

CSGOperand::CSGOperand(CSGNode* lh, CSGNode* rh)
	: mLeftHand(lh),
		mRightHand(rh)
{
}

CSGOperand::~CSGOperand()
{
	removeChildren();
}

void CSGOperand::removeChildren()
{

}