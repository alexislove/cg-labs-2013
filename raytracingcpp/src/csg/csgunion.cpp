//-------------------------------------------------------------------
// File: csgunion.cpp
// 
// Constructive solid geometry evaluation tree union operation
// Left and right nodes can be either values or operations.
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include <algorithm>
#include <xutility>

#include "csgunion.h"

#define TOO_FAR_AWAY		 1000000.f 

CSGUnion::CSGUnion(CSGNode *lh, CSGNode *rh)
	: CSGOperation(lh, rh)
{
}

CSGUnion::~CSGUnion()
{
}

Intersection CSGUnion::intersect(const Ray& ray) 
{
	// Post-order depth-first tree traversal
	Intersection leftIsect  = left()->intersect(ray);
	Intersection rightIsect = right()->intersect(ray);

	// Update distance of not existing intersections to optimize algorithm
	if (!leftIsect.Exists)
	{
		leftIsect.Distance = TOO_FAR_AWAY;
	}
	if (!rightIsect.Exists)
	{
		rightIsect.Distance = TOO_FAR_AWAY;
	}

	Intersection unite;

	unite.Exists   = leftIsect.Exists || rightIsect.Exists;
	if (!unite.Exists)
	{
		return Intersection(false);
	}

	// We won't update intersected object, to be able to determine which object was really intersected
	if (leftIsect.Distance < rightIsect.Distance)
	{
		unite.Distance  = leftIsect.Distance;
		unite.Normal	  = leftIsect.Normal;
		unite.Object	  = leftIsect.Object;
		unite.TexCoords = leftIsect.TexCoords;
		unite.U					= leftIsect.U;
		unite.V					= leftIsect.V;
		// Do not copy distances - later
	}
	else
	{
		unite.Distance  = rightIsect.Distance;
		unite.Normal	  = rightIsect.Normal;
		unite.Object	  = rightIsect.Object;
		unite.TexCoords = rightIsect.TexCoords;
		unite.U					= rightIsect.U;
		unite.V					= rightIsect.V;
	}
	
	// Copy all distances
	//unite.Distances.resize(leftIsect.Distances.size() + rightIsect.Distances.size()); n
	if (leftIsect.Exists)
	{
		unite.Distances.resize(leftIsect.Distances.size());
		std::copy(leftIsect.Distances.begin(), leftIsect.Distances.end(), unite.Distances.begin());
	}
	if (rightIsect.Exists)
	{
		const int size = unite.Distances.size();
		unite.Distances.resize(unite.Distances.size() + rightIsect.Distances.size());
		std::copy(rightIsect.Distances.begin(), rightIsect.Distances.end(), unite.Distances.begin() + size);
	}

	return unite;
}

const Material* CSGUnion::getMaterial() const
{
	return NULL;
}

Vector3D CSGUnion::getNormal(const Ray& ray, float distance, const Intersection& isect/* = Intersection() */) const
{
	return isect.Normal;
}

void CSGUnion::setIsLight(bool)
{
	/** Can't be light **/
}

bool CSGUnion::isLight() const
{
	/** Can't be light **/
	return false;
}

Color CSGUnion::getAmbientColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// Fallback to data, cached in intersection
	return isect.Object->getAmbientColor(point, isect);
}


Color CSGUnion::getDiffuseColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// Fallback to data, cached in intersection
	return isect.Object->getDiffuseColor(point, isect);
}


Color CSGUnion::getSpecularColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// Fallback to data, cached in intersection
	return isect.Object->getSpecularColor(point, isect);
}

Vector3D CSGUnion::getTexCoords(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// Fallback to data, cached in intersection
	return isect.Object->getTexCoords(point);
}
