//-------------------------------------------------------------------
// File: csgintersection.h
// 
// Constructive solid geometry evaluation tree intersection operation
// Left and right nodes can be either values or operations.
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef CSG_CSGINTERSECTION_H
	#define CSG_CSGINTERSECTION_H

	#include "csgoperation.h"

	class CSGIntersection : public CSGOperation
	{
	public:
		//! Construct operand, containing 2 nodes
		explicit CSGIntersection(CSGNode* lh, CSGNode* rh);

		virtual ~CSGIntersection();

		virtual Intersection intersect(const Ray& ray);

		// Implement IShape methods
		//! Get material of the shape
		virtual const Material* getMaterial() const;

		//! Get normal of the shape at given distance and ray with additional data about intersection
		virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const;

		//! Set that object is light source
		virtual void setIsLight(bool light);

		//! Get object's light state
		virtual bool isLight() const;

		//! Get color at given point, returns either the texel of an ambient texture or ambient color of material in given point
		virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		//! Get color at given point, returns either the texel of an diffuse texture or diffuse color of material in given point
		virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		//! Get color at given point, returns either the texel of an specular texture or specular color of material in given point
		virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		//! Get texture coordinates at given point, 2 first components of vector will be used, and one will be ommited
		virtual Vector3D getTexCoords(const Vector3D& point, const Intersection& isect = Intersection()) const;
	};

#endif // CSG_INTERSECTION_H