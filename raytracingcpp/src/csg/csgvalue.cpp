//-------------------------------------------------------------------
// File: csgvalue.cpp
// 
// Constructive solid geometry evaluation tree value, it's actually the tree leaf
// Value contains scene object to search intersection. 
// This is the final class, implementing IShape interface methods
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include "csgvalue.h"

CSGValue::CSGValue(IShape* shape)
	: CSGOperand(NULL, NULL),
		mShape(shape)
{
}

CSGValue::~CSGValue()
{
	delete mShape;
	mShape = NULL;
}

Intersection CSGValue::intersect(const Ray& ray)
{
	return mShape->intersect(ray);
}

const Material* CSGValue::getMaterial() const
{
	return mShape->getMaterial();
}

Vector3D CSGValue::getNormal(const Ray& ray, float distance, const Intersection& isect/* = Intersection() */) const
{
	return mShape->getNormal(ray, distance, isect);
}

void CSGValue::setIsLight(bool light)
{
	mShape->setIsLight(light);
}

bool CSGValue::isLight() const
{
	return mShape->isLight();
}

Color CSGValue::getAmbientColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	return mShape->getAmbientColor(point, isect);
}


Color CSGValue::getDiffuseColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	return mShape->getDiffuseColor(point, isect);
}


Color CSGValue::getSpecularColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	return mShape->getSpecularColor(point, isect);
}

Vector3D CSGValue::getTexCoords(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	return mShape->getTexCoords(point, isect);
}