//-------------------------------------------------------------------
// File: csgintersection.cpp
// 
// Constructive solid geometry evaluation tree intersection operation
// Left and right nodes can be either values or operations.
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include <algorithm>
#include <xutility>

#include "csgintersection.h"

#define TOO_FAR_AWAY		 1000000.f 

CSGIntersection::CSGIntersection(CSGNode *lh, CSGNode *rh)
	: CSGOperation(lh, rh)
{
}

CSGIntersection::~CSGIntersection()
{
}

Intersection CSGIntersection::intersect(const Ray& ray) 
{
	// Post-order depth-first tree traversal
	Intersection leftIsect  = left()->intersect(ray);
	Intersection rightIsect = right()->intersect(ray);

	// Due to intersection, if one doesn't exist, no intersection at all
	if (!leftIsect.Exists || !rightIsect.Exists)
	{
		return Intersection(false);
	}

	// Get min and max intersections
	float leftMin  = TOO_FAR_AWAY, leftMax  = -1.f;
	float rightMin = TOO_FAR_AWAY, rightMax = -1.f;

	// Sort intersections
	std::sort(leftIsect.Distances.begin(), leftIsect.Distances.end());
	std::sort(rightIsect.Distances.begin(), rightIsect.Distances.end());

	leftMin  = leftIsect.Distances.front();  leftMax  = leftIsect.Distances.back();
	rightMin = rightIsect.Distances.front(); rightMax = rightIsect.Distances.back();

	Intersection intersect(true);

	if (leftMin < rightMin && leftMax > rightMin)
	{
		intersect.Distance  = rightMin;
		intersect.Object	  = rightIsect.Object;
		intersect.Normal    = rightIsect.Normal;
		intersect.U					= rightIsect.U;
		intersect.V					= rightIsect.V;
		intersect.TexCoords = rightIsect.TexCoords;
		intersect.Distances.push_back(rightMin);
		intersect.Distances.push_back(std::min(leftMax, rightMax));
	}
	else if (rightMin < leftMin && rightMax > leftMin)
	{
		intersect.Distance  = leftMin;
		intersect.Object	  = leftIsect.Object;
		intersect.Normal    = leftIsect.Normal;
		intersect.U					= leftIsect.U;
		intersect.V					= leftIsect.V;
		intersect.TexCoords = leftIsect.TexCoords;
		intersect.Distances.push_back(leftMin);
		intersect.Distances.push_back(std::min(leftMax, rightMax));
	}
	else
	{
		return Intersection(false);
	}
	
	
	// Copy all the distances
	/*intersect.Distances.resize(leftIsect.Distances.size() + rightIsect.Distances.size());
	std::copy(leftIsect.Distances.begin(), leftIsect.Distances.end(), intersect.Distances.begin());
	std::copy(rightIsect.Distances.begin(), rightIsect.Distances.end(), intersect.Distances.begin() + leftIsect.Distances.size());*/

	return intersect;
}

const Material* CSGIntersection::getMaterial() const
{
	return NULL;
}

Vector3D CSGIntersection::getNormal(const Ray& ray, float distance, const Intersection& isect/* = Intersection() */) const
{
	return isect.Normal;
}

void CSGIntersection::setIsLight(bool)
{
	/** Can't be light **/
}

bool CSGIntersection::isLight() const
{
	/** Can't be light **/
	return false;
}

Color CSGIntersection::getAmbientColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// Fallback to data, cached in intersection
	return isect.Object->getAmbientColor(point, isect);
}


Color CSGIntersection::getDiffuseColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// Fallback to data, cached in intersection
	return isect.Object->getDiffuseColor(point, isect);
}


Color CSGIntersection::getSpecularColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// Fallback to data, cached in intersection
	return isect.Object->getSpecularColor(point, isect);
}

Vector3D CSGIntersection::getTexCoords(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// Fallback to data, cached in intersection
	return isect.Object->getTexCoords(point);
}
