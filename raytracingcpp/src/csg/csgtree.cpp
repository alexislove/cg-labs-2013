//-------------------------------------------------------------------
// File: csgtree.cpp
// 
// Constructive solid geometry evaluation tree 
// CSG tree is a binary tree starting in given root, typically an operation
// Tree extends IShape interface with tree-related methods
// There's no need to write some generic algorithms on the tree, because it will
// be constructed from XML, so tree supports only operation of setting root node.
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include "csgtree.h"

CSGTree::CSGTree(CSGNode* root)
	: mRoot(root)
{

}

CSGTree::~CSGTree()
{
	if (mRoot)
	{
		mRoot->removeChildren();
		delete mRoot;
	}
}

Intersection CSGTree::intersect(const Ray& ray) 
{
	// Simply traverse tree, starting from the root
	return mRoot->intersect(ray);
}

const Material* CSGTree::getMaterial() const
{
	return NULL;
}

Vector3D CSGTree::getNormal(const Ray& ray, float distance, const Intersection& isect/* = Intersection() */) const
{
	// Falback to the intersection cached data
	return isect.Normal;
}

void CSGTree::setIsLight(bool)
{
	/** Can't be light **/
}

bool CSGTree::isLight() const
{
	/** Can't be light **/
	return false;
}

Color CSGTree::getAmbientColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// Fallback to data, cached in intersection
	return isect.Object->getAmbientColor(point, isect);
}


Color CSGTree::getDiffuseColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// Fallback to data, cached in intersection
	return isect.Object->getDiffuseColor(point, isect);
}


Color CSGTree::getSpecularColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// Fallback to data, cached in intersection
	return isect.Object->getSpecularColor(point, isect);
}

Vector3D CSGTree::getTexCoords(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// Fallback to data, cached in intersection
	return isect.Object->getTexCoords(point);
}
