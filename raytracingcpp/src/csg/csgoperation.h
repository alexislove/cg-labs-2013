//-------------------------------------------------------------------
// File: csgoperation.h
// 
// Constructive solid geometry evaluation tree operation
// Left and right nodes can be either values or operations.
// Also, this class stays abstract, and left for subclassing with concrete operations
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef CSG_CSGOPERATION_H
	#define CSG_CSGOPERATION_H

	#include "csgoperand.h"

	class CSGOperation : public CSGOperand
	{
	public:
		//! Construct operand, containing 2 nodes
		explicit CSGOperation(CSGNode* lh, CSGNode* rh)
			: CSGOperand(lh, rh)
		{
		}

	};

#endif // CSG_OPERATION_H