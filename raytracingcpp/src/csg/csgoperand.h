//-------------------------------------------------------------------
// File: csgoperand.h
// 
// Constructive solid geometry evaluation tree operand
// Operand can either be an operation or the object container, if it's a leaf node
// So this is the super class for value and operation. 
// Also it stays abstract, because all methods, inherited from IShape are still not implemented.
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef CSG_CSGOPERAND_H
	#define CSG_CSGOPERAND_H

	#include "csgnode.h"

	class CSGOperand : public CSGNode
	{
	public:
		//! Construct operand, containing 2 nodes
		explicit CSGOperand(CSGNode* lh, CSGNode* rh);

		~CSGOperand();

		virtual void removeChildren();

		virtual CSGNode* left() const
		{
			return mLeftHand;
		}

		virtual CSGNode* right() const
		{
			return mRightHand;
		}

		virtual bool hasLeft() const
		{
			return mLeftHand != NULL;
		}

		virtual bool hasRight() const
		{
			return mRightHand != NULL;
		}

	private:
		//! Operands instances
		CSGNode *mLeftHand;
		CSGNode *mRightHand;
	};

#endif // CSG_OPERAND_H