//-------------------------------------------------------------------
// File: csgtree.h
// 
// Constructive solid geometry evaluation tree 
// CSG tree is a binary tree starting in given root, typically an operation
// Tree extends IShape interface with tree-related methods
// There's no need to write some generic algorithms on the tree, because it will
// be constructed from XML, so tree supports only operation of setting root node.
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------
 
#ifndef CSG_CSGTREE_H
	#define CSG_CSGTREE_H

	#include "interfaces/ishape.h"
	#include "csgnode.h"

	class CSGTree : public IShape
	{
	public:
		// Construct tree with given root
		explicit CSGTree(CSGNode* root);
		
		virtual ~CSGTree();

		// Implement IShape methods
		virtual Intersection intersect(const Ray& ray);

		// Implement IShape methods
		//! Get material of the shape
		virtual const Material* getMaterial() const;

		//! Get normal of the shape at given distance and ray with additional data about intersection
		virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const;

		//! Set that object is light source
		virtual void setIsLight(bool light);

		//! Get object's light state
		virtual bool isLight() const;

		//! Get color at given point, returns either the texel of an ambient texture or ambient color of material in given point
		virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		//! Get color at given point, returns either the texel of an diffuse texture or diffuse color of material in given point
		virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		//! Get color at given point, returns either the texel of an specular texture or specular color of material in given point
		virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		//! Get texture coordinates at given point, 2 first components of vector will be used, and one will be ommited
		virtual Vector3D getTexCoords(const Vector3D& point, const Intersection& isect = Intersection()) const;

	private:
		CSGNode* mRoot;
	};

#endif // CSG_CSGTREE_H