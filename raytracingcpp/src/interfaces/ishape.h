//-------------------------------------------------------------------
// File: plane.h
// 
// Scene object geometry shape interface
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------
#ifndef INTERFACES_ISHAPE_H
	#define INTERFACES_ISHAPE_H

	#include "geometry/ray.h"
	#include "geometry/intersection.h"
	#include "illumination/types.h"

	class Material;


	struct IShape
	{
		virtual ~IShape() = 0
		{
		}

		//! Find intersection with the ray and return data
		virtual Intersection intersect(const Ray& ray) = 0;

		//! Get material of the shape
		virtual const Material* getMaterial() const = 0;

		//! Get normal of the shape at given distance and ray with additional data about intersection
		virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const = 0;

		//! Set that object is light source
		virtual void setIsLight(bool light) = 0;

		//! Get object's light state
		virtual bool isLight() const = 0;

		//! Get color at given point, returns either the texel of an ambient texture or ambient color of material in given point
		virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const = 0;

		//! Get color at given point, returns either the texel of an diffuse texture or diffuse color of material in given point
		virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const = 0;

		//! Get color at given point, returns either the texel of an specular texture or specular color of material in given point
		virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const = 0;

		//! Get texture coordinates at given point, 2 first components of vector will be used, and one will be ommited
		virtual Vector3D getTexCoords(const Vector3D& point, const Intersection& isect = Intersection()) const = 0;

		// TODO: Add BBox etc.
	};


#endif // INTERFACES_ISHAPE_H