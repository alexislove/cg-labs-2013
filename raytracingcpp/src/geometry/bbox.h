//-------------------------------------------------------------------
// File: bbox.h
// 
// Scene object's axis-aligned bounding box implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef GEOMETRY_BBOX_H
#define GEOMETRY_BBOX_H

#include "geometry/vector3D.h"

class Ray;

struct BBox
{
	//! Check, whether ray intersects bounding box or not
	bool intersect(const Ray& ray) const;

	// Bounding boxes definition points
	Vector3D Max;
	Vector3D Min;
};

#endif // GEOMETRY_BBOX_H