//-------------------------------------------------------------------
// File: modeltriangle.h
// 
// This class extends trianle, providing capabilites to use per-vertex texture coordinates and normals for
// proper model rendering
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef GEOMETRY_MODELTRIANGLE_H
	#define GEOMETRY_MODELTRIANGLE_H

	#include "smoothtriangle.h"
	#include "texturedtriangle.h"
	
	class ModelTriangle : virtual public SmoothTriangle,   // Classical rombus, but no problems :)
												virtual public TexturedTriangle
	{
	public:
		//! Constructor from 3 points
		explicit ModelTriangle(const Vector3D& v0,  const Vector3D& v1,  const Vector3D& v2, 
													 const Vector3D& n0,  const Vector3D& n1,  const Vector3D& n2,
													 const Vector3D& uv0, const Vector3D& uv1, const Vector3D& uv2,
													 Material* material)
			: SmoothTriangle(v0, v1, v2, n0, n1, n2, material),
				TexturedTriangle(v0, v1, v2, uv0, uv1, uv2, material)
		{
			
		}

		//! Solve ambiguous calls problem
		virtual Intersection intersect(const Ray& ray)
		{
			return Triangle::intersect(ray);
		}
	};

#endif // GEOMETRY_MODELTRIANGLE_H
