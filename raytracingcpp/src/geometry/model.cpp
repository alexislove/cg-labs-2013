//-------------------------------------------------------------------
// File: model.cpp
// 
// Model scene object implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include "illumination/material.h"
#include "triangle.h"

#include "model.h"

#define TOO_FAR_AWAY		 1000000.f

Model::Model(const std::vector< ModelTriangle* > &triangles, const BBox &bbox, Material* material)
	: mTriangles(triangles),
		mBoundingBox(bbox),
		mMaterial(material),
		mIsLight(false)
{
	
}

Model::~Model()
{
	for (std::vector< ModelTriangle* >::iterator tri = mTriangles.begin(); tri != mTriangles.end(); ++tri)
	{
		delete *tri;
		*tri = NULL;
	}
	delete mMaterial;
}

Intersection Model::intersect(const Ray& ray)
{
	if (!mBoundingBox.intersect(ray))
	{
		return Intersection(false);
	}

	float				 closestDistance = TOO_FAR_AWAY;
	Intersection closestIntersection(false);
	for (std::vector< ModelTriangle* >::iterator tri = mTriangles.begin(); tri != mTriangles.end(); ++tri)
	{
		Intersection current = (*tri)->intersect(ray);
		if (current.Exists)
		{
			closestIntersection.Distances.push_back(current.Distance);
			if (current.Distance < closestDistance)
			{
				closestIntersection.Exists	 = true;
				closestIntersection.Distance = current.Distance;
				closestIntersection.Object	 = this;
				closestIntersection.Normal	 = current.Normal;

				closestDistance = current.Distance;
			}
		}
	}

	return closestIntersection;
}


Vector3D Model::getNormal(const Ray& ray, float distance, const Intersection& isect /*= Intersection()*/) const
{
	return isect.Normal; // Return already calculated normal
}

Color Model::getAmbientColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	return mMaterial->AmbientColor;
}

Color Model::getDiffuseColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	if (mMaterial->DiffuseTexture)
	{
		Vector3D texCoords = getTexCoords(point, isect);
		return scale3D(mMaterial->DiffuseTexture->sample(texCoords.x(), texCoords.y()), mMaterial->DiffuseColor);
	}
	return mMaterial->DiffuseColor;
}

Color Model::getSpecularColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	return mMaterial->SpecularColor;
}

Vector3D Model::getTexCoords(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	return isect.TexCoords;
}