//-------------------------------------------------------------------
// File: cone.cpp
// 
// Cone scene object implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include "illumination/material.h"

#include "cone.h"

Cone::Cone(const Vector3D& top, const Vector3D& bottom, float radius, Material* material)
	: mTop(top),
		mBottom(bottom),
		mRadius(radius),
		mMaterial(material),
		mIsLight(false)
{
	mAxis					= (mTop - mBottom).toUnit();
	mRadius2			= mRadius * mRadius;
	mRadPerHeight = mRadius / length(mTop - mBottom);
}

Cone::~Cone()
{
	delete mMaterial;
}

Intersection Cone::intersect(const Ray& ray)
{
	const Vector3D& rayOrigin    = ray.getOrigin();
	const Vector3D& rayDirection = ray.getDirection();

	const Vector3D& CO = rayOrigin - mBottom;

	const float dirDotAxis = dot(rayDirection, mAxis);
	const float CODotAxis	 = dot(CO, mAxis);

	const Vector3D& u = rayDirection + mAxis * (-dirDotAxis);
	const Vector3D& v = CO + mAxis * (-CODotAxis);
	const float     w = CODotAxis * mRadPerHeight;

	const float	radPerDir = dirDotAxis * mRadPerHeight;

	Intersection res = Intersection(true);

	// Let a, b, c be the coefficients of the square equation
	const float a = dot(u, u) - radPerDir * radPerDir;
	float closest = -1.f;
	float root		= 0.f;
	float rayExit = -1.f; // Distance, where ray has exited the cone
	if (fabs(a) > FLOAT_ZERO)
	{
		const float b = 2 * (dot(u, v) - w * radPerDir);
		const float c = dot(v, v) - w * w;

		float D = b * b - 4 * a * c;

		// Complete miss
		if (D < 0.f) 
		{
			return Intersection(false);
		}

		D = sqrtf(D);

		const float denom = 1 / (2 * a);

		root = (-b - D) * denom;
		if (root > 0.f)
		{
			const Vector3D& surfacePoint = ray.apply(root);
			Vector3D toBottom = surfacePoint - mBottom;
			Vector3D toTop		= surfacePoint - mTop;
			if (dot(mAxis, toBottom) > 0.f && dot((-mAxis), toTop) > 0.f)
			{
				res.Distances.push_back(root);
				closest = root;
			}
		}
		root = (-b + D) * denom;
		if (root > 0.f)
		{
			const Vector3D& surfacePoint = ray.apply(root);
			Vector3D toBottom = surfacePoint - mBottom;
			Vector3D toTop		= surfacePoint - mTop;
			if (dot(mAxis, toBottom) > 0.f && dot(-mAxis, toTop) > 0.f)
			{
				res.Distances.push_back(root);
				if (closest < 0.f)
				{
					closest = root;
				}
				else if (root < closest)
				{
					rayExit = closest;
					closest = root;
				}
				else
				{
					rayExit = root;
				}
			}
		}
	}

	// Intersection with bottom cap

	if (fabs(dirDotAxis) < FLOAT_ZERO)
	{
		if (closest > 0.f)
		{
			res.Exists	 = true;
			res.Distance = closest;
			res.Object   = this;
			res.Normal	 = getNormal(ray, closest);
			if (rayExit < 0.f) 
			{
				res.InsideIntervals.push_back(Span(0.f, closest));
				res.Distances.insert(res.Distances.begin(), 0.f);
			}
			else
			{
				res.InsideIntervals.push_back(Span(closest, rayExit));
			}
			
			return res;
		}

		return Intersection(false);
	}

	// Somewhere I messed up with the top and bottom points
	root = (dot(-mAxis, rayOrigin - mTop)) / dirDotAxis;
	if (root > 0.f)
	{
		Vector3D test = ray.apply(root) - mTop;
		if (dot(test, test) < mRadius2)
		{
			res.Distances.push_back(root);
			if (closest < 0.f)
			{
				closest = root;
				rayExit = root;
			}
			else if (root < closest)
			{
				rayExit = closest;
				closest = root;
			}
			else
			{
				rayExit = root;
			}
		}
	}
	
	if (closest > 0.f)
	{
			res.Exists	 = true;
			res.Distance = closest;
			res.Object   = this;
			res.Normal	 = getNormal(ray, closest);
			if (rayExit < 0.f) 
			{
				res.InsideIntervals.push_back(Span(0.f, closest));
				res.Distances.insert(res.Distances.begin(), 0.f);
			}
			else
			{
				res.InsideIntervals.push_back(Span(closest, rayExit));
			}
			return res;
	}
	return Intersection(false);
}


Vector3D Cone::getNormal(const Ray& ray, float distance, const Intersection& isect /*= Intersection()*/) const
{
	const Vector3D atSurface = ray.apply(distance);
	// Check, whether point is lying on caps
	// Bottom
	const Vector3D& toBottom		 = atSurface - mTop;
	if (fabs(dot(mAxis, toBottom)) < FLOAT_ZERO && dot(toBottom, toBottom) < mRadius2)
	{
		return mAxis;
	}
	
	const Vector3D& approxNorm = atSurface - (mAxis * (dot(atSurface - mTop, mAxis)) + mTop); // Check me
	return (approxNorm + mAxis * (-mRadPerHeight * length(approxNorm))).toUnit();
}

Color Cone::getAmbientColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	return mMaterial->AmbientColor;
}

Color Cone::getDiffuseColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	if (mMaterial->DiffuseTexture)
	{
		Vector3D texCoords = getTexCoords(point, isect);
		return mMaterial->DiffuseTexture->sample(texCoords.x(), texCoords.y());
	}
	return mMaterial->DiffuseColor;
}

Color Cone::getSpecularColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	return mMaterial->SpecularColor;
}

Vector3D Cone::getTexCoords(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	// TODO:
	return Vector3D();
}