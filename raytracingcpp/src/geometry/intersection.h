//-------------------------------------------------------------------
// File: intersection.h
// 
// Intersection data enclosure type
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------


#ifndef GEOMETRY_INTERSECTION_H
	#define GEOMETRY_INTERSECTION_H

	#include <vector>
	
	#include "span.h"
	#include "vector3d.h"

	struct IShape;

	struct Intersection
	{
		explicit Intersection(bool hit = false, float dist = -1.f, IShape *object = 0x0, const Vector3D& normal = Vector3D())
			: Exists(hit),
				Distance(dist),
				Object(object),
				Normal(normal)
		{
		}

		//! Actual existence state
		bool		 Exists;
		//! Distance from camera on the ray
		float    Distance;
		//! Object, hit by ray
		IShape  *Object;
		//! Normal, calculated in the intersection point
		Vector3D Normal;
		
		//! Barycentric coordinates for triangle intersection
		float		 U;
		float		 V;
    //! Texture coordinates 
    Vector3D TexCoords;

		//! Store all distances, where the object was intersected by the ray (for CSG)
		//! This is bad, dynamic allocation, but in general number of intersections is not limited
		std::vector< float > Distances;

		//! Store spans, where ray was inside the object
		Spans InsideIntervals;
	};

#endif // GEOMETRY_INTERSECTION_H