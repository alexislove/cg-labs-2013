//-------------------------------------------------------------------
// File: torus.h
// 
// Torus scene object implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef GEOMETRY_TORUS_H
#define GEOMETRY_TORUS_H

#include "interfaces/ishape.h"

#include "geometry/ray.h"
#include "geometry/vector3d.h"

class Torus : public IShape
{
public:
	//! Constructor, cone owns passed material;
	explicit Torus(const Vector3D& center, const Vector3D& axis, float innerRadius, float outedRadius, Material* material);

	virtual ~Torus();

	//! Override IShape methods
	virtual Intersection intersect(const Ray& ray);

	virtual const Material* getMaterial() const
	{
		return mMaterial;
	}

	virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const;

	virtual void setIsLight(bool light)
	{
		mIsLight = light;
	}

	virtual bool isLight() const
	{
		return mIsLight;
	}

	virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Vector3D getTexCoords(const Vector3D& point, const Intersection& isect = Intersection()) const;

private:
	//! Location
	Vector3D	mCenter;
	Vector3D	mAxis;
	float     mInnerRadius;
	float			mOuterRadius;
	//! Precalculated values
	float			mInnerRadius2;
	float			mOuterRadius2;
	//! Illumination material
	Material* mMaterial;
	//! Is cylinder a light source
	bool			mIsLight;
};

#endif // GEOMETRY_SPHERE_H