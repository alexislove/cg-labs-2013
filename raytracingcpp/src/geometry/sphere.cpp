//-------------------------------------------------------------------
// File: sphere.cpp
// 
// Sphere scene object implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#define _USE_MATH_DEFINES
#include <math.h>

#include "illumination/material.h"

#include "sphere.h"

Sphere::Sphere(const Vector3D& center, float radius, Material* material)
	: mCenter(center),
		mRadius(radius),
		mMaterial(material),
		mIsLight(false)
{
	mRadius2 = radius * radius;
	mVn = Vector3D(0.f, 1.f, 0.f);
	mVe = Vector3D(1.f, 0.f, 0.f);
	mVc = cross(mVn, mVe);
}

Sphere::~Sphere()
{
	delete mMaterial;
}

Intersection Sphere::intersect(const Ray& ray)
{
	// Solve square equation
	Vector3D CO = ray.getOrigin() - mCenter;

	// Let p and q be coefficients of the square equation x^2 + p * x + q = 0
	float p = dot(ray.getDirection(), CO);
	float q = dot(CO, CO) - mRadius2;

	// Let D be discriminant of the equation
	float D = p * p - q;

	if (D < 0)
	{
		return Intersection(false);
	}

	D = sqrt(D);

	float				 closest = -1.f;
	Intersection res(true);
	
	// Get closest intersection
	float root = -p - D;
	float rayExit = -1.f;
	if (root >= 0.f)
	{
		closest = root;
		res.Distances.push_back(root);
	}
	
	root = -p + D;
	if (root >= 0.f)
	{
		res.Distances.push_back(root);
		if (closest < 0.f)
		{
			closest = root;
		}
		else if (root < closest)
		{
			rayExit = closest;
			closest = root;
		}
		else
		{
			rayExit = root;
		}
	}

	if (closest > 0.f)
	{
		res.Distance = closest;
		res.Object	 = this;
		res.Normal	 = getNormal(ray, closest);
		if (rayExit < 0.f)
		{
			res.InsideIntervals.push_back(Span(0.f, closest));
			res.Distances.insert(res.Distances.begin(), 0.f);
		}
		else
		{
			res.InsideIntervals.push_back(Span(closest, rayExit));
		}
		return res;
	}

	return Intersection(false);
}

Vector3D Sphere::getNormal(const Ray& ray, float distance, const Intersection& isect /*= Intersection()*/) const
{
	Vector3D normal = (ray.apply(distance) - mCenter) / mRadius;
	normal.normalize();
	return normal;
}


Color Sphere::getAmbientColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	return mMaterial->AmbientColor;
}

Color Sphere::getDiffuseColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	if (mMaterial->DiffuseTexture)
	{
		Vector3D texCoords = getTexCoords(point, isect);
		return scale3D(mMaterial->DiffuseTexture->sample(texCoords.x(), texCoords.y()), mMaterial->DiffuseColor);
	}
	return mMaterial->DiffuseColor;
}

Color Sphere::getSpecularColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	return mMaterial->SpecularColor;
}

Vector3D Sphere::getTexCoords(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	const Vector3D CO = (point - mCenter) / mRadius;

	const float phi   = acosf(-dot(CO, mVn));
	const float theta = (acosf(dot(mVe, CO)) / (sinf(phi))) * (2.f / M_PI);

	float u;

	if (dot(mVc, CO) >= 0.f)
	{
		u = (1.f - theta) / mMaterial->TexScaleU;
	}
	else
	{
		u = theta / mMaterial->TexScaleU;
	}

	float v = phi / mMaterial->TexScaleV * (1.f / M_PI);

	return Vector3D(u, v, 0.f);
}