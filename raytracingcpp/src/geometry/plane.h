//-------------------------------------------------------------------
// File: plane.h
// 
// Infinite plane scene object implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef GEOMETRY_PLANE_H
	#define GEOMETRY_PLANE_H

	#include "interfaces/ishape.h"

	class Plane : public IShape
	{
	public:
		//! Constructor from normal, distance and material
		explicit Plane(const Vector3D& normal, float D, Material* material);

		virtual ~Plane();

		//! Override IShape methods
		virtual Intersection intersect(const Ray& ray);

		virtual const Material* getMaterial() const
		{
			return mMaterial;
		}

		virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const
		{
			//return ray.apply(distance).toUnit();
			return mNormal;
			//return Vector3D();
		}

		virtual void setIsLight(bool light)
		{
			mIsLight = light;
		}

		virtual bool isLight() const
		{
			return mIsLight;
		}

		virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		virtual Vector3D getTexCoords(const Vector3D& point, const Intersection& isect = Intersection()) const;

	private:
		//! Plane orientation
		Vector3D	mNormal;
		float			mD; // From the equation
		//! Illumination part
		Material *mMaterial;
		//! Vectors for texture mapping
		Vector3D  mUAxis;
		Vector3D  mVAxis;
		//! Ligh state
		bool			mIsLight;
	};

#endif // GEOMETRY_PLANE_H
