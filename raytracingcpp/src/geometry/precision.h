//-------------------------------------------------------------------
// File: precision.h
// 
// Some precision aspects of mathematics
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------
#ifndef GEOMETRY_PRECISION_H
	#define GEOMETRY_PRECISION_H

	//! Float typically holds about 7 digits after dot, we'll rely on 6
	#define FLOAT_ZERO 0.000001f
	//! Small value for computations
	#define EPSILON		 0.0005f

#endif // GEOMETRY_PRECISION_H