//-------------------------------------------------------------------
// File: texturedtriangle.h
// 
// This class extends trianle, providing capabilites to use per-vertex texture coordinates
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef GEOMETRY_TEXTUREDTRIANGLE_H
	#define GEOMETRY_TEXTUREDTRIANGLE_H

	#include "triangle.h"

	class TexturedTriangle : public Triangle
	{
	public:
		//! Constructor from 3 points
		explicit TexturedTriangle(const Vector3D& v0, const Vector3D& v1, const Vector3D& v2, 
															const Vector3D& uv0, const Vector3D& uv1, const Vector3D& uv2,
															Material* material)
			: Triangle(v0, v1, v2, material),
				mUV0(uv0),
				mUV1(uv1),
				mUV2(uv2)
		{
			
		}

		virtual Vector3D getTexCoords(const Vector3D& point, const Intersection& isect) const
		{
			// Interpolate coordinates between vertices
			return isect.U * mUV1 + isect.V * mUV2 + (1 - isect.U - isect.V) * mUV0;
		}

	private:
		//! Texture coordinates for each vertex
		Vector3D mUV0;
		Vector3D mUV1;
		Vector3D mUV2;
	};

#endif // GEOMETRY_TEXTUREDTRIANGLE_H