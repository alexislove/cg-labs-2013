//-------------------------------------------------------------------
// File: smoothtriangle.h
// 
// This class extends trianle, providing capabilites to use per-vertex normals
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef GEOMETRY_SMOOTHTRIANGLE_H
	#define GEOMETRY_SMOOTHTRIANGLE_H

	#include "triangle.h"

	class SmoothTriangle : public Triangle
	{
	public:
		//! Constructor from 3 points
		explicit SmoothTriangle(const Vector3D& v0, const Vector3D& v1, const Vector3D& v2, 
														const Vector3D& n0, const Vector3D& n1, const Vector3D& n2,
														Material* material)
			: Triangle(v0, v1, v2, material),
				mNormalV0(n0),
				mNormalV1(n1),
				mNormalV2(n2)
		{
			
		}

		virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect) const
		{
			Vector3D normal = isect.U * mNormalV1 + isect.V * mNormalV2 + (1 - isect.U - isect.V) * mNormalV0;
			return normal.toUnit();
		}

	private:
		//! Normals for each vertex
		Vector3D mNormalV0;
		Vector3D mNormalV1;
		Vector3D mNormalV2;
	};

#endif // GEOMETRY_SMOOTHTRIANGLE_H