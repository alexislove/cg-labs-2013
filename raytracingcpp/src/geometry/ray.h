//-------------------------------------------------------------------
// File: ray.h
// 
// Traditional ray implementation (r = Org + t * Dir)
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------
#ifndef GEOMETRY_RAY_H
	#define GEOMETRY_RAY_H

	#include "vector3d.h"

	class Ray
	{
	public:
		//! Constructor
		Ray(const Vector3D& org = Vector3D(), const Vector3D& dir = Vector3D())
			: mOrigin(org), 
				mDirection(dir)
		{
			mDirection.normalize(); // If already normalized, this will cost nothing
		}

		//! Accessors and mutators
		const Vector3D& getOrigin() const
		{
			return mOrigin;
		}

		const Vector3D& getDirection() const
		{
			return mDirection;
		}

		//! Calculate position on the ray at given distance
		//! R = O + D * t - canonical ray formulae
		Vector3D apply(float t) const
		{
			return mOrigin + mDirection * t;
		}

	private:
		Vector3D mOrigin;
		Vector3D mDirection;
	};

#endif // GEOMETRY_RAY_H