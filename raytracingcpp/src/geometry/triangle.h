//-------------------------------------------------------------------
// File: triangle.h
// 
// Triangle scene object implementation
//			 Algorithm can be found here, so don't get confused with identifier names
//			 http://geomalgorithms.com/a06-_intersect-2.html
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef GEOMETRY_TRIANGLE_H
	#define GEOMETRY_TRIANGLE_H

	#include "interfaces/ishape.h"
	
	class Triangle : public IShape
	{
	public:
		//! Constructor from 3 points
		explicit Triangle(const Vector3D& v0, const Vector3D& v1, const Vector3D& v2, Material* material)
		: mV0(v0),
			mV1(v1),
			mV2(v2),
			mMaterial(material),
			mIsLight(false)
		{
			Vector3D e1 = v1 - v0;
			Vector3D e2 = v2 - v0;
			mNormal = cross(e1, e2).toUnit();
		}
		

		virtual ~Triangle();

		//! Override IShape methods
		virtual Intersection intersect(const Ray& ray);

		virtual const Material* getMaterial() const
		{
			return mMaterial;
		}

		virtual Vector3D getNormal(const Ray&, float, const Intersection& isect = Intersection()) const
		{
			return mNormal;
		}

		virtual void setIsLight(bool light)
		{
			mIsLight = light;
		}

		virtual bool isLight() const
		{
			return mIsLight;
		}

		virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		virtual Vector3D getTexCoords(const Vector3D& point, const Intersection& isect = Intersection()) const;

	private:
		//! Triangle vertices
		Vector3D mV0;
		Vector3D mV1;
		Vector3D mV2;
		//! Precalculated values for optimization
		Vector3D mNormal;		
		//! Illumination part
		Material *mMaterial;
		//! Is a light source
		bool			mIsLight;
	};

#endif // GEOMETRY_TRIANGLE_H