//-------------------------------------------------------------------
// File: torus.cpp
// 
// Torus scene object implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#define _USE_MATH_DEFINES
#include <assert.h>
#include <climits>
#include <math.h>

#include <algorithm>
#include <vector>

#include "illumination/material.h"
#include "vendors/quarticsolver.h"

#include "torus.h"

Torus::Torus(const Vector3D& center, const Vector3D& axis, float innerRadius, float outerRadius, Material* material)
	: mCenter(center),
		mAxis(axis),
		mInnerRadius(innerRadius),
		mOuterRadius(outerRadius),
		mMaterial(material),
		mIsLight(false)
{
	mAxis.toUnit();
	mInnerRadius2 = mInnerRadius * mInnerRadius;
	mOuterRadius2	= mOuterRadius * mOuterRadius;
}

Torus::~Torus()
{
	delete mMaterial;
}

Intersection Torus::intersect(const Ray& ray)
{
	const Vector3D& rayOrigin    = ray.getOrigin();
	const Vector3D& rayDirection = ray.getDirection();

	const Vector3D CO	= rayOrigin - mCenter;

	const float CODotDir = dot(rayDirection, CO);

	const float		 CO2 = dot(CO, CO);
	const float		 u	 = dot(mAxis, CO);
	const float		 v   = dot(mAxis, rayDirection);
	const float		 a   = (1 - v * v); // At general (dot(rayDirection, rayDirection) - v * v), but ray direction is normalized)
	const float		 b   = 2 * (dot(CO, rayDirection) - u * v);
	const float    c   = (CO2 - u * u);
	const float		 d   = (CO2 + mOuterRadius2 - mInnerRadius2);

	//// Let A, B, C, D, E be the coefficients of general quadratic
	const float A = 1; // As soon as ray direction was normalized A = (P1 * P1)^2
	const float B = 4 * CODotDir;
	const float C = 2 * d + B * B * 0.25f - 4 * mOuterRadius2 * a;
	const float D = B * d - 4 * mOuterRadius2 * b;
	const float E = d * d - 4 * mOuterRadius2 * c;

	// Now solve quadratic, extern solver requires double array as result for roots
	const int cRootsCount = 4;
	double solutions[cRootsCount] = { -1.0, -1.0, -1.0, -1.0 };

	QuarticEquation equation(A, B, C, D, E);
	int rootsCount = equation.Solve(solutions);

	if (rootsCount == 0)
	{
		return Intersection(false);
	}

	Intersection res(true);

	// Now find closest intersection
	float closest = FLT_MAX;
	for (int idx = 0; idx < cRootsCount; ++idx)
	{
		float solution = static_cast<float>(solutions[idx]);
		if (solution > 0.f)
		{
			res.Distances.push_back(solution);
			if (solution < closest)
			{
				closest = solution;
			}
		}
	}

	if (res.Distances.empty())
	{
		return Intersection(false);
	}

	std::sort(res.Distances.begin(), res.Distances.end());

	res.Distance = closest;
	res.Object	 = this;
	res.Normal	 = getNormal(ray, closest);

	return res;
}


Vector3D Torus::getNormal(const Ray& ray, float distance, const Intersection& isect /*= Intersection()*/) const
{
	const Vector3D atSurface = ray.apply(distance);
	const Vector3D toCenter  = atSurface - mCenter;
	const float		 y = dot(toCenter, mAxis);
	const Vector3D D = (toCenter - y * mAxis).toUnit();
	const Vector3D X = D * mOuterRadius;
	return (toCenter + X).toUnit();
}

Color Torus::getAmbientColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	return mMaterial->AmbientColor;
}

Color Torus::getDiffuseColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	if (mMaterial->DiffuseTexture)
	{
		Vector3D texCoords = getTexCoords(point, isect);
		return scale3D(mMaterial->DiffuseTexture->sample(texCoords.x(), texCoords.y()), mMaterial->DiffuseColor);
	}
	return mMaterial->DiffuseColor;
}

Color Torus::getSpecularColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	return mMaterial->SpecularColor;
}

Vector3D Torus::getTexCoords(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	// TODO:
	return Vector3D();
}
