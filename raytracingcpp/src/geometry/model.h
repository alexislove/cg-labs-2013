//-------------------------------------------------------------------
// File: model.h
// 
// Model scene object implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef GEOMETRY_MODEL_H
#define GEOMETRY_MODEL_H

#include <vector>

#include "interfaces/ishape.h"

#include "geometry/bbox.h"
#include "geometry/modeltriangle.h"
#include "geometry/ray.h"
#include "geometry/vector3d.h"

class Triangle;

class Model : public IShape
{
public:
	//! Constructor, model owns material
	Model(const std::vector< ModelTriangle* >& triangles, const BBox& bbox, Material* material);

	virtual ~Model();

	//! Override IShape methods
	virtual Intersection intersect(const Ray& ray);

	virtual const Material* getMaterial() const
	{
		return mMaterial;
	}

	virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const;

	virtual void setIsLight(bool light)
	{
		mIsLight = light;
	}

	virtual bool isLight() const
	{
		return mIsLight;
	}

	virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Vector3D getTexCoords(const Vector3D& point, const Intersection& isect = Intersection()) const;

private:
	//! Location
	std::vector< ModelTriangle* > mTriangles;
	BBox													mBoundingBox;
	//! Illumination material
	Material* mMaterial;
	//! Is cylinder a light source
	bool			mIsLight;
};

#endif // GEOMETRY_SPHERE_H