//-------------------------------------------------------------------
// File: cylinder.cpp
// 
// Cylinder scene object implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#define _USE_MATH_DEFINES
#include <math.h>

#include "illumination/material.h"

#include "cylinder.h"

Cylinder::Cylinder(const Vector3D& top, const Vector3D& bottom, float radius, Material* material)
  : mTop(top),
    mBottom(bottom),
    mRadius(radius),
    mMaterial(material),
		mIsLight(false)
{
  mAxis		 = (mTop - mBottom).toUnit();
	mRadius2 = mRadius * mRadius;
	mVe			 = Vector3D(mAxis.y(), mAxis.z(), -mAxis.x());
	mVn			 = cross(mVe, mAxis);
}

Cylinder::~Cylinder()
{
  delete mMaterial;
}

Intersection Cylinder::intersect(const Ray& ray)
{
  const Vector3D& origin    = ray.getOrigin();
  const Vector3D& direction = ray.getDirection();

	const Vector3D& CO = origin - mBottom;
	
	const Vector3D& u = direction - mAxis * (dot(direction, mAxis));
	const Vector3D& v = CO - mAxis * (dot(CO, mAxis));

	Intersection res = Intersection(true);
	// Let a, b and c be coefficients of some square equation
	const float a = dot(u, u);
	float root		= 0.f;
	float closest = -1.f;
	float rayExit = -1.f;
	if (fabs(a) > FLOAT_ZERO)
	{
		const float b = 2 * dot(u, v);
		const float c = dot(v, v) - mRadius2;

		float D = b * b - 4 * a * c;

		// Complete miss
		if (D < 0.f) 
		{
			return Intersection(false);
		}

		D = sqrtf(D);

		// Calculate roots and take closest
		float denom = 1 / (2 * a);

		root = (-b - D) * denom;
		if (root >= 0.f)
		{
			Vector3D toBottom = ray.apply(root) - mBottom;
			Vector3D toTop		= ray.apply(root) - mTop;
			if (dot(mAxis, toBottom) > 0.f && dot(mAxis, toTop) < 0.f)
			{
				res.Distances.push_back(root);
				closest = root;
			}
		}
		root = (-b + D) * denom;
		if (root > 0.f)
		{
			// Awful copy paste :(
			Vector3D toBottom = ray.apply(root) - mBottom;
			Vector3D toTop		= ray.apply(root) - mTop;
			if (dot(mAxis, toBottom) > 0.f && dot(mAxis, toTop) < 0.f)
			{
				res.Distances.push_back(root);
				if (closest < 0.f)
				{
					root = closest;
				}
				else if (root < closest)
				{
					rayExit = closest;
					closest = root;
				}
				else
				{
					rayExit = root;
				}
			}
		}
	}
	
	// dot(va, (q - p1)) = 0   t = (dot(va, p1) - dot(va, p)) / dot(va, v)

	// Find intersection with caps
	// Bottom one
	float axisToDirection = dot(mAxis, direction);

	if (fabs(axisToDirection) < FLOAT_ZERO)
	{
		if (closest > 0.f)
		{
			res.Distance = closest;
			res.Object   = this;
			res.Normal	 = getNormal(ray, closest);
			if (rayExit < 0.f) 
			{
				res.InsideIntervals.push_back(Span(0.f, closest));
				res.Distances.insert(res.Distances.begin(), 0.f);
			}
			else
			{
				res.InsideIntervals.push_back(Span(closest, rayExit));
			}
			return res;
		}

		return Intersection(false);
	}

	float axisToOrigin		= dot(mAxis, origin);
	//root = (dot(mAxis, mBottom) - axisToOrigin) / axisToDirection;
	float CODotAxis = dot(CO, mAxis);
	root = -CODotAxis / axisToDirection;
	if (root > 0.f)
	{
		Vector3D toBottom = ray.apply(root) - mBottom;
		if (dot(toBottom, toBottom) < mRadius2)
		{
			res.Distances.push_back(root);
			// Awful copy paste :(
			if (closest < 0.f)
			{
				closest = root;
			}
			else if (root < closest)
			{
				rayExit = closest;
				closest = root;
			}
			else
			{
				rayExit = root;
			}
		}
	}
	// Top one
	//root = (dot(mAxis, mTop) - axisToOrigin) / axisToDirection;
	float CTDotAxis = dot(origin - mTop, -mAxis);
	root = CTDotAxis / axisToDirection;
	if (root > 0.f)
	{
		// Awful copy paste :(
		Vector3D toTop = ray.apply(root) - mTop;
		if (dot(toTop, toTop) < mRadius2)
		{
			res.Distances.push_back(root);
			if (closest < 0.f)
			{
				closest = root;
			}
			else if (root < closest)
			{
				rayExit = closest;
				closest = root;
			}
			else
			{
				rayExit = root;
			}
		}
	}
	
	if (closest >= 0.f)
	{
		res.Distance = closest;
		res.Object   = this;
		res.Normal	 = getNormal(ray, closest);
		if (rayExit < 0.f) 
		{
			res.InsideIntervals.push_back(Span(0.f, closest));
			res.Distances.insert(res.Distances.begin(), 0.f);
		}
		else
		{
			res.InsideIntervals.push_back(Span(closest, rayExit));
		}
		return res;
	}
  return Intersection(false);
}

Vector3D Cylinder::getNormal(const Ray& ray, float distance, const Intersection& isect /*= Intersection()*/) const
{
	const Vector3D atSurface = ray.apply(distance);
  // Check, whether point is lying on caps
	// Bottom
	const Vector3D& toBottom = atSurface - mBottom;
	if (fabs(dot(mAxis, toBottom)) < FLOAT_ZERO && dot(toBottom, toBottom) < mRadius2)
	{
		return -mAxis;
	}
	const Vector3D& toTop = atSurface - mTop;
	if (fabs(dot(mAxis, toTop)) < FLOAT_ZERO && dot(toTop, toTop) < mRadius2)
	{
		return mAxis;
	}
    
	return (atSurface - mAxis * dot(toBottom, mAxis) - mBottom).toUnit();
}

Color Cylinder::getAmbientColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	return mMaterial->AmbientColor;
}

Color Cylinder::getDiffuseColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	if (mMaterial->DiffuseTexture)
	{
		Vector3D texCoords = getTexCoords(point, isect);
		return scale3D(mMaterial->DiffuseTexture->sample(texCoords.x(), texCoords.y()), mMaterial->DiffuseColor);
	}
	return mMaterial->DiffuseColor;
}

Color Cylinder::getSpecularColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	return mMaterial->SpecularColor;
}

Vector3D Cylinder::getTexCoords(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// Get position of point on the surface of the cylinder
	const Vector3D CO = point - mBottom;

	float CODotAxis = dot(CO, mAxis);

	Vector3D atCircle = point - CODotAxis * mAxis - mBottom;
	float		 x			  = dot(atCircle, mVe);
	float		 z				= dot(atCircle, mVn);

	float u = atan2(x, z) / (2 * M_PI);

	// v coordinate is the projection of the point on axis, mapped to [0..1]
	const float height = length(mTop - mBottom);

	float v = CODotAxis / height;

	return Vector3D(u, v, 0.f);
}