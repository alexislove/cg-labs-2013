//-------------------------------------------------------------------
// File: box.h
// 
// Box scene object implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef GEOMETRY_BOX_H
#define GEOMETRY_BOX_H

#include "interfaces/ishape.h"

#include "geometry/ray.h"
#include "geometry/vector3d.h"

class Box : public IShape
{
public:
  //! Constructor, cylinder owns passed material;
  explicit Box(const Vector3D& min, const Vector3D& max, Material* material);

  virtual ~Box();

  //! Override IShape methods
  virtual Intersection intersect(const Ray& ray);

  virtual const Material* getMaterial() const
  {
    return mMaterial;
  }

  virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const;

  virtual void setIsLight(bool light)
  {
    mIsLight = light;
  }

  virtual bool isLight() const
  {
    return mIsLight;
  }

	virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Vector3D getTexCoords(const Vector3D& point, const Intersection& isect = Intersection()) const;

private:
  //! Location
  Vector3D	mMin;
  Vector3D	mMax;
	//! Precalculated values
	float			mDiagonalLength;
  //! Illumination material
  Material* mMaterial;
  //! Is cylinder a light source
  bool			mIsLight;
};

#endif // GEOMETRY_BOX_H