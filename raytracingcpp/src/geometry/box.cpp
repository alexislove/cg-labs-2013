//-------------------------------------------------------------------
// File: box.cpp
// 
// Box scene object implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#define _USE_MATH_DEFINES

#include <assert.h>
#include <math.h>
#include <cfloat>
#include <utility>
#include <xutility>

#include "illumination/material.h"

#include "box.h"

#define DIRECTION_EPSILON 0.1f
#define Infinity FLT_MAX

Box::Box(const Vector3D& min, const Vector3D& max, Material* material)
  : mMin(min),
    mMax(max),
		mIsLight(false),
		mMaterial(material)
{
	mDiagonalLength = length(mMax - mMin);
}

Box::~Box()
{
  delete mMaterial;
}

Intersection Box::intersect(const Ray& ray)
{
  const Vector3D& origin    = ray.getOrigin();
	const Vector3D& direction = ray.getDirection();

	float tmax = Infinity, tmin = -Infinity;

	if (fabs(direction.x()) < FLOAT_ZERO)
	{
		int a = 0;
		a= 5;
	}

	float invDirection = 0.f;
	if (fabs(direction.x()) > FLOAT_ZERO)
	{
		invDirection = 1.f / direction.x();
		tmin = (mMin.x() - origin.x()) * invDirection;
		tmax = (mMax.x() - origin.x()) * invDirection;

		if (tmin > tmax)
		{
			std::swap(tmin, tmax);
		}
	}

	if (fabs(direction.y()) > FLOAT_ZERO)
	{
		invDirection = 1.f / direction.y();
		float tymin = (mMin.y() - origin.y()) * invDirection;
		float tymax = (mMax.y() - origin.y()) * invDirection;

		if (tymin > tymax)
		{
			std::swap(tymin, tymax);
		}

		if (tymin > tmin)
		{
			tmin = tymin;
		}
		if (tymax < tmax)
		{
			tmax = tymax;
		}

		if (tmin > tmax)
		{
			return Intersection(false);
		}

		if (tmax < 0.f)
		{
			return Intersection(false);
		}
	}

	if (fabs(direction.z()) > FLOAT_ZERO)
	{
		invDirection = 1.f / direction.z();
		float tzmin = (mMin.z() - origin.z()) * invDirection;
		float tzmax = (mMax.z() - origin.z()) * invDirection;

		if (tzmin > tzmax)
		{
			std::swap(tzmin, tzmax);
		}

		if (tzmin > tmin)
		{
			tmin = tzmin;
		}
		if (tzmax < tmax)
		{
			tmax = tzmax;
		}

		if (tmin > tmax)
		{
			return Intersection(false);
		}
	}

	if (tmax < 0.f)
	{
		return Intersection(false);
	}

	if (tmin < 0.f)
	{
		return Intersection(false);
	}

	if (tmin == Infinity)
	{
		return Intersection(false);
	}

	Intersection res = Intersection(true, tmin, this, getNormal(ray, tmin));
	res.Distances.resize(2);
	res.Distances[0] = tmin;
	res.Distances[1] = tmax;
	res.InsideIntervals.push_back(Span(tmin, tmax));
	return res;
}

Vector3D Box::getNormal(const Ray& ray, float distance, const Intersection& isect /*= Intersection()*/) const
{
	const Vector3D atSurface = ray.apply(distance);
  
	const Vector3D toMin = atSurface - mMin;
	const Vector3D toMax = atSurface - mMax;

	if (fabs(toMin.x()) < EPSILON)
	{
		return Vector3D(-1.f, 0.f, 0.f);
	}
	if (fabs(toMax.x()) < EPSILON)
	{
		return Vector3D(1.f, 0.f, 0.f);
	}
	if (fabs(toMin.y()) < EPSILON)
	{
		return Vector3D(0.f, -1.f, 0.f);
	}
	if (fabs(toMax.y()) < EPSILON)
	{
		return Vector3D(0.f, 1.f, 0.f);
	}
	if (fabs(toMin.z()) < EPSILON)
	{
		return Vector3D(0.f, 0.f, -1.f);
	}
	if (fabs(toMax.z()) < EPSILON)
	{
		return Vector3D(0.f, 0.f, 1.f);
	}
	assert(false);
	return Vector3D(0.f, 0.f, 0.f);
}

Color Box::getAmbientColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	return mMaterial->AmbientColor;
}

Color Box::getDiffuseColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	if (mMaterial->DiffuseTexture)
	{
		Vector3D texCoords = getTexCoords(point, isect);
		return scale3D(mMaterial->DiffuseTexture->sample(texCoords.x(), texCoords.y()), mMaterial->DiffuseColor);
	}
	return mMaterial->DiffuseColor;
}

Color Box::getSpecularColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	return mMaterial->SpecularColor;
}

Vector3D Box::getTexCoords(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	// TODO:
	return Vector3D();
}