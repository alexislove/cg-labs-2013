//-------------------------------------------------------------------
// File: cone.h
// 
// Cone scene object implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef GEOMETRY_CONE_H
#define GEOMETRY_CONE_H

#include "interfaces/ishape.h"

#include "geometry/ray.h"
#include "geometry/vector3d.h"

class Cone : public IShape
{
public:
	//! Constructor, cone owns passed material;
	Cone(const Vector3D& top, const Vector3D& bottom, float radius, Material* material);

	virtual ~Cone();

	//! Override IShape methods
	virtual Intersection intersect(const Ray& ray);

	virtual const Material* getMaterial() const
	{
		return mMaterial;
	}

	virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const;

	virtual void setIsLight(bool light)
	{
		mIsLight = light;
	}

	virtual bool isLight() const
	{
		return mIsLight;
	}

	virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Vector3D getTexCoords(const Vector3D& point, const Intersection& isect = Intersection()) const;

private:
	//! Location
	Vector3D	mBottom;
	Vector3D	mTop;
	float     mRadius;
	//! Precalculated values
	Vector3D  mAxis;
	float			mRadius2;
	float			mRadPerHeight; // Radius change fer height value
	//! Illumination material
	Material* mMaterial;
	//! Is cylinder a light source
	bool			mIsLight;
};

#endif // GEOMETRY_SPHERE_H