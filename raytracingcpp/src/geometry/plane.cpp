//-------------------------------------------------------------------
// File: plane.cpp
// 
// Infinite plane scene object implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include "illumination/material.h"

#define TILE_SIZE 16

#include "plane.h"

Plane::Plane(const Vector3D& normal, float D, Material* material)
	: mNormal(normal),
		mD(D),
		mMaterial(material),
		mIsLight(false)
{
	mUAxis = Vector3D(mNormal.y(), mNormal.z(), -mNormal.x());
	mVAxis = cross(mUAxis, mNormal);
}

Plane::~Plane()
{
	// Plane owns material
	delete mMaterial;
}


Intersection Plane::intersect(const Ray& ray)
{
	float angle = dot(mNormal, ray.getDirection());
	if (fabs(angle) < FLOAT_ZERO) 
	{
		return Intersection(false);
	}
	float t = (-(dot(ray.getOrigin(), mNormal) + mD) / angle);
	if (t > 0.f)
	{
		Intersection res(true, t, this, getNormal(ray, t));
		res.Distances.push_back(t);
		res.InsideIntervals.push_back(Span(t, t));
		return res;
	}
	return Intersection(false);
}

Color Plane::getAmbientColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	return mMaterial->AmbientColor;
}

Color Plane::getDiffuseColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	if (mMaterial->DiffuseTexture)
	{
		Vector3D texCoords = getTexCoords(point, isect);
		return scale3D(mMaterial->DiffuseTexture->sample(texCoords.x(), texCoords.y()), mMaterial->DiffuseColor);
	}
	return mMaterial->DiffuseColor;
}

Color Plane::getSpecularColor(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	return mMaterial->SpecularColor;
}

Vector3D Plane::getTexCoords(const Vector3D& point, const Intersection& isect/* = Intersection()*/) const
{
	// That's not really good to calculate coordinates according to set texture, but plane if infinite and so on..

	const float xu = dot(point, mUAxis) * mMaterial->TexScaleU;
	const float yv = dot(point, mVAxis) * mMaterial->TexScaleV; 

	//float u = (static_cast<int>(xu) % TILE_SIZE + (xu - floorf(xu))) * 1.f / TILE_SIZE; 
	//float v = (static_cast<int>(yv) % TILE_SIZE + (yv - floorf(yv))) * 1.f / TILE_SIZE;
	//// Repeat texture around negative axis
	//if (u < 0.f)
	//{
	//	u = 1.f + u;
	//}
	//if (v < 0.f)
	//{
	//	v = 1.f + v; 
	//}
	return Vector3D(xu, yv, 0.f);
}