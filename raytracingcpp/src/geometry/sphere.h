//-------------------------------------------------------------------
// File: sphere.h
// 
// Sphere scene object implementation
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef GEOMETRY_SPHERE_H
	#define GEOMETRY_SPHERE_H
	
	#include "interfaces/ishape.h"

	#include "geometry/ray.h"
	#include "geometry/vector3d.h"

	class Sphere : public IShape
	{
	public:
		//! Constructor, sphere owns passed material;
		Sphere(const Vector3D& center, float radius, Material* material);

		virtual ~Sphere();

		//! Override IShape methods
		virtual Intersection intersect(const Ray& ray);

		virtual const Material* getMaterial() const
		{
			return mMaterial;
		}

		virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const;

		virtual void setIsLight(bool light)
		{
			mIsLight = light;
		}

		virtual bool isLight() const
		{
			return mIsLight;
		}

		virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

		virtual Vector3D getTexCoords(const Vector3D& point, const Intersection& isect = Intersection()) const;

	private:
		//! Location
		Vector3D	mCenter;
		float			mRadius;
		//! Precalculated square radius
		float			mRadius2;
		//! Vectors for texture mapping
		Vector3D  mVn;
		Vector3D  mVe;
		Vector3D  mVc;
		//! Illumination material
		Material* mMaterial;
		//! Is sphere a light
		bool			mIsLight;
	};

#endif // GEOMETRY_SPHERE_H