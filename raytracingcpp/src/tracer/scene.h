//-------------------------------------------------------------------
// File: scene.h
// 
// Scene properties collection
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef TRACER_SCENE_H
	#define TRACER_SCENE_H
	
	#include <vector>

	#include "geometry/intersection.h"

	#include "illumination/types.h"

	class	 Camera;
	struct CameraProperties;
	struct IShape;
	struct LightSource;
	struct Material;
	class  Ray;
	struct TracerProperties;
	

	class Scene
	{
	public:
		//! Get default material properties for air
		static const Material* GetDefaultAirProperties();

	public:
		explicit Scene();

		~Scene();

		//! Find closest intersection with one of the scene objects
		Intersection intersect(const Ray& ray, bool stopIfFound) const;

		//! Illuminate scene in given point (calculated along ray direction at given distance)
		Color illuminate(const Ray& viewRay, IShape* object, float distance, const Vector3D& normal) const;

		void addObject(IShape* object);

		void addLightSource(LightSource* light);

		void setBackground(Material* bgMaterial);

		void setupCamera(const CameraProperties& properties);

		void setTracerProperties(TracerProperties* properties);

		void setImagePlaneRes(int resX, int resY);

		void clear();

		void setTracerDepth(float depth);

		const std::vector< IShape* > &getObjects() const
		{
			return mObjects;
		}

		const std::vector< LightSource* > &getLights() const
		{
			return mLights;
		}

		Material* const getBackground() const
		{
			return mBackground;
		}

		Camera* const getCamera() const
		{
			return mCamera;
		}

		TracerProperties* const getTracerProperties() const
		{
			return mTracerProperties;
		}

		int getImagePlaneWidth() const
		{
			return mImagePlaneWidth;
		}

		int getImagePlaneHeight() const
		{
			return mImagePlaneHeight;
		}

	private:
		// Disable copy and assignment
		Scene(const Scene&);
		Scene& operator=(const Scene&);

	private:
		std::vector< IShape* >			mObjects;
		std::vector< LightSource* > mLights;
		Material									 *mBackground;
		Camera										 *mCamera;
		TracerProperties					 *mTracerProperties;
		float												mTracerDepth;
		// Explicitly store dimensions of the scene
		int													mImagePlaneWidth;
		int													mImagePlaneHeight;
	};

#endif // TRACER_SCENE_H