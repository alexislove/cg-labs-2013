//-------------------------------------------------------------------
// File: material.h
// 
// Scene object's material definition as a collection of properties
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------


#ifndef ILLUMINATION_MATERIAL_H
	#define ILLUMINATION_MATERIAL_H

	#include "texture.h"
	#include "types.h"
	
	struct Material
	{
		Material()
			: DiffuseTexture(0x0)
		{
		}

		~Material()
		{
			if (DiffuseTexture)
			{
				delete DiffuseTexture;
				DiffuseTexture  = 0x0;
			}
		}

		// General color properties, according to Phong color model
		Color						EmissiveColor;
		Color						AmbientColor;	 
		Color						DiffuseColor;
		Color						SpecularColor;
		float						SpecularPower;
		// or
		Texture				 *DiffuseTexture;
		// Material properties
		float						Refraction;
		float						Reflection;
		float						Density;
		float						Illumination;
		// Texture properties
		float						TexScaleU;
		float						TexScaleV;
	};

#endif // ILLUMINATION_MATERIAL_H