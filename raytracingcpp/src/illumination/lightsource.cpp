//-------------------------------------------------------------------
// File: lightsource.cpp
// 
// Light source properties collection
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include "geometry/vector3d.h"
#include "geometry/ray.h"
#include "interfaces/ishape.h"
#include "tracer/scene.h"
#include "material.h"

#include "lightsource.h"

#define SHADOW_RAY_EPSILON 0.05f

Color PointLightSource::computeColor(const Scene& scene, IShape* object, const Ray& viewRay, float distance, const Vector3D& normal) const
{
	const Material *objectMaterial  = object->getMaterial();
	const Vector3D  objSurfacePoint = viewRay.apply(distance);
	Color ambientTerm = scale3D(object->getAmbientColor(objSurfacePoint), AmbientIntensity);
	Color diffuseTerm;
	Color specularTerm;
	Color result			= ambientTerm;

	Vector3D shadowRayDirection = Position - objSurfacePoint;
	const float		 distanceToLight	  = length(shadowRayDirection);
	const float		 attenuation				= 1 / (ConstantAttenutaion + LinearAttenutaion * distanceToLight + QuadraticAttenutaion * distanceToLight * distanceToLight);
	// Avoid twice length calculation, similar to normalize
	shadowRayDirection /= distanceToLight; 
	//result *= attenuation;
	const float	cosShadowNormal	= dot(shadowRayDirection, normal);
  result *= attenuation;
	// Light is on the other side, we're illuminating front one
	if (cosShadowNormal <= 0.f)
	{
		return object->getAmbientColor(objSurfacePoint);
	}

	const Ray shadowRay(objSurfacePoint + shadowRayDirection * SHADOW_RAY_EPSILON, shadowRayDirection);	

	const Intersection lightIntersection = scene.intersect(shadowRay, true);
	// Object not in the shadow
	if (!lightIntersection.Exists || lightIntersection.Object->isLight() || lightIntersection.Distance > distanceToLight)
	{
		const Color diffuseColor = object->getDiffuseColor(objSurfacePoint);
		diffuseTerm  = scale3D(diffuseColor, cosShadowNormal * DiffuseIntensity * attenuation);

		const Vector3D lightReflect = (shadowRayDirection - 2 * dot(shadowRayDirection, normal) * normal).toUnit();

		const Vector3D cameraDirection = (viewRay.getOrigin() - objSurfacePoint).toUnit();
		//float	cosLightReflect = dot(shadowRayDirection, lightReflect);
		const float	cosLightReflect = dot(cameraDirection, lightReflect);

		if (cosLightReflect > 0.0f)
		{
			const Color specularColor = object->getSpecularColor(objSurfacePoint); 

			specularTerm	= scale3D(specularColor, SpecularIntensity * powf(cosLightReflect, objectMaterial->SpecularPower) * attenuation);
		}				
	}
	// Compute color, see Phong model
	result += (diffuseTerm + specularTerm);
	return result;
}

Color DirectionalLightSource::computeColor(const Scene& scene, IShape* object, const Ray& viewRay, float distance, const Vector3D& normal) const
{
	const Material *objectMaterial  = object->getMaterial();
	const Vector3D  objSurfacePoint = viewRay.apply(distance);
	Color ambientTerm = scale3D(object->getAmbientColor(objSurfacePoint), AmbientIntensity);
	Color diffuseTerm;
	Color specularTerm;
	Color result			= ambientTerm;

	const Vector3D lightVector	= -Direction;
	
  const Vector3D surfaceLightVector = (Position - objSurfacePoint);
  const float lightDistance				  = dot(surfaceLightVector, lightVector);

	// Return only object's color, if it's too far away from the light source
  if (lightDistance > LightRange)
	{
		return object->getAmbientColor(objSurfacePoint);
	}
	
  const float	cosLightNormal	= dot(lightVector, normal);
	// Light is on the other side, we're illuminating front one
	if (cosLightNormal <= 0.f)
	{
		return result;
	}

	const Ray shadowRay(objSurfacePoint + lightVector * SHADOW_RAY_EPSILON, lightVector);	

	const Intersection lightIntersection = scene.intersect(shadowRay, true);
	// Object not in the shadow
	if (!lightIntersection.Exists || lightIntersection.Object->isLight() || lightIntersection.Distance > lightDistance)
	{
		const Color diffuseColor = object->getDiffuseColor(objSurfacePoint);
		diffuseTerm  = scale3D(diffuseColor, cosLightNormal * DiffuseIntensity);

		const Vector3D lightReflect = (Direction - 2 * dot(Direction, normal) * normal).toUnit();

		const Vector3D cameraDirection = (viewRay.getOrigin() - objSurfacePoint).toUnit();
		//float	cosLightReflect = dot(shadowRayDirection, lightReflect);
		const float	cosLightReflect = dot(cameraDirection, lightReflect);

		if (cosLightReflect > 0.0f)
		{
			const Color specularColor = object->getSpecularColor(objSurfacePoint); 

			specularTerm	= scale3D(specularColor, SpecularIntensity * powf(cosLightReflect, objectMaterial->SpecularPower));
		}				
	}
	// Compute color, see Phong model
	result += (diffuseTerm + specularTerm);
	return result;
}

Color SpotLightSource::computeColor(const Scene& scene, IShape* object, const Ray& viewRay, float distance, const Vector3D& normal) const
{
	const Material *objectMaterial  = object->getMaterial();
	const Vector3D  objSurfacePoint = viewRay.apply(distance);
	Color ambientTerm = scale3D(object->getAmbientColor(objSurfacePoint), AmbientIntensity);
	Color diffuseTerm;
	Color specularTerm;
	Color result			= ambientTerm;

	Vector3D lightVector	= -Direction;
	const float	cosLightNormal	= dot(lightVector, normal);

  const Vector3D lightDirection = (Position - objSurfacePoint).toUnit();

	// Light is on the other side, we're illuminating front one
	if (cosLightNormal <= 0.f)
	{
		return object->getAmbientColor(objSurfacePoint);
	}
	
	const float distanceToLight			= length(Position - objSurfacePoint);
	const float distanceAttenuation = 1 / (ConstantAttenutaion + LinearAttenutaion * distanceToLight + QuadraticAttenutaion * distanceToLight * distanceToLight);
	
  float cosCurrentAngle     = dot(lightDirection, lightVector);
  if (cosCurrentAngle <= 0.f)
  {
    return objectMaterial->AmbientColor;
  }

	result *= distanceAttenuation;

	const float rho = dot(lightDirection, lightVector);
  float spotAttenuation = 0.f;
	if (rho > CosHalfUmbraAngle)
	{
		spotAttenuation = 1.f;
	}
	else if (rho < CosHalfPenumbraAngle)
	{
		spotAttenuation = 0.f;
	}
	else
	{
		const float factor = (rho - CosHalfPenumbraAngle) / (CosHalfUmbraAngle - CosHalfPenumbraAngle);
		spotAttenuation = powf(factor, SpotlightFalloff);
	}

  result *= spotAttenuation;

	const Ray shadowRay(objSurfacePoint + lightVector * SHADOW_RAY_EPSILON, lightVector);	

	const Intersection lightIntersection = scene.intersect(shadowRay, true);
	// Object not in the shadow
	if (!lightIntersection.Exists || lightIntersection.Object->isLight() || lightIntersection.Distance > distanceToLight)
	{
		//result *= spotAttenuation;
		const Color diffuseColor = object->getDiffuseColor(objSurfacePoint);
		diffuseTerm  = scale3D(diffuseColor, cosLightNormal * DiffuseIntensity * spotAttenuation * distanceAttenuation);

		const Vector3D lightReflect = (Direction - 2 * dot(Direction, normal) * normal).toUnit();

		const Vector3D cameraDirection = (viewRay.getOrigin() - objSurfacePoint).toUnit();
		//float	cosLightReflect = dot(shadowRayDirection, lightReflect);
		float	cosLightReflect = dot(cameraDirection, lightReflect);

		if (cosLightReflect > 0.0f)
		{
			const Color specularColor = object->getSpecularColor(objSurfacePoint); 

			specularTerm	= scale3D(specularColor, SpecularIntensity * powf(cosLightReflect, objectMaterial->SpecularPower) * spotAttenuation * distanceAttenuation);
		}				
	}
	// Compute color, see Phong model
	result += (diffuseTerm + specularTerm);
	return result;
}
