//-------------------------------------------------------------------
// File: main.cpp
// 
// Application entry point
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#include <iostream>

#include <QCoreApplication>
#include <QFileInfo>
#include <QUrl>

#include "frontend/tracerwrapper.h"

void usage()
{
	std::cout << "Ray tracing engine"																																			<< std::endl;
	std::cout << "Usage:"																																									<< std::endl;
	std::cout	<< "rt.exe --scene=myscene.xml --resolution_x=1024 --resolution_y=768 --output=myimage.png" << std::endl;
	std::cout << "/? or --help to print this message!"																										<< std::endl;
	std::cout << "Good luck!"																																							<< std::endl;
}

int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);
	// Some params, that can be retrieved from the command line args

	QString sceneFile;
	int			resX			 = 0;
	int     resY			 = 0;
	QString outputFile;
	int		  traceDepth = -1;

	if (argc < 5)
	{
		usage();
		return 0;
	}

	for (int idx = 0; idx < argc; ++idx)
	{
		QString arg(argv[idx]);

		
		if (arg.contains("--scene"))
		{
			sceneFile = arg.remove("--scene=");
			sceneFile.remove("\"");
		}
		else if (arg.contains("--resolution_x"))
		{
			resX = arg.remove("--resolution_x=").toInt();
		}
		else if (arg.contains("--resolution_y"))
		{
			resY = arg.remove("--resolution_y=").toInt();
		}
		else if (arg.contains("--output"))
		{
			outputFile = arg.remove("--output=");
			outputFile.remove("\"");
		}
		else if (arg.contains("--trace_depth"))
		{
			traceDepth = arg.remove("--trace_depth=").toInt();
		}
		else if (arg.contains("/?") || arg.contains("--help"))
		{
			usage();
			return 0;
		}
	}

	
	TracerWrapper wrapper;
	
	wrapper.setRecursionDepth(traceDepth);

	std::cout << "Loading scene." << std::endl;
	if (!wrapper.loadScene(sceneFile))
		return -1;
	std::cout << "Scene loaded, starting ray tracing..." << std::endl;
	wrapper.renderScene(resX, resY, resX, resY);
	std::cout << "Saving result image into file." << std::endl;
	wrapper.saveSceneImage(outputFile);
	std::cout << "Have a nice day!" << std::endl;

	return 0;
}
