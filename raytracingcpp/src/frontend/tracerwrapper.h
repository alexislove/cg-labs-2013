//-------------------------------------------------------------------
// File: tracerwrapper.h
// 
// Qt based ray tracer wrapper
//
// Copyright (c) Alexey Vinogradov <alexey.vinogradov@d-inter.ru>
//-------------------------------------------------------------------

#ifndef FRONTEND_TRACERWRAPPER_H
	#define FRONTEND_TRACERWRAPPER_H

	#include <QImage>
	#include <QSharedPointer>
	
	class QPainter;
	class Scene;

	class TracerWrapper
	{
	public:
		TracerWrapper();

		~TracerWrapper();

		//! Load scene from xml
		bool loadScene(const QString& fileName);

		//! Perform ray tracing of the scene with given resolutions
		//! And then scale all up to width/height of the render image, which will be saved
		void renderScene(int resolutionX, int resolutionY, int width, int height);

		//! Render current image 
		void renderImage(QPainter* painter);

		//! Save current scene to the given file
		void saveSceneImage(const QString& fileName);

		//! Set tracer depth
		void setRecursionDepth(int depth);

	private:
		//! Image for ray tracer output
		QImage									mTracerOutput;
		//! Image for rendering
		QImage									mRenderImage;
		//! Scene pointer
		QSharedPointer< Scene > mScene;
		//! Tracer depth
		int										mTracerDepth;
	};

#endif // FRONTEND_TRACERWRAPPER_H
